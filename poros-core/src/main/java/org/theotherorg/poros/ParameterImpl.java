/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.util.Objects;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

/**
 * The default implementation of {@link Parameter}.
 */
class ParameterImpl implements Parameter {
	private static final long serialVersionUID = -8428493616219641236L;
	private final String defaultValue;
	private final String key;

	public ParameterImpl(@Nonnull final Parameter copy, @Nonnull final String key) {
		Preconditions.checkArgument(copy != null, "null copy");
		Preconditions.checkArgument(key != null, "null key");
		defaultValue = copy.getDefaultValue();
		this.key = key;
	}

	public ParameterImpl(@Nonnull final String key, final String defaultValue) {
		Preconditions.checkArgument(key != null, "null key");
		this.defaultValue = defaultValue;
		this.key = key;
	}

	@Override
	public String getDefaultValue() {
		return defaultValue;
	}

	@Nonnull
	@Override
	public String getKey() {
		return key;
	}

	@Override
	public Iterable<String> resolveIn(final HasParameters parameters) {
		return parameters.resolve(this);
	}

	@Override
	public String write(final ParameterWriter writer) {
		return writer.write(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(key);
	}

	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Parameter) {
			final Parameter other = (Parameter)o;

			return Objects.equals(other.getKey(), key);
		}

		return false;
	}
}
