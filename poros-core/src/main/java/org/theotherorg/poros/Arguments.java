/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;

/**
 * Encapsulates the argument list of a command line and handles configurations and parameters according to the build required. By default --dumper, --stacktrace and --conf are supported by the core's argument handling.
 * <ul>
 * <li>--dumper: causes any errors to dump its parameter state when being evaluated.</li>
 * <li>--stacktrace: enables printing of the stack on error</li>
 * <li>--conf &lt;path&gt;: allows specifying top priority configurations, highest priority first, lowest last </li>
 * </ul>
 */
public final class Arguments {
	/**
	 * Handles one or several arguments using the provided {@link Builder}.
	 */
	public interface Handler {
		/**
		 * Evaluate the provided {@code args} using the specified {@link Builder}.
		 *
		 * @param builder
		 * 	An instance of {@link Builder}, can not be null
		 * @param args
		 * 	An array of {@link String} arguments
		 */
		void handle(@Nonnull Builder builder, String... args);
	}

	/**
	 * An abstract {@link UncaughtExceptionHandler} which holds a {@link PrintStream}
	 */
	static abstract class AbstractPrintExceptionHandler implements UncaughtExceptionHandler {
		private final PrintStream out;

		public AbstractPrintExceptionHandler(@Nonnull final PrintStream out) {
			Preconditions.checkArgument(out != null, "null out");
			this.out = out;
		}

		public PrintStream getOut() {
			return out;
		}
	}

	/**
	 * The {@link UncaughtExceptionHandler} used by default.
	 */
	static final class ArgumentsExceptionHandler extends AbstractPrintExceptionHandler {
		ArgumentsExceptionHandler(@Nonnull final PrintStream out) {
			super(out);
		}

		public ArgumentsExceptionHandler() {
			super(System.err);
		}

		@Override
		public void uncaughtException(final Thread thread, final Throwable throwable) {
			getOut().println("\u001B[31m" + throwable.getMessage() + "\u001B[0m");
		}
	}

	/**
	 * Composition of several {@link UncaughtExceptionHandler}, all will be called on an exception thrown.
	 */
	static final class CompositeExceptionHandler extends AbstractPrintExceptionHandler {
		private final List<UncaughtExceptionHandler> handlers;

		CompositeExceptionHandler(@Nonnull final PrintStream out, final UncaughtExceptionHandler... handlers) {
			super(out);
			this.handlers = Arrays.asList(handlers);
		}

		public CompositeExceptionHandler(final UncaughtExceptionHandler... handlers) {
			this(System.err, handlers);
		}

		@Override
		public void uncaughtException(final Thread thread, final Throwable throwable) {
			final Iterator<UncaughtExceptionHandler> it = handlers.iterator();

			while (it.hasNext()) {
				final UncaughtExceptionHandler handler = it.next();

				if (handler != null) {
					handler.uncaughtException(thread, throwable);
				}

				if (it.hasNext()) {
					getOut().print("\n\n");
				}
			}
		}
	}

	/**
	 * An {@link UncaughtExceptionHandler} which will call {@link Parameters#resolve(Parameter)} and print the result its parent {@link PrintStream}.
	 */
	static final class DumperExceptionHandler extends AbstractPrintExceptionHandler {
		private final HasParameters parameters;
		private final ParameterWriter writer;

		DumperExceptionHandler(@Nonnull final PrintStream out, final HasParameters parameters) {
			super(out);
			this.parameters = parameters;
			this.writer = new ParameterWriterImpl();
		}

		public DumperExceptionHandler(final HasParameters parameters) {
			this(System.err, parameters);
		}

		@Override
		public void uncaughtException(final Thread thread, final Throwable throwable) {
			if (throwable instanceof IllegalParameterException) {
				final IllegalParameterException exception = (IllegalParameterException)throwable;
				final Parameter parameter = exception.getParameter();
				final PrintStream out = getOut();

				if (parameter != null) {
					out.print(parameter.write(writer));
					out.println();
					out.print(writer.write(parameters, parameter));
				}
			}
		}
	}

	/**
	 * An {@link UncaughtExceptionHandler} which simply call {@link Throwable#printStackTrace(PrintStream)} with its parent {@link PrintStream}.
	 */
	static final class TraceExceptionHandler extends AbstractPrintExceptionHandler {
		TraceExceptionHandler(@Nonnull final PrintStream out) {
			super(out);
		}

		public TraceExceptionHandler() {
			this(System.err);
		}

		@Override
		public void uncaughtException(final Thread thread, final Throwable throwable) {
			throwable.printStackTrace(getOut());
		}
	}

	/**
	 * Builds instances of {@link Arguments} following the argument handlers, resources parameters and parameters loaded. Argument builders are generally prepared with a confHandler and an {@link UncaughtExceptionHandler} to enable on {@link
	 * Arguments#handle(Consumer)}. The confHandler used handles the --conf options recursively from the arguments fed to {@link Builder#from(String...)} which ends the builder chain.
	 */
	public static class Builder {
		private final List<Handler> argumentHandlers = new ArrayList<>();
		private Handler confHandler;
		private UncaughtExceptionHandler exceptionHandler;
		private HasParameters parameters;
		private ParametersPool parametersPool;
		private List<ParameterOverride> resourceParameters;

		/**
		 * Constructs an {@link Arguments} from a list of {@link String} arguments.
		 *
		 * @param args
		 * 	A non-null array of {@link String arguments}
		 *
		 * @return A new instance of {@link Arguments}
		 */
		@Nonnull
		public Arguments from(final String... args) {
			final Parameters systemParams = new Parameters();

			if (confHandler != null) {
				confHandler.handle(this, args);
			}

			if (parametersPool == null) {
				parameters = systemParams;
			} else {
				parametersPool.add(systemParams);
				parameters = parametersPool;
			}

			if (resourceParameters != null) {
				resourceParameters.forEach(systemParams::load);
			}

			for (final Handler handler : argumentHandlers) {
				handler.handle(this, args);
			}

			return new Arguments(parameters, exceptionHandler);
		}

		/**
		 * Gets the currently held {@code confHandler} or null if none have been set.
		 *
		 * @return A {@link Handler} or null
		 */
		public Handler getConfHandler() {
			return confHandler;
		}

		/**
		 * Sets a {@link Handler} as the new {@code confHandler}.
		 *
		 * @param confHandler
		 * 	An instance of {@link Handler}
		 */
		public void setConfHandler(final Handler confHandler) {
			this.confHandler = confHandler;
		}

		/**
		 * Returns the currently held {@code exceptionHandler} or null if none have been set.
		 *
		 * @return An {@link UncaughtExceptionHandler} or null
		 */
		public UncaughtExceptionHandler getExceptionHandler() {
			return exceptionHandler;
		}

		/**
		 * Sets an {@link UncaughtExceptionHandler} as the new {@code exceptionHandler}.
		 *
		 * @param exceptionHandler
		 * 	An {@link UncaughtExceptionHandler} or null
		 */
		public void setExceptionHandler(final UncaughtExceptionHandler exceptionHandler) {
			this.exceptionHandler = exceptionHandler;
		}

		/**
		 * Returns the parameters read from this Builder. This will generally be null if the {@link Builder#from(String...)} method has not been called.
		 *
		 * @return An instance of {@link HasParameters} or null
		 */
		public HasParameters getParameters() {
			return parameters;
		}

		/**
		 * Parses configuration file options recursively from the provided {@code arguments}.
		 *
		 * @param arguments
		 * 	A list of {@link String} arguments
		 *
		 * @return {@code this}
		 *
		 * @throws IllegalResourceException
		 * 	if a resource can not be loaded
		 */
		public Builder withConfigurations(final List<String> arguments) {
			final int confIndex = Iterables.indexOf(arguments, "--conf"::equals);

			if (confIndex >= 0 && confIndex + 1 < arguments.size()) {
				final Parameters fileParams = new Parameters(new FileBasedLoader());

				fileParams.load(arguments.get(confIndex + 1));

				if (parametersPool == null) {
					parametersPool = new ParametersPool();
				}

				parametersPool.add(fileParams);
				return withConfigurations(arguments.subList(confIndex + 2, arguments.size()));
			}

			return this;
		}

		/**
		 * Adds a {@link Handler} to this {@link Builder} handler chain.
		 *
		 * @param handler
		 * 	A {@link Handler}, can not be null
		 *
		 * @return {@code this}
		 *
		 * @throws IllegalArgumentException
		 * 	if {@code handler} is null
		 */
		@Nonnull
		public Builder withHandler(@Nonnull final Handler handler) {
			Preconditions.checkArgument(handler != null, "null parameter");
			argumentHandlers.add(handler);
			return this;
		}

		/**
		 * Adds a {@link ParameterOverride} for which a resource name can be loaded into the parameters of this Builder.
		 *
		 * @param parameter
		 * 	A {@link ParameterOverride}, can not be null
		 *
		 * @return {@code this}
		 *
		 * @throws IllegalArgumentException
		 * 	if {@code parameter} is null
		 */
		@Nonnull
		public Builder withResource(@Nonnull final ParameterOverride parameter) {
			Preconditions.checkArgument(parameter != null, "null parameter");

			if (resourceParameters == null) {
				resourceParameters = new ArrayList<>();
			}

			resourceParameters.add(parameter);
			return this;
		}
	}

	private final UncaughtExceptionHandler exceptionHandler;
	private final HasParameters parameters;

	Arguments(@Nonnull final HasParameters parameters, final UncaughtExceptionHandler exceptionHandler) {
		Preconditions.checkArgument(parameters != null, "null parameters");
		this.exceptionHandler = exceptionHandler;
		this.parameters = parameters;
	}

	/**
	 * Handles the {@link HasParameters} gathered by this {@link Arguments}.
	 *
	 * @param parametersConsumer
	 * 	A {@link Consumer} of {@link HasParameters}, can not be null
	 */
	public void handle(final Consumer<HasParameters> parametersConsumer) {
		Preconditions.checkArgument(parametersConsumer != null, "null consumer");

		if (exceptionHandler != null) {
			Thread.currentThread().setUncaughtExceptionHandler(exceptionHandler);
		}

		parametersConsumer.accept(parameters);
	}

	/**
	 * Constructs an {@link Arguments} from all defaults and the provided {@link String} arguments
	 *
	 * @param args
	 * 	An array of {@link String} args
	 *
	 * @return A new instance of {@link Arguments}
	 */
	@Nonnull
	public static Arguments from(final String... args) {
		return Arguments.withDefaults().from(args);
	}

	/**
	 * Returns a {@link Builder} with the default {@code confHandler}, the --dumper and the --stacktrace handlers. Sets the current thread {@link UncaughtExceptionHandler} to the default.
	 *
	 * @return A new instance of {@link Builder}
	 */
	@Nonnull
	public static Builder withDefaults() {
		final Builder builder = new Builder();

		Thread.currentThread().setUncaughtExceptionHandler(new ArgumentsExceptionHandler());
		builder.setConfHandler(Arguments::withConfHandler);
		builder.withHandler(Arguments::withDumperHandler);
		builder.withHandler(Arguments::withTracerHandler);
		return builder;
	}

	/**
	 * Returns a {@link Builder} with the default {@code confHandler} and the provided {@code argHandler}.
	 *
	 * @param argHandler
	 * 	An instance of {@link Handler}, can not be null
	 *
	 * @return A new instance of {@link Builder}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code argHandler} is null
	 */
	public static Builder withHandler(@Nonnull final Handler argHandler) {
		final Builder builder = new Builder();

		Thread.currentThread().setUncaughtExceptionHandler(new ArgumentsExceptionHandler());
		builder.setConfHandler(Arguments::withConfHandler);
		return builder.withHandler(argHandler);
	}

	/**
	 * Returns {@link Builder} with the default {@code confHandler}, the --dumper and the --stacktrace handlers, and the provided {@link ParameterOverride} to look for a configuration resource.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return A new instance of {@link Builder}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	public static Builder withResource(@Nonnull final ParameterOverride parameter) {
		Preconditions.checkArgument(parameter != null, parameter);
		return withDefaults().withResource(parameter);
	}

	/**
	 * Handles --conf recursively, any amount of configurations may be provided.
	 *
	 * @param builder
	 * 	A non-null {@link Builder}
	 * @param args
	 * 	An array of {@link String} arguments
	 */
	static void withConfHandler(@Nonnull final Builder builder, final String... args) {
		builder.withConfigurations(Arrays.asList(args));
	}

	/**
	 * Handles --dumper by setting the exceptionHandler to {@link DumperExceptionHandler} if the current builder holds none. If it does, this handler makes a composite and resets the handler.
	 *
	 * @param builder
	 * 	A non-null {@link Builder}
	 * @param args
	 * 	An array of {@link String} arguments
	 */
	static void withDumperHandler(@Nonnull final Builder builder, final String... args) {
		final boolean needsDumper = Iterables.any(Arrays.asList(args), "--dumper"::equals);

		if (needsDumper) {
			final DumperExceptionHandler dumperHandler = new DumperExceptionHandler(builder.getParameters());
			final UncaughtExceptionHandler handler = builder.getExceptionHandler();

			if (handler == null) {
				builder.setExceptionHandler(dumperHandler);
			} else {
				builder.setExceptionHandler(new CompositeExceptionHandler(dumperHandler, handler));
			}
		}
	}

	/**
	 * Handles --stacktrace by setting the exceptionHandler to {@link TraceExceptionHandler} if the current builder holds none. If it does, this handler makes a composite and resets the handler.
	 *
	 * @param builder
	 * 	A non-null {@link Builder}
	 * @param args
	 * 	An array of {@link String} arguments
	 */
	static void withTracerHandler(@Nonnull final Builder builder, final String... args) {
		final boolean needsTrace = Iterables.any(Arrays.asList(args), "--stacktrace"::equals);

		if (needsTrace) {
			final TraceExceptionHandler traceHandler = new TraceExceptionHandler();
			final UncaughtExceptionHandler handler = builder.getExceptionHandler();

			if (handler == null) {
				builder.setExceptionHandler(traceHandler);
			} else {
				builder.setExceptionHandler(new CompositeExceptionHandler(handler, traceHandler));
			}
		}
	}
}
