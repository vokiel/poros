/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.text.DateFormat;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

/**
 * Factories for {@link ChildParameter}.
 */
public final class ChildParameters {
	/**
	 * A child parameter for a {@link ParameterDate}.
	 */
	static class ChildParameterDate extends ChildParameterImpl implements ParameterDate {
		private static final long serialVersionUID = 1889898284483502448L;
		private final DateFormat dateFormat;
		private final Parameter parameterFormat;
		private final ParameterDate parent;

		public ChildParameterDate(@Nonnull final ParameterDate parent, final String key) {
			this(parent, key, null, null);
		}

		public ChildParameterDate(@Nonnull final ParameterDate parent, final String key, final DateFormat dateFormat) {
			this(parent, key, dateFormat, null);
		}

		public ChildParameterDate(@Nonnull final ParameterDate parent, final String key, final DateFormat dateFormat, final Parameter parameterFormat) {
			super(parent, key);
			this.parent = parent;
			this.dateFormat = dateFormat;
			this.parameterFormat = parameterFormat;
		}

		@Nonnull
		@Override
		public DateFormat getDateFormat() {
			return dateFormat == null ? parent.getDateFormat() : dateFormat;
		}

		@Override
		public String getDefaultValue() {
			return null;
		}

		@Override
		public Parameter getParameterFormat() {
			return parameterFormat == null ? parent.getParameterFormat() : parameterFormat;
		}

		@Override
		public Iterable<String> resolveIn(final HasParameters parameters) {
			return parameters.resolve(this);
		}

		@Override
		public String write(final ParameterWriter writer) {
			return writer.writeParent(this) + writer.write(this);
		}
	}

	/**
	 * A child parameter for a {@link Parameter}.
	 */
	static class ChildParameterImpl implements ChildParameter {
		private static final long serialVersionUID = -3388936494992542017L;
		private final String defaultValue;
		private final String key;
		private final Parameter parent;

		public ChildParameterImpl(@Nonnull final Parameter parent, final String key) {
			this(parent, key, null);
		}

		public ChildParameterImpl(@Nonnull final Parameter parent, final String key, final String defaultValue) {
			Preconditions.checkArgument(parent != null, "parent null");
			this.parent = parent;
			this.key = key;
			this.defaultValue = defaultValue;
		}

		@Override
		public String getDefaultValue() {
			return defaultValue == null ? parent.getDefaultValue() : defaultValue;
		}

		@Nonnull
		@Override
		public String getKey() {
			return key == null ? parent.getKey() : key;
		}

		@Nonnull
		@Override
		public Parameter getParent() {
			return parent;
		}

		@Override
		public Iterable<String> resolveIn(final HasParameters parameters) {
			return parameters.resolve(this);
		}

		@Override
		public String write(final ParameterWriter writer) {
			return writer.writeParent(this) + writer.write(this);
		}
	}

	/**
	 * A child parameter for a {@link ParameterOverride}.
	 */
	static class ChildParameterOverride extends ChildParameterImpl implements ParameterOverride {
		private static final long serialVersionUID = -682575359964850988L;
		private final String envKey;
		private final ParameterOverride parent;

		public ChildParameterOverride(final ParameterOverride parent, final String key) {
			this(parent, key, null, null);
		}

		public ChildParameterOverride(final ParameterOverride parent, final String key, final String envKey) {
			this(parent, key, envKey, null);
		}

		public ChildParameterOverride(final ParameterOverride parent, final String key, final String envKey, final String defaultValue) {
			super(parent, key, defaultValue);
			this.envKey = envKey;
			this.parent = parent;
		}

		@Nonnull
		@Override
		public String getEnvKey() {
			return envKey == null ? parent.getEnvKey() : envKey;
		}

		@Override
		public Iterable<String> resolveIn(final HasParameters parameters) {
			return parameters.resolve(this);
		}

		@Override
		public String write(final ParameterWriter writer) {
			return writer.writeParent(this) + writer.write(this);
		}
	}

	private ChildParameters() {
		throw new IllegalStateException();
	}

	/**
	 * Creates a {@link Parameter} with the provided default {@link String} value and the specified parent {@code parameter}.
	 *
	 * @param parameter
	 * 	A non-null {@link Parameter}
	 * @param defaultValue
	 * 	A {@link String} value
	 *
	 * @return A new {@link Parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	public static Parameter withDefaultValue(@Nonnull final Parameter parameter, final String defaultValue) {
		return new ChildParameterImpl(parameter, null, defaultValue);
	}

	/**
	 * Creates a {@link ParameterOverride} with the provided default {@link String} value and the specified parent {@code parameterOverride}.
	 *
	 * @param parameterOverride
	 * 	A non-null {@link ParameterOverride}
	 * @param defaultValue
	 * 	A {@link String} value
	 *
	 * @return A new {@link ParameterOverride}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterOverride} is null
	 */
	public static ParameterOverride withDefaultValue(@Nonnull final ParameterOverride parameterOverride, final String defaultValue) {
		return new ChildParameterOverride(parameterOverride, null, null, defaultValue);
	}

	/**
	 * Creates a {@link ParameterOverride} with the provided {@link String} key and default value for the specified parent {@code parameterOverride}.
	 *
	 * @param parameterOverride
	 * 	A non-null {@link ParameterOverride}
	 * @param key
	 * 	A {@link String} key
	 * @param defaultValue
	 * 	A {@link String} value
	 *
	 * @return A new {@link ParameterOverride}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterOverride} is null
	 */
	public static ParameterOverride withDefaultValue(@Nonnull final ParameterOverride parameterOverride, final String key, final String defaultValue) {
		return new ChildParameterOverride(parameterOverride, key, null, defaultValue);
	}

	/**
	 * Creates a {@link ParameterOverride} with the provided {@link String} environment key for the specified parent {@code parameterOverride}.
	 *
	 * @param parameterOverride
	 * 	A non-null {@link ParameterOverride}
	 * @param envKey
	 * 	A {@link String} envKey
	 *
	 * @return A new {@link ParameterOverride}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterOverride} is null
	 */
	public static ParameterOverride withEnvKey(@Nonnull final ParameterOverride parameterOverride, final String envKey) {
		return new ChildParameterOverride(parameterOverride, null, envKey, null);
	}

	/**
	 * Creates a {@link ParameterOverride} with the provided {@link String} environment key and default value for the specified parent {@code parameterOverride}.
	 *
	 * @param parameterOverride
	 * 	A non-null {@link ParameterOverride}
	 * @param envKey
	 * 	A {@link String} envKey
	 * @param defaultValue
	 * 	A {@link String} value
	 *
	 * @return A new {@link ParameterOverride}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterOverride} is null
	 */
	public static ParameterOverride withEnvKey(@Nonnull final ParameterOverride parameterOverride, final String envKey, final String defaultValue) {
		return new ChildParameterOverride(parameterOverride, null, envKey, defaultValue);
	}

	/**
	 * Creates a {@link Parameter} with the provided {@link String} key for the specified parent {@code parameter}.
	 *
	 * @param parameter
	 * 	A non-null {@link Parameter}
	 * @param key
	 * 	A {@link String} key
	 *
	 * @return A new {@link Parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	public static Parameter withKey(@Nonnull final Parameter parameter, final String key) {
		return new ChildParameterImpl(parameter, key);
	}

	/**
	 * Creates a {@link Parameter} with the provided {@link String} key and default value for the specified parent {@code parameter}.
	 *
	 * @param parameter
	 * 	A non-null {@link Parameter}
	 * @param key
	 * 	A {@link String} key
	 * @param defaultValue
	 * 	A {@link String} value
	 *
	 * @return A new {@link Parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	public static Parameter withKey(@Nonnull final Parameter parameter, final String key, final String defaultValue) {
		return new ChildParameterImpl(parameter, key, defaultValue);
	}

	/**
	 * Creates a {@link ParameterDate} with the provided {@link String} key for the specified parent {@code parameterDate}.
	 *
	 * @param parameterDate
	 * 	A non-null {@link ParameterDate}
	 * @param key
	 * 	A {@link String} key
	 *
	 * @return A new {@link ParameterDate}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterDate} is null
	 */
	public static ParameterDate withKey(@Nonnull final ParameterDate parameterDate, final String key) {
		return new ChildParameterDate(parameterDate, key);
	}

	/**
	 * Creates a {@link ParameterOverride} with the provided {@link String} key for the specified parent {@code parameterOverride}.
	 *
	 * @param parameterOverride
	 * 	A non-null {@link ParameterOverride}
	 * @param key
	 * 	A {@link String} key
	 *
	 * @return A new {@link ParameterOverride}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterOverride} is null
	 */
	public static ParameterOverride withKey(@Nonnull final ParameterOverride parameterOverride, final String key) {
		return new ChildParameterOverride(parameterOverride, key);
	}

	/**
	 * Creates a {@link ParameterDate} with the provided {@link String} key and {@link DateFormat} for the specified {@code parameterDate}.
	 *
	 * @param parameterDate
	 * 	A non-null {@link ParameterDate}
	 * @param key
	 * 	A {@link String} key
	 * @param dateFormat
	 * 	A {@link DateFormat}
	 *
	 * @return A new {@link ParameterDate}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterDate} is null
	 */
	public static ParameterDate withKeyFormat(@Nonnull final ParameterDate parameterDate, final String key, final DateFormat dateFormat) {
		return new ChildParameterDate(parameterDate, key, dateFormat);
	}

	/**
	 * Creates a {@link ParameterDate} with the provided {@link String} key, {@link DateFormat} and {@link Parameter} format for the specified {@code parameterDate}.
	 *
	 * @param parameterDate
	 * 	A non-null {@link ParameterDate}
	 * @param key
	 * 	A {@link String} key
	 * @param dateFormat
	 * 	A {@link DateFormat}
	 * @param parameterFormat
	 * 	A {@link Parameter} format
	 *
	 * @return A new {@link ParameterDate}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterDate} is null
	 */
	public static ParameterDate withKeyFormat(@Nonnull final ParameterDate parameterDate, final String key, final DateFormat dateFormat, final Parameter parameterFormat) {
		return new ChildParameterDate(parameterDate, key, dateFormat, parameterFormat);
	}

	/**
	 * Creates a {@link ParameterOverride} with the provided {@link String} key and environment key for the specified {@code parameterOverride}.
	 *
	 * @param parameterOverride
	 * 	A non-null {@link ParameterOverride}
	 * @param key
	 * 	A {@link String} key
	 * @param envKey
	 * 	A {@link String} key
	 *
	 * @return A new {@link ParameterOverride}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterOverride} is null
	 */
	public static ParameterOverride withKeys(@Nonnull final ParameterOverride parameterOverride, final String key, final String envKey) {
		return new ChildParameterOverride(parameterOverride, key, envKey);
	}

	/**
	 * Creates a {@link ParameterOverride} with the provided {@link String} key, environment key and default value for the specified {@code parameterOverride}.
	 *
	 * @param parameterOverride
	 * 	A non-null {@link ParameterOverride}
	 * @param key
	 * 	A {@link String} key
	 * @param envKey
	 * 	A {@link String} key
	 * @param defaultValue
	 * 	A {@link String} value
	 *
	 * @return A new {@link ParameterOverride}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameterOverride} is null
	 */
	public static ParameterOverride withKeys(@Nonnull final ParameterOverride parameterOverride, final String key, final String envKey, final String defaultValue) {
		return new ChildParameterOverride(parameterOverride, key, envKey, defaultValue);
	}
}
