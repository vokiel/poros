/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.util.Collections;
import java.util.Properties;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

/**
 * Adapts an instance of {@link Properties} to {@link HasProperties}.
 */
final class HasPropertiesAdapter implements HasProperties {
	private final String name;
	private final Properties properties;

	public HasPropertiesAdapter(@Nonnull final Properties properties) {
		this(properties, "local");
	}

	public HasPropertiesAdapter(@Nonnull final Properties properties, @Nonnull final String name) {
		Preconditions.checkArgument(properties != null, "null properties");
		Preconditions.checkArgument(name != null, "null name");
		this.properties = properties;
		this.name = name;
	}

	@Override
	public String get(@Nonnull final String key, final String defaultValue) {
		Preconditions.checkArgument(key != null, "null key");
		return properties.getProperty(key, defaultValue);
	}

	@Override
	public String get(@Nonnull final String key) {
		Preconditions.checkArgument(key != null, "null key");
		return properties.getProperty(key);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isEmpty() {
		return properties.isEmpty();
	}

	@Nonnull
	@Override
	public Iterable<String> resolve(@Nonnull final String key) {
		final String value = properties.getProperty(key);

		return value == null ? Collections.emptyList() : Collections.singleton(name + ": " + key + ": " + value);
	}
}
