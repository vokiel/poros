/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import javax.annotation.Nonnull;

/**
 * Thrown when a resource can not be loaded.
 */
public final class IllegalResourceException extends IllegalArgumentException {
	private static final long serialVersionUID = -7609666977649035852L;
	private final String resourceName;

	IllegalResourceException(final String resourceName, final String message) {
		super(message);
		this.resourceName = resourceName;
	}

	IllegalResourceException(final String resourceName, final String message, final Throwable t) {
		super(message, t);
		this.resourceName = resourceName;
	}

	public IllegalResourceException(final String resourceName) {
		super("Unknown resource exception");
		this.resourceName = resourceName;
	}

	/**
	 * Returns the name of the resource that is illegal
	 *
	 * @return A {@link String} name
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * Constructs and {@link IllegalResourceException} with a {@code resourceName} and a {@link Throwable}.
	 *
	 * @param resourceName
	 * 	A {@link String} resourceName, should not be null
	 * @param t
	 * 	An instance of {@link Throwable}
	 *
	 * @return A new instance of {@link IllegalResourceException}
	 */
	@Nonnull
	public static IllegalResourceException from(@Nonnull final String resourceName, final Throwable t) {
		return new IllegalResourceException(resourceName, "Resource [" + resourceName + "] could not be used", t);
	}

	/**
	 * Constructs and {@link IllegalResourceException} with a {@code resourceName}.
	 *
	 * @param resourceName
	 * 	A {@link String} resourceName, should not be null
	 *
	 * @return A new instance of {@link IllegalResourceException}
	 */
	@Nonnull
	public static IllegalResourceException from(@Nonnull final String resourceName) {
		return new IllegalResourceException(resourceName, "Resource [" + resourceName + "] could not be found");
	}
}
