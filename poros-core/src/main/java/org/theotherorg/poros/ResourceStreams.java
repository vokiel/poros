/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Nonnull;

/**
 * Contract to provide instances of {@link InputStream} associated to resource names.
 */
public interface ResourceStreams {
	/**
	 * Gets an {@link InputStream} for the provided {@link String} resource name. The returned stream should be opened and ready for input.
	 *
	 * @param resourceName
	 * 	A non-null {@link String} name
	 *
	 * @return An instance of {@link InputStream}
	 *
	 * @throws IOException
	 * 	if it feels like it
	 */
	@Nonnull
	InputStream getInputStream(@Nonnull String resourceName) throws IOException;
}
