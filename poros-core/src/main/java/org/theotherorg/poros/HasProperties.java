/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import javax.annotation.Nonnull;

/**
 * Contracts an instance with the responsibility of providing {@link String} values for {@link String} keys.
 */
interface HasProperties {
	/**
	 * Gets the value associated with the provided {@code key}. If the value evaluates to null, the {@code defaultValue} specified is returned.
	 *
	 * @param key
	 * 	A {@link String} key, can not be null
	 * @param defaultValue
	 * 	A {@link String} value
	 *
	 * @return A {@link String} value or null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code key} is null
	 */
	String get(@Nonnull String key, String defaultValue);

	/**
	 * Gets the value associated with the provided {@code key}. This will return null if the key was not found.
	 *
	 * @param key
	 * 	A {@link String} key, can not be null
	 *
	 * @return A {@link String} value that can be null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code key} is null
	 */
	String get(@Nonnull String key);

	/**
	 * Returns the name associated with this instance of {@link HasProperties} if any have been specified.
	 *
	 * @return A {@link String} name or null
	 */
	String getName();

	/**
	 * Indicates whether this {@link HasProperties} is empty or not.
	 *
	 * @return true if no properties are present, false otherwise
	 */
	boolean isEmpty();

	/**
	 * Returns a list describing the location of a key in an instance of {@link HasProperties}.
	 *
	 * @param key
	 * 	A {@link String} key
	 *
	 * @return An {@link Iterable} of {@link String}
	 */
	@Nonnull
	Iterable<String> resolve(@Nonnull String key);
}
