/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.text.DateFormat;
import java.util.Objects;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

/**
 * The default implementation of {@link ParameterDate}.
 */
class ParameterDateImpl extends ParameterImpl implements ParameterDate {
	private static final long serialVersionUID = -5795473953238045829L;
	private final DateFormat dateFormat;
	private Parameter parameterFormat;

	public ParameterDateImpl(@Nonnull final String key, @Nonnull final DateFormat dateFormat) {
		super(key, null);
		Preconditions.checkArgument(dateFormat != null, "null dateFormat");
		this.dateFormat = dateFormat;
	}

	@Nonnull
	@Override
	public DateFormat getDateFormat() {
		return dateFormat;
	}

	@Override
	public Parameter getParameterFormat() {
		return parameterFormat;
	}

	public void setParameterFormat(@Nonnull final Parameter parameterFormat) {
		Preconditions.checkArgument(parameterFormat != null, "null parameterFormat");
		this.parameterFormat = parameterFormat;
	}

	@Override
	public Iterable<String> resolveIn(final HasParameters parameters) {
		return parameters.resolve(this);
	}

	@Override
	public String write(final ParameterWriter writer) {
		return writer.write(this);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getKey(), getDateFormat());
	}

	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof ParameterDate) {
			final ParameterDate other = (ParameterDate)o;

			return Objects.equals(other.getKey(), getKey()) && Objects.equals(other.getDateFormat(), getDateFormat());
		}

		return false;
	}
}
