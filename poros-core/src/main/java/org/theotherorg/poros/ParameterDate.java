/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

/**
 * A {@link Date} parameter needs to be read using a parser associated with a {@link DateFormat}. This extension to {@link Parameter} adds a required date format and the possibility to override it with another parameter containing the
 * format as a {@link String}.
 */
public interface ParameterDate extends Parameter {
	/**
	 * A date parameter is read using a {@link DateFormat} format if {@link ParameterDate#getParameterFormat()} is null. This can not be null.
	 *
	 * @return An instance of {@link DateFormat}
	 */
	@Nonnull
	DateFormat getDateFormat();

	/**
	 * A parameter to read for overriding {@link ParameterDate#getDateFormat()}. This may be null.
	 *
	 * @return An instance of {@link Parameter}
	 */
	Parameter getParameterFormat();

	/**
	 * Gets a date from the specified {@code dateFormat} and {@code dateValue}.
	 *
	 * @param dateFormat
	 * 	An instance of {@link DateFormat}, can not be null
	 * @param dateValue
	 * 	A {@link String} value, can not be null
	 *
	 * @return A {@link Date}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code dateFormat} is null
	 * @throws IllegalArgumentException
	 * 	if {@code dateValue} is null
	 * @throws IllegalArgumentException
	 * 	if {@code dateValue} can not be parsed as a date
	 */
	static Date getDate(@Nonnull final DateFormat dateFormat, @Nonnull final String dateValue) {
		final Date result;

		Preconditions.checkArgument(dateFormat != null, "null dateFormat");
		Preconditions.checkArgument(dateValue != null, "null dateValue");

		try {
			result = dateFormat.parse(dateValue);
		} catch (final ParseException ex) {
			throw new IllegalArgumentException(ex);
		}

		return result;
	}

	/**
	 * Gets a {@link Date} from {@code parameter} in {@code properties}. This method will trying parsing the first non-null value it gets according to the format specified either in {@link ParameterDate#getDateFormat()} or in {@link
	 * ParameterDate#getParameterFormat()}.
	 *
	 * @param parameter
	 * 	A {@link ParameterDate}, can not be null
	 * @param properties
	 * 	An instance of {@link HasProperties}, can not be null
	 *
	 * @return An instance of {@link Date} or null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties is null}
	 * @throws IllegalParameterDateException
	 * 	if the parameter value can not be parsed to a date
	 */
	static Date getDate(@Nonnull final ParameterDate parameter, @Nonnull final HasProperties properties) {
		final DateFormat dateFormat;
		final Parameter parameterFormat;
		final String parameterValue;
		DateFormat parameterDateFormat = null;
		Date result;

		Preconditions.checkArgument(parameter != null, "null parameter");
		Preconditions.checkArgument(properties != null, "null properties");
		parameterValue = properties.get(parameter.getKey());

		if (parameterValue == null) {
			return null;
		}

		dateFormat = parameter.getDateFormat();
		parameterFormat = parameter.getParameterFormat();

		try {
			if (parameterFormat == null) {
				result = ParameterDate.getDate(dateFormat, parameterValue);
			} else {
				parameterDateFormat = ParameterDate.getDateFormat(parameterFormat, properties);

				if (parameterDateFormat == null) {
					result = ParameterDate.getDate(dateFormat, parameterValue);
				} else {
					try {
						result = ParameterDate.getDate(parameterDateFormat, parameterValue);
					} catch (final IllegalArgumentException ex) {
						result = ParameterDate.getDate(dateFormat, parameterValue);
					}
				}
			}
		} catch (final IllegalArgumentException ex) {
			final DateFormat reportedFormat = parameterDateFormat == null ? dateFormat : parameterDateFormat;

			throw IllegalParameterDateException.illegalValue(parameter, reportedFormat, parameterValue, ex);
		}

		return result;
	}

	/**
	 * Gets a date format from {@code parameter} in {@code properties}. This method will use the format found if it is not null, otherwise it returns null.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 * @param properties
	 * 	An instance of {@link HasProperties}, can not be null
	 *
	 * @return A {@link DateFormat} or null if the parameter is not found
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties is null}
	 * @throws IllegalParameterException
	 * 	if the value retrieved is not a valid {@link DateFormat}
	 */
	static DateFormat getDateFormat(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		final String parameterValue;
		DateFormat result = null;

		Preconditions.checkArgument(parameter != null, "null parameter");
		Preconditions.checkArgument(properties != null, "null properties");
		parameterValue = properties.get(parameter.getKey(), parameter.getDefaultValue());

		if (parameterValue != null) {
			try {
				result = new SimpleDateFormat(parameterValue);
			} catch (final IllegalArgumentException ex) {
				throw IllegalParameterException.illegalValue(parameter, "date", parameterValue, ex);
			}
		}

		return result;
	}

	/**
	 * Constructs a {@link ParameterDate} with the provided {@code key} and {@code dateFormat}.
	 *
	 * @param key
	 * 	A {@link String} key, can not be null
	 * @param dateFormat
	 * 	An instance of {@link DateFormat}, can not be null
	 *
	 * @return A new instance of {@link ParameterDate}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code key} is null
	 * @throws IllegalArgumentException
	 * 	if {@code dateFormat} is null
	 */
	@Nonnull
	static ParameterDate withDateFormat(@Nonnull final String key, final DateFormat dateFormat) {
		return new ParameterDateImpl(key, dateFormat);
	}

	/**
	 * Constructs a {@link ParameterDate} for the provided {@code key}, with the system default date format instance {@link DateFormat#getDateInstance()}.
	 *
	 * @param key
	 * 	A {@link String} key, can not be null
	 *
	 * @return A new instance of {@link ParameterDate}
	 */
	@Nonnull
	static ParameterDate withKeyValue(@Nonnull final String key) {
		return withDateFormat(key, DateFormat.getDateInstance());
	}

	/**
	 * Constructs a {@link ParameterDate} for the provided {@code key}, with the date format specified by the associated value of  {@code parameterFormat}.
	 *
	 * @param key
	 * 	A {@link String key}, can not be null
	 * @param parameterFormat
	 * 	An instance {@link Parameter}, can not be null
	 *
	 * @return A new instance of {@link ParameterDate}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code key} is null
	 * @throws IllegalArgumentException
	 * 	if {@code parameterFormat} is null
	 */
	@Nonnull
	static ParameterDate withParameterFormat(@Nonnull final String key, @Nonnull final Parameter parameterFormat) {
		final ParameterDateImpl result = new ParameterDateImpl(key, DateFormat.getDateTimeInstance());

		result.setParameterFormat(parameterFormat);
		return result;
	}
}
