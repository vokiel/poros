/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.time.Period;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Pools {@link HasParameters} instances into a priority list from the highest (0) to the lowest (n). This is useful if you want to read configurations in different formats and want to access a flat prioritized representation of your
 * program's configurations. This can also be used if you want to load parameters from different sources. For example the classpath and the filesystem.
 */
public class ParametersPool implements HasParameters {
	private final LinkedList<HasParameters> parametersList = new LinkedList<>();

	/**
	 * Constructs a {@link ParametersPool} with the highest priority {@code parameters} first and lowest last. If parameters are null, the pool will be constructed with no initial members.
	 *
	 * @param parameters
	 * 	An array of {@link HasParameters},
	 */
	public ParametersPool(final HasParameters... parameters) {
		if (parameters.length != 0) {
			parametersList.addAll(Arrays.asList(parameters));
		}
	}

	/**
	 * Adds {@code parameters} to this pool membership in first in first queried order.
	 *
	 * @param parameters
	 * 	An instance of {@link HasParameters}, can not be null
	 *
	 * @return {@code this}
	 */
	@Nonnull
	public ParametersPool add(@Nonnull final HasParameters parameters) {
		Preconditions.checkArgument(parameters != null, "null parameters");
		parametersList.add(parameters);
		return this;
	}

	/**
	 * Clears the pool of all parameters.
	 *
	 * @return {@code this}
	 */
	public ParametersPool clear() {
		parametersList.clear();
		return this;
	}

	@Nonnull
	@Override
	public Optional<String> find(@Nonnull final Parameter parameter) {
		final Iterable<String> values = Iterables.transform(parametersList, parameters -> parameters == null ? null : parameters.get(parameter));
		final com.google.common.base.Optional<String> firstNonEmpty = Iterables.tryFind(values, Predicates.notNull());

		return firstNonEmpty.toJavaUtil();
	}

	@Nonnull
	@Override
	public Optional<String> find(@Nonnull final ParameterOverride parameter) {
		final Iterable<String> values = Iterables.transform(parametersList, parameters -> parameters == null ? null : parameters.get(parameter));
		final com.google.common.base.Optional<String> firstNonEmpty = Iterables.tryFind(values, Predicates.notNull());

		return firstNonEmpty.toJavaUtil();
	}

	@Override
	public String get(@Nonnull final Parameter parameter) {
		final Optional<String> result = find(parameter);

		return result.orElse(null);
	}

	@Override
	public String get(@Nonnull final ParameterOverride parameter) {
		final Optional<String> result = find(parameter);

		return result.orElse(null);
	}

	@Override
	public Date getDate(@Nonnull final ParameterDate parameter) {
		final Iterable<Date> lookForDates = Iterables.transform(parametersList, params -> params == null ? null : params.getDate(parameter));
		final Optional<Date> firstNonNull = Iterables.tryFind(lookForDates, Predicates.notNull()).toJavaUtil();

		return firstNonNull.orElse(null);
	}

	@Override
	public double getDouble(@Nonnull final Parameter parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getDouble(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "double");
	}

	@Override
	public double getDouble(@Nonnull final ParameterOverride parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getDouble(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "double");
	}

	@Override
	public float getFloat(@Nonnull final Parameter parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getFloat(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "float");
	}

	@Override
	public float getFloat(@Nonnull final ParameterOverride parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getFloat(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "float");
	}

	@Override
	public int getInt(@Nonnull final Parameter parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getInt(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "int");
	}

	@Override
	public int getInt(@Nonnull final ParameterOverride parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getInt(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "int");
	}

	@Override
	public long getLong(@Nonnull final Parameter parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getLong(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "long");
	}

	@Override
	public long getLong(@Nonnull final ParameterOverride parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getLong(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "long");
	}

	@Override
	public Period getPeriod(@Nonnull final Parameter parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getPeriod(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "period");
	}

	@Override
	public Period getPeriod(@Nonnull final ParameterOverride parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getPeriod(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "period");
	}

	@Override
	public short getShort(@Nonnull final Parameter parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getShort(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "short");
	}

	@Override
	public short getShort(@Nonnull final ParameterOverride parameter) {
		final Optional<String> valueFound = find(parameter);

		if (valueFound.isPresent()) {
			return getShort(parameter, valueFound.get());
		}

		throw IllegalParameterException.nullValue(parameter, "short");
	}

	public boolean isEmpty() {
		return parametersList.isEmpty() || Iterables.all(parametersList, HasParameters::isEmpty);
	}

	@Override
	public boolean isTrue(@Nonnull final Parameter parameter) {
		final Optional<String> valueFound = find(parameter);

		return valueFound.isPresent() && Boolean.valueOf(valueFound.get());
	}

	@Override
	public boolean isTrue(@Nonnull final ParameterOverride parameter) {
		final Optional<String> valueFound = find(parameter);

		return valueFound.isPresent() && Boolean.valueOf(valueFound.get());
	}

	@Nonnull
	@Override
	public Iterable<String> resolve(@Nonnull final Parameter parameter) {
		final List<HasParameters> reversed = Lists.newArrayList(parametersList.descendingIterator());
		final Iterable<Iterable<String>> unmerged = Iterables.transform(reversed,
			params -> params == null ? Collections.emptyList() : params.resolve(parameter));

		return Iterables.concat(unmerged);
	}

	@Nonnull
	@Override
	public Iterable<String> resolve(@Nonnull final ParameterOverride parameter) {
		final List<HasParameters> reversed = Lists.newArrayList(parametersList.descendingIterator());
		final Iterable<Iterable<String>> unmerged = Iterables.transform(reversed,
			params -> params == null ? Collections.emptyList() : params.resolve(parameter));

		return Iterables.concat(unmerged);
	}

	private double getDouble(@Nonnull final Parameter parameter, final String stringValue) {
		try {
			return Double.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "double", stringValue, ex);
		}
	}

	private float getFloat(@Nonnull final Parameter parameter, final String stringValue) {
		try {
			return Float.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "float", stringValue, ex);
		}
	}

	private int getInt(@Nonnull final Parameter parameter, final String stringValue) {
		try {
			return Integer.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "int", stringValue, ex);
		}
	}

	private long getLong(@Nonnull final Parameter parameter, final String stringValue) {
		try {
			return Long.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "long", stringValue, ex);
		}
	}

	private Period getPeriod(@Nonnull final Parameter parameter, final String stringValue) {
		try {
			return Period.parse(stringValue);
		} catch (final DateTimeParseException ex) {
			throw IllegalParameterException.illegalValue(parameter, "period", stringValue, ex);
		}
	}

	private short getShort(@Nonnull final Parameter parameter, final String stringValue) {
		try {
			return Short.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "short", stringValue, ex);
		}
	}
}
