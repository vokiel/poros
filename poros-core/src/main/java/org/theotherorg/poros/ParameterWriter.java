/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import javax.annotation.Nonnull;

/**
 * Contract used to write the representation of {@link Parameter}s as {@link String} relationships.
 */
public interface ParameterWriter {
	/**
	 * Writes {@code parameter} in a string representation. Syntax is implementation dependent. If {@code parameter} is null this method should still return a valid non-null value.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}
	 *
	 * @return A non-null {@link String}
	 */
	@Nonnull
	String write(Parameter parameter);

	/**
	 * Writes {@code parameterOverride} in a string representation. Syntax is implementation dependent. If {@code parameterOverride} is null this method should still return a valid non-null value.
	 *
	 * @param parameterOverride
	 * 	An instance of {@link ParameterOverride}
	 *
	 * @return A non-null {@link String}
	 */
	@Nonnull
	String write(ParameterOverride parameterOverride);

	/**
	 * Writes {@code parameterDate} in a string representation. Syntax is implementation dependent. If {@code parameterDate} is null this method should still return a valid non-null value.
	 *
	 * @param parameterDate
	 * 	An instance of {@link ParameterDate}
	 *
	 * @return A non-null {@link String}
	 */
	@Nonnull
	String write(ParameterDate parameterDate);

	/**
	 * Writes a structured path of where {@code parameter} can be found in {@code parameters}. Syntax is implementation dependent.
	 *
	 * @param parameters
	 * 	An instance of {@link HasParameters}, can not be null.
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null.
	 *
	 * @return A non-null {@link String}
	 */
	@Nonnull
	String write(@Nonnull HasParameters parameters, @Nonnull Parameter parameter);

	/**
	 * Writes the parent content of {@code childParameter} has a string structured representation. Syntax is implementation dependent. If {@code childParameter} is null this method should still return a valid non-null value.
	 *
	 * @param childParameter
	 * 	An instance of {@link ChildParameter}
	 *
	 * @return A non-null {@link String}
	 */
	@Nonnull
	String writeParent(@Nonnull ChildParameter childParameter);
}
