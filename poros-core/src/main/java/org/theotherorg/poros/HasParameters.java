/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.time.Period;
import java.util.Date;
import java.util.Optional;

import javax.annotation.Nonnull;

/**
 * Contract specification for the retrieval of {@link Parameter} and {@link ParameterOverride} values.
 */
public interface HasParameters {
	/**
	 * Finds the first non-empty value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return An {@link Optional} of {@link String}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 */
	@Nonnull
	Optional<String> find(@Nonnull Parameter parameter);

	/**
	 * Finds the first non-empty value for {@code parameter} from all available key value pairs. This method will prioritize lookup in the order specified by {@link ParameterOverride}.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return An {@link Optional} of {@link String}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 */
	@Nonnull
	Optional<String> find(@Nonnull ParameterOverride parameter);

	/**
	 * Gets a non-empty value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return A {@link String} value or null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 */
	String get(@Nonnull Parameter parameter);

	/**
	 * Gets a non-empty value for {@code parameter} from all available key value pairs. This method will prioritize lookup in the order specified by {@link ParameterOverride}.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return A {@link String} value or null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 */
	String get(@Nonnull ParameterOverride parameter);

	/**
	 * Gets a {@link Date} value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return A {@link Date} value or null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a date
	 */
	Date getDate(@Nonnull ParameterDate parameter);

	/**
	 * Gets a double value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return A double value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a double
	 */
	double getDouble(@Nonnull Parameter parameter);

	/**
	 * Gets a double value for {@code parameter} from all available key value pairs. This method will prioritize lookup in the order specified by {@link ParameterOverride}.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return A double value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a double
	 */
	double getDouble(@Nonnull ParameterOverride parameter);

	/**
	 * Gets a float value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return A float value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a float
	 */
	float getFloat(@Nonnull Parameter parameter);

	/**
	 * Gets a float value for {@code parameter} from all available key value pairs. This method will prioritize lookup in the order specified by {@link ParameterOverride}.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return A float value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a float
	 */
	float getFloat(@Nonnull ParameterOverride parameter);

	/**
	 * Gets an int value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return An int value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not an int
	 */
	int getInt(@Nonnull Parameter parameter);

	/**
	 * Gets an int value for {@code parameter} from all available key value pairs. This method will prioritize lookup in the order specified by {@link ParameterOverride}.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return An int value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not an int
	 */
	int getInt(@Nonnull ParameterOverride parameter);

	/**
	 * Gets a long value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return A long value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a long
	 */
	long getLong(@Nonnull Parameter parameter);

	/**
	 * Gets a long value for {@code parameter} from all available key value pairs. This method will prioritize lookup in the order specified by {@link ParameterOverride}.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return A long value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a long
	 */
	long getLong(@Nonnull ParameterOverride parameter);

	/**
	 * Gets a {@link Period} value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return An instance of {@link Period}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not {@link Period}
	 */
	Period getPeriod(@Nonnull Parameter parameter);

	/**
	 * Gets a {@link Period} value for {@code parameter} from all available key value pairs. This method will prioritize lookup in the order specified by {@link ParameterOverride}.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return An instance of {@link Period}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a short
	 */
	Period getPeriod(@Nonnull ParameterOverride parameter);

	/**
	 * Gets a short value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return A short value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a short
	 */
	short getShort(@Nonnull final Parameter parameter);

	/**
	 * Gets a short value for {@code parameter} from all available key value pairs. This method will prioritize lookup in the order specified by {@link ParameterOverride}.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return A short value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the {@code parameter} value is not a short
	 */
	short getShort(@Nonnull final ParameterOverride parameter);

	/**
	 * Checks if the underlying local parameters are empty
	 *
	 * @return true if empty, false otherwise
	 */
	boolean isEmpty();

	/**
	 * Gets a boolean value for {@code parameter} from the loaded key value pairs only.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return A boolean value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 */
	boolean isTrue(@Nonnull final Parameter parameter);

	/**
	 * Gets a boolean value for {@code parameter} from all available parameters. This method will prioritize lookup in the order specified by {@link ParameterOverride}.
	 *
	 * @param parameter
	 * 	An instance of {@link ParameterOverride}, can not be null
	 *
	 * @return A boolean value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 */
	boolean isTrue(@Nonnull final ParameterOverride parameter);

	/**
	 * Returns a list of locations in which the provided {@code parameter} appears.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return An {@link Iterable} of {@link String}
	 */
	@Nonnull
	Iterable<String> resolve(@Nonnull Parameter parameter);

	/**
	 * Returns a list of locations in which the provided {@code parameter} appears.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 *
	 * @return An {@link Iterable} of {@link String}
	 */
	@Nonnull
	Iterable<String> resolve(@Nonnull ParameterOverride parameter);
}
