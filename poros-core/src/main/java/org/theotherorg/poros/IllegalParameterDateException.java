/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.Nonnull;

/**
 * Thrown to indicate the value of a {@link ParameterDate} could not be read according to its handling and specification.
 */
public final class IllegalParameterDateException extends IllegalParameterException {
	private static final long serialVersionUID = 7948565064729984759L;

	static class DateMessageBuilder extends IllegalParameterException.MessageBuilder {
		private String format;

		public DateMessageBuilder() {
			super("illegal");
		}

		public void setFormat(final String format) {
			this.format = format;
		}

		@Nonnull
		@Override
		public String toString() {
			final StringBuilder result = new StringBuilder(super.toString());

			if (format != null) {
				result.append(", format: [").append(format).append(']');
			}

			return result.toString();
		}
	}

	IllegalParameterDateException(final Parameter parameter, final String message, final Throwable t) {
		super(parameter, message, t);
	}

	/**
	 * Constructs an IllegalParameterDateException using non-null {@code parameter}, a {@link DateFormat}, the {@code illegalValue} and potentially the cause of this exception.
	 *
	 * @param parameter
	 * 	A non-null instance of {@link Parameter}
	 * @param format
	 * 	A {@link DateFormat}, can not be null
	 * @param illegalValue
	 * 	A value {@link String}, can be null
	 * @param t
	 * 	An instance of {@link Throwable}, can be null
	 *
	 * @return An instance of {@link IllegalParameterDateException}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	@Nonnull
	public static IllegalParameterDateException illegalValue(@Nonnull final ParameterDate parameter, @Nonnull final DateFormat format, final String illegalValue, final Throwable t) {
		final DateMessageBuilder messageBuilder = new DateMessageBuilder();

		if (format instanceof SimpleDateFormat) {
			messageBuilder.setFormat(((SimpleDateFormat)format).toPattern());
		}

		return new IllegalParameterDateException(parameter, messageBuilder.parameter(parameter).value(illegalValue).toString(), t);
	}
}
