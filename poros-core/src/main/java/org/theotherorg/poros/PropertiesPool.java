/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Pools properties instances into a priority list from the highest (0) to the lowest (n).
 */
public class PropertiesPool implements Iterable<HasProperties>, HasProperties {
	private final LinkedList<HasProperties> propertiesList = new LinkedList<>();

	/**
	 * Constructs a PropertiesPool with the highest priority {@code properties} first and lowest last. If properties are null, the pool will be constructed with no initial members.
	 *
	 * @param properties
	 * 	An array of {@link Properties},
	 */
	public PropertiesPool(final HasProperties... properties) {
		if (properties.length != 0) {
			propertiesList.addAll(Arrays.asList(properties));
		}
	}

	/**
	 * Adds {@code properties} to this pool membership in first in first queried order.
	 *
	 * @param properties
	 * 	An instance of {@link HasProperties}, can not be null
	 *
	 * @return {@code this}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 */
	@Nonnull
	public PropertiesPool add(@Nonnull final HasProperties properties) {
		Preconditions.checkArgument(properties != null, "null properties");
		propertiesList.add(properties);
		return this;
	}

	/**
	 * Adds {@code properties} to this pool membership in first in first queried order.
	 *
	 * @param properties
	 * 	An instance of {@link HasProperties}, can not be null
	 *
	 * @return {@code this}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 */
	@Nonnull
	public PropertiesPool add(@Nonnull final Properties properties) {
		propertiesList.add(new HasPropertiesAdapter(properties));
		return this;
	}

	/**
	 * Adds {@code properties} to this pool membership in first in first queried order with {@code name} as resolving name.
	 *
	 * @param properties
	 * 	An instance of {@link HasProperties}, can not be null
	 * @param name
	 * 	A non-null {@link String} name
	 *
	 * @return {@code this}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 */
	@Nonnull
	public PropertiesPool add(@Nonnull final Properties properties, @Nonnull final String name) {
		propertiesList.add(new HasPropertiesAdapter(properties, name));
		return this;
	}

	/**
	 * Clears all properties from this pool
	 */
	public void clear() {
		propertiesList.clear();
	}

	/**
	 * When finding a property {@code key}, the pool iterates over all pool members until a non-empty value for the corresponding key is found.
	 *
	 * @param key
	 * 	A {@link String} key, can not be null
	 *
	 * @return An {@link Optional} containing the value found or null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code key} is null
	 */
	@Nonnull
	public Optional<String> find(@Nonnull final String key) {
		final Iterable<String> foundValues;

		Preconditions.checkArgument(key != null, "null key");
		foundValues = Iterables.transform(propertiesList, properties -> properties == null ? null : properties.get(key));
		return Iterables.tryFind(foundValues, Predicates.not(Strings::isNullOrEmpty)).toJavaUtil();
	}

	@Override
	public String get(@Nonnull final String key, final String defaultValue) {
		return find(key).orElse(defaultValue);
	}

	@Override
	public String get(@Nonnull final String key) {
		return find(key).orElse(null);
	}

	@Override
	public String getName() {
		return "pool";
	}

	@Override
	public boolean isEmpty() {
		if (propertiesList.isEmpty()) {
			return true;
		}

		return Iterables.all(propertiesList, properties -> properties == null || properties.isEmpty());
	}

	@Nonnull
	@Override
	public Iterator<HasProperties> iterator() {
		return propertiesList.iterator();
	}

	@Nonnull
	@Override
	public Iterable<String> resolve(@Nonnull final String key) {
		final Iterable<HasProperties> reversedList = Lists.newArrayList(propertiesList.descendingIterator());
		final Iterable<Iterable<String>> propertiesResolutions = Iterables.transform(reversedList,
			input -> input == null ? Collections.emptyList() : input.resolve(key)
		);
		final Iterable<Iterable<String>> nonEmpties = Iterables.filter(propertiesResolutions,
			input -> input != null && !Iterables.isEmpty(input)
		);
		final Iterable<String> indentedProjections = Iterables.transform(Iterables.concat(nonEmpties), input -> "\t" + input);
		final List<String> result = new ArrayList<>(propertiesList.size() + 1);

		result.add(getName() + ":");
		Iterables.addAll(result, indentedProjections);
		return result;
	}
}
