/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.util.Properties;

import javax.annotation.Nonnull;

/**
 * Contract used to read and parse {@link Properties} from {@link String} resource names.
 */
public interface PropertiesLoader {
	/**
	 * Loads {@link Properties} from {@code resourceName}.
	 *
	 * @param resourceName
	 * 	A non-null {@link String} name
	 *
	 * @return An instance of {@link Properties}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code resourceName} is not accepted
	 */
	@Nonnull
	Properties load(@Nonnull String resourceName);
}
