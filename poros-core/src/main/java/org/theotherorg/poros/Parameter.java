/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.io.Serializable;
import java.time.Period;
import java.time.format.DateTimeParseException;
import java.util.Properties;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * A parameter is defined by a {@link String} key and a default value.
 */
public interface Parameter extends Serializable {
	/**
	 * The default value is read as a {@link String} for all parameters. It can be null.
	 *
	 * @return A nullable {@link String}
	 */
	String getDefaultValue();

	/**
	 * The key of a parameter, which can not be null.
	 *
	 * @return A non-null {@link String}
	 */
	@Nonnull
	String getKey();

	/**
	 * Resolves this parameter in the provided {@code parameters}.
	 *
	 * @param parameters
	 * 	An instance of {@link HasParameters}
	 *
	 * @return An {@link Iterable} of {@link String}
	 */
	Iterable<String> resolveIn(HasParameters parameters);

	/**
	 * Writes this parameter in a {@link String} representation according to {@code writer}.
	 *
	 * @param writer
	 * 	An instance of {@link ParameterWriter}, can not be null
	 *
	 * @return A {@link String} value or null
	 */
	String write(ParameterWriter writer);

	/**
	 * Converts a {@code properties} value to a boolean type for the {@code parameter} provided. If the {@code parameter} is not found or if {@code properties} is null, the default value is converted. Conversion is done according to {@link
	 * Boolean#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param properties
	 * 	The {@link Properties} to look into, can not be null
	 *
	 * @return true if the {@code parameter} evaluates to true, false otherwise, null is always false
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 */
	static boolean getBoolProperty(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		final String stringValue = getProperty(parameter, properties);

		return Boolean.valueOf(stringValue);
	}

	/**
	 * Converts a {@code properties} value to a double type for the {@code parameter} provided. If the {@code parameter} is not found or if {@code properties} is null, the default value is converted. Conversion is done according to {@link
	 * Double#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param properties
	 * 	The {@link HasProperties} to look into, can not be null
	 *
	 * @return the double value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to a double
	 */
	static double getDoubleProperty(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		final String stringValue = getRawProperty(parameter, properties);
		double result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "double");
		}

		try {
			result = Double.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "double", stringValue, ex);
		}

		return result;
	}

	/**
	 * Converts a {@code properties} value to a float type for the {@code parameter} provided. If the {@code parameter} is not found or if {@code properties} is null, the default value is converted. Conversion is done according to {@link
	 * Float#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param properties
	 * 	The {@link HasProperties} to look into, can not be null
	 *
	 * @return the float value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to a float
	 */
	static float getFloatProperty(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		final String stringValue = getRawProperty(parameter, properties);
		float result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "float");
		}

		try {
			result = Float.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "float", stringValue, ex);
		}

		return result;
	}

	/**
	 * Converts a {@code properties} value to an integer type for the {@code parameter} provided. If the {@code parameter} is not found or if {@code properties} is null, the default value is converted. Conversion is done according to {@link
	 * Integer#valueOf(String)}, but the value is trimmed before-hand.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param properties
	 * 	The {@link HasProperties} to look into, can not be null
	 *
	 * @return the integer value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to an int
	 */
	static int getIntProperty(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		final String stringValue = Parameter.getProperty(parameter, properties);
		int result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "int");
		}

		try {
			result = Integer.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "int", stringValue, ex);
		}

		return result;
	}

	/**
	 * Converts a {@code properties} value to an long type for the {@code parameter} provided. If the {@code parameter} is not found or if {@code properties} is null, the default value is converted. Conversion is done according to {@link
	 * Long#valueOf(String)}, but the value is trimmed before-hand.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param properties
	 * 	The {@link HasProperties} to look into, can not be null
	 *
	 * @return the long value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to a long
	 */
	static long getLongProperty(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		final String stringValue = Parameter.getProperty(parameter, properties);
		long result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "long");
		}

		try {
			result = Long.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "long", stringValue, ex);
		}

		return result;
	}

	/**
	 * Converts a {@code properties} value to a {@link Period} type for the {@code parameter} provided. If the {@code parameter} is not found, its default value is returned. If {@code properties} is null,the default value is converted.
	 * Conversion is done according to {@link Period#parse(CharSequence)}.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 * @param properties
	 * 	An instance of {@link HasProperties}, can not be null
	 *
	 * @return An instance of {@link Period} or null.
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 * @throws IllegalParameterException
	 * 	if the value can not be parsed to a {@link Period}
	 */
	static Period getPeriodProperty(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		final String stringValue = Parameter.getProperty(parameter, properties);
		Period result = null;

		if (stringValue != null) {
			try {
				result = Period.parse(stringValue);
			} catch (final DateTimeParseException ex) {
				throw IllegalParameterException.illegalValue(parameter, "period", stringValue, ex);
			}
		}

		return result;
	}

	/**
	 * Retrieves the {@code properties} value trimmed as returned with {@link String#trim()} associated with the provided {@code parameter}. If the {@code parameter} is not found or if {@code properties} is null, the default value is
	 * returned. The value of a string parameter is always nullable. Empty strings will converted to null.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param properties
	 * 	The {@link HasProperties} to look into, can not be null
	 *
	 * @return A {@link String} value or null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 */
	static String getProperty(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		final String property = getRawProperty(parameter, properties);

		return property == null ? null : Strings.emptyToNull(property.trim());
	}

	/**
	 * Retrieves the {@code properties} value unmodified associated with the provided {@code parameter}. If the {@code parameter} is not found or if {@code properties} is null, the default value is returned. The value of a string parameter
	 * is always nullable.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param properties
	 * 	The {@link HasProperties} to look into, can not be null
	 *
	 * @return A {@link String} value or null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 */
	static String getRawProperty(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		Preconditions.checkArgument(parameter != null, "null parameter");
		Preconditions.checkArgument(properties != null, "null properties");
		return properties.get(parameter.getKey(), parameter.getDefaultValue());
	}

	/**
	 * Converts a {@code properties} value to a short type for the {@code parameter} provided. If the {@code parameter} is not found, its default value is returned. If {@code properties} is null,the default value is converted. Conversion is
	 * done according to {@link Short#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param properties
	 * 	The {@link HasProperties} to look into, can not be null
	 *
	 * @return the short value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to a short
	 */
	static short getShortProperty(@Nonnull final Parameter parameter, @Nonnull final HasProperties properties) {
		final String stringValue = getProperty(parameter, properties);
		short result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "short");
		}

		try {
			result = Short.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "short", stringValue, ex);
		}

		return result;
	}

	/**
	 * Constructs a {@link Parameter} instance from the specified {@code prototype} overriding its key value with {@code key}.
	 *
	 * @param prototype
	 * 	An instance of {@link Parameter} to override, can not be null
	 * @param key
	 * 	A {@link String} key, can not be null
	 *
	 * @return A new instance of {@link Parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code prototype} is null
	 * @throws IllegalArgumentException
	 * 	if {@code key} is null
	 */
	@Nonnull
	static Parameter withKey(@Nonnull final Parameter prototype, @Nonnull final String key) {
		return new ParameterImpl(prototype, key);
	}

	/**
	 * Constructs a {@link Parameter} instance from the provided {@code key} and {@code defaultValue}.
	 *
	 * @param key
	 * 	A {@link String} key, can not be null
	 * @param defaultValue
	 * 	A nullable {@link String} value
	 *
	 * @return A new instance of {@link Parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code key} is null
	 */
	@Nonnull
	static Parameter withKeyValue(@Nonnull final String key, final String defaultValue) {
		return new ParameterImpl(key, defaultValue);
	}
}
