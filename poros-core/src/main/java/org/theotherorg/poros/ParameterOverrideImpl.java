/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

/**
 * The default implementation of {@link ParameterOverride}.
 */
class ParameterOverrideImpl extends ParameterImpl implements ParameterOverride {
	private static final long serialVersionUID = 8582217982353874719L;
	private final String envKey;

	public ParameterOverrideImpl(@Nonnull final String key, @Nonnull final String envKey, final String defaultValue) {
		super(key, defaultValue);
		Preconditions.checkArgument(envKey != null, "null envKey");
		this.envKey = envKey;
	}

	@Nonnull
	@Override
	public String getEnvKey() {
		return envKey;
	}

	@Override
	public Iterable<String> resolveIn(final HasParameters parameters) {
		return parameters.resolve(this);
	}

	@Override
	public String write(final ParameterWriter writer) {
		return writer.write(this);
	}
}
