/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

/**
 * A base {@link PropertiesLoader} implementation which loads {@link Properties} objects from a {@link ResourceStreams}.
 */
public class BasePropertiesLoader implements PropertiesLoader {
	private final ResourceStreams resourceStreams;

	/**
	 * Constructs a base properties loader using the provided {@code resourceStreams}.
	 *
	 * @param resourceStreams
	 * 	A {@link ResourceStreams}, can not be null
	 */
	public BasePropertiesLoader(@Nonnull final ResourceStreams resourceStreams) {
		Preconditions.checkArgument(resourceStreams != null, "null resourceStreams");
		this.resourceStreams = resourceStreams;
	}

	/**
	 * Loads the properties from a classpath resource by {@code resourceName}.
	 *
	 * @param resourceName
	 * 	A non-null {@link String} name
	 *
	 * @return An instance of {@link Properties}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code resourceName} is null
	 * @throws IllegalResourceException
	 * 	if {@code resourceName} can not be loaded
	 */
	@Nonnull
	@Override
	public Properties load(@Nonnull final String resourceName) {
		final Properties result = new Properties();

		try {
			load(resourceName, result);
		} catch (final NullPointerException ex) {
			throw new IllegalArgumentException("illegal resourceName", ex);
		}

		return result;
	}

	/**
	 * Loads the specified {@code resourceName} into the provided {@code properties}.
	 *
	 * @param resourceName
	 * 	A non-null {@link String} name
	 * @param properties
	 * 	An instance of {@link Properties}, can not be null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code resourceName} is null
	 * @throws IllegalArgumentException
	 * 	if {@code properties} is null
	 * @throws IllegalResourceException
	 * 	if {@code resourceName} could not be loaded
	 */
	public void load(@Nonnull final String resourceName, @Nonnull final Properties properties) {
		try (final InputStream resourceStream = resourceStreams.getInputStream(resourceName)) {
			if (resourceStream == null) {
				throw IllegalResourceException.from(resourceName);
			} else {
				load(resourceStream, properties);
			}
		} catch (final IOException | IllegalStateException ex) {
			throw IllegalResourceException.from(resourceName, ex);
		}
	}

	/**
	 * Loads the specified {@code resourceStream} into the provided {@code properties}.
	 *
	 * @param resourceStream
	 * 	An {@link InputStream}, can not be null
	 * @param properties
	 * 	An instance of {@link Properties}, can not be null
	 *
	 * @throws NullPointerException
	 * 	if {@code resourceStream} is null
	 * @throws NullPointerException
	 * 	if {@code properties} is null
	 * @throws IllegalStateException
	 * 	if the stream can not be loaded
	 */
	public void load(@Nonnull final InputStream resourceStream, @Nonnull final Properties properties) {
		Objects.requireNonNull(resourceStream, "null resourceStream");
		Objects.requireNonNull(properties, "null properties");

		try {
			properties.load(resourceStream);
		} catch (final IOException ex) {
			throw new IllegalStateException("Could not load resourceStream", ex);
		}
	}
}
