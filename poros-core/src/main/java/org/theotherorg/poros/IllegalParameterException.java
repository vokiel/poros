/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * Thrown to indicate the value of a {@link Parameter} could not be read according to its handling and specification.
 */
public class IllegalParameterException extends IllegalArgumentException {
	private static final long serialVersionUID = -7903254410736728055L;

	static class MessageBuilder {
		private final StringBuilder messageBuilder;
		private Parameter parameter;
		private String type;
		private String value;

		public MessageBuilder(final String prefix) {
			this.messageBuilder = new StringBuilder(prefix);
		}

		public MessageBuilder parameter(@Nonnull final Parameter parameter) {
			Preconditions.checkArgument(parameter != null, "null parameter");
			this.parameter = parameter;
			return this;
		}

		@Nonnull
		@Override
		public String toString() {
			if (Strings.isNullOrEmpty(type)) {
				messageBuilder.append(" object");
			} else {
				messageBuilder.append(" ").append(type);
			}

			messageBuilder.append(" parameter value for key ");
			messageBuilder.append('[').append(parameter.getKey()).append(']');

			if (value != null) {
				messageBuilder.append(", value: ");
				messageBuilder.append('[').append(value).append(']');
			}

			return messageBuilder.toString();
		}

		public MessageBuilder type(final String type) {
			this.type = type;
			return this;
		}

		public MessageBuilder value(final String value) {
			this.value = value;
			return this;
		}
	}

	private final Parameter parameter;

	IllegalParameterException(final Parameter parameter, final String message) {
		super(message);
		this.parameter = parameter;
	}

	IllegalParameterException(final Parameter parameter, final String message, final Throwable t) {
		super(message, t);
		this.parameter = parameter;
	}

	public Parameter getParameter() {
		return parameter;
	}

	/**
	 * Constructs an IllegalParameterException using non-null {@code parameter}, a {@code type} string, the {@code illegalValue} and potentially the cause of this exception.
	 *
	 * @param parameter
	 * 	A non-null instance of {@link Parameter}
	 * @param type
	 * 	A type {@link String}, can be null
	 * @param illegalValue
	 * 	A value {@link String}, can be null
	 * @param t
	 * 	An instance of {@link Throwable}, can be null
	 *
	 * @return An instance of {@link IllegalParameterException}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	public static IllegalParameterException illegalValue(@Nonnull final Parameter parameter, final String type, final String illegalValue, final Throwable t) {
		final MessageBuilder messageBuilder = new MessageBuilder("illegal").type(type);
		final String message = messageBuilder.parameter(parameter).value(illegalValue).toString();

		return new IllegalParameterException(parameter, message, t);
	}

	/**
	 * Constructs an IllegalParameterException using non-null {@code parameter} and {@code type} string. {@link Parameter} values that evaluate to null, can not be returned cause this exception form to be thrown.
	 *
	 * @param parameter
	 * 	A non-null instance of {@link Parameter}
	 * @param type
	 * 	A type {@link String}, can be null
	 *
	 * @return An instance of {@link IllegalParameterException}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	public static IllegalParameterException nullValue(@Nonnull final Parameter parameter, final String type) {
		final MessageBuilder messageBuilder = new MessageBuilder("null").type(type).parameter(parameter);

		return new IllegalParameterException(parameter, messageBuilder.toString());
	}
}
