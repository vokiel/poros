/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.time.Period;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * A parameter override extends the basic {@link Parameter} functionality with an environment variable key.
 * <p>
 * The environment key is only checked against the environment variables from {@link System#getenv()}. Overridden parameter values are retrieved in order of priority:
 * <ol>
 * <li>Arguments</li>
 * <li>Java system properties (key)</li>
 * <li>Environment variables (envKey,key)</li>
 * <li>Loaded key value pairs (key))</li>
 * </ol>
 */
public interface ParameterOverride extends Parameter {
	/**
	 * The environment key of a parameter can not be null.
	 *
	 * @return A non-null {@link String}
	 */
	@Nonnull
	String getEnvKey();

	/**
	 * Converts a value for {@code parameter} to a boolean type. This method finds a value using the {@link ParameterOverride} order or converts the {@code parameter} default value. Conversion is done according to {@link
	 * Boolean#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link ParameterOverride} to look for, can not be null
	 * @param confProperties
	 * 	The configuration files {@link HasProperties} to look into, unless null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return true if the value evaluates to true, false otherwise
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	static boolean getBoolProperty(@Nonnull final ParameterOverride parameter, final HasProperties confProperties, final Map<String, String> environment, final Properties systemProperties) {
		final String stringValue = getProperty(parameter, confProperties, environment, systemProperties);

		return Boolean.valueOf(stringValue);
	}

	/**
	 * Converts a value for {@code parameter} to a double type for the. If the {@code parameter} is not found or if {@code properties} is null, the default value is converted. Conversion is done according to {@link Double#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link ParameterOverride} to look for, can not be null
	 * @param confProperties
	 * 	The configuration files {@link HasProperties} to look into, unless null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return the double value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalParameterException
	 * 	if the value is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to a double
	 */
	static double getDoubleProperty(@Nonnull final ParameterOverride parameter, final HasProperties confProperties, final Map<String, String> environment, final Properties systemProperties) {
		final String stringValue = getRawProperty(parameter, confProperties, environment, systemProperties);
		double result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "double");
		}

		try {
			result = Double.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "double", stringValue, ex);
		}

		return result;
	}

	/**
	 * Converts a value for {@code parameter} to a float type. This method finds a value using the {@link ParameterOverride} order or returns the {@code parameter} default value. Conversion is done according to {@link
	 * Float#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link ParameterOverride} to look for, can not be null
	 * @param confProperties
	 * 	The configuration files {@link HasProperties} to look into, unless null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return the float value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalParameterException
	 * 	if the value is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to a float
	 */
	static float getFloatProperty(@Nonnull final ParameterOverride parameter, final HasProperties confProperties, final Map<String, String> environment, final Properties systemProperties) {
		final String stringValue = getRawProperty(parameter, confProperties, environment, systemProperties);
		float result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "float");
		}

		try {
			result = Float.valueOf(stringValue);
		} catch (final NumberFormatException ex) {
			throw IllegalParameterException.illegalValue(parameter, "float", stringValue, ex);
		}

		return result;
	}

	/**
	 * Converts a value for {@code parameter} to an integer type. This method finds a value using the {@link ParameterOverride} order or returns the {@code parameter} default value. Conversion is done according to {@link
	 * Integer#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link ParameterOverride} to look for, can not be null
	 * @param confProperties
	 * 	The configuration files {@link HasProperties} to look into, unless null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return the integer value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalParameterException
	 * 	if the value is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to an int
	 */
	static int getIntProperty(@Nonnull final ParameterOverride parameter, final HasProperties confProperties, final Map<String, String> environment, final Properties systemProperties) {
		final String stringValue = getProperty(parameter, confProperties, environment, systemProperties);
		int result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "int");
		}

		try {
			result = Integer.valueOf(stringValue);
		} catch (final RuntimeException ex) {
			throw IllegalParameterException.illegalValue(parameter, "int", stringValue, ex);
		}

		return result;
	}

	/**
	 * Converts a value for {@code parameter} to a long type. This method finds a value using the {@link ParameterOverride} order or returns the {@code parameter} default value. Conversion is done according to {@link Long#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link ParameterOverride} to look for, can not be null
	 * @param confProperties
	 * 	The configuration files {@link HasProperties} to look into, unless null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return the long value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is not accepted
	 * @throws IllegalParameterException
	 * 	if the value is not accepted
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to a long
	 */
	static long getLongProperty(@Nonnull final ParameterOverride parameter, final HasProperties confProperties, final Map<String, String> environment, final Properties systemProperties) {
		final String stringValue = getProperty(parameter, confProperties, environment, systemProperties);
		long result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "long");
		}

		try {
			result = Long.valueOf(stringValue);
		} catch (final RuntimeException ex) {
			throw IllegalParameterException.illegalValue(parameter, "long", stringValue, ex);
		}

		return result;
	}

	/**
	 * Converts a value for {@code parameter} to a {@link Period}. This method finds a value using the {@link ParameterOverride} order or returns the {@code parameter} default value. Conversion is done according to {@link
	 * Period#parse(CharSequence)}.
	 *
	 * @param parameter
	 * 	The {@link ParameterOverride} to look for, can not be null
	 * @param confProperties
	 * 	The configuration files {@link HasProperties} to look into, unless null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return the {@link Period} of {code parameter} or null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to a {@link Period}
	 */
	static Period getPeriodProperty(@Nonnull final ParameterOverride parameter, final HasProperties confProperties, final Map<String, String> environment, final Properties systemProperties) {
		final String stringValue = getProperty(parameter, confProperties, environment, systemProperties);
		Period result = null;

		if (stringValue != null) {
			try {
				result = Period.parse(stringValue);
			} catch (final RuntimeException ex) {
				throw IllegalParameterException.illegalValue(parameter, "period", stringValue, ex);
			}
		}

		return result;
	}

	/**
	 * Retrieves the value associated with the provided {@code parameter}. This method finds a value from the {@code systemProperties} or the {@code environment} or returns the {@code parameter} default value. The value of a string parameter
	 * can be null, but can not be the empty string.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return A {@link String} value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	static String getProperty(@Nonnull final ParameterOverride parameter, final Map<String, String> environment, final Properties systemProperties) {
		return getProperty(parameter, null, environment, systemProperties);
	}

	/**
	 * Retrieves the value associated with the provided {@code parameter}. This method finds a value using the {@link ParameterOverride} order or returns the {@code parameter} default value. The value of a string parameter can be null, but
	 * can not be the empty string.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param confProperties
	 * 	The configuration files {@link HasProperties} to look into, unless null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return A {@link String} value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	static String getProperty(@Nonnull final ParameterOverride parameter, final HasProperties confProperties, final Map<String, String> environment, final Properties systemProperties) {
		final String result = getRawProperty(parameter, confProperties, environment, systemProperties);

		return result == null ? null : Strings.nullToEmpty(result.trim());
	}

	/**
	 * Retrieves the value unmodified associated with the provided {@code parameter}. This method finds a value using the {@link ParameterOverride} order or returns the {@code parameter} default value. The value of a string parameter can be
	 * null. This method will not trim or nullify empty strings.
	 *
	 * @param parameter
	 * 	The {@link Parameter} to look for, can not be null
	 * @param confProperties
	 * 	The configuration files {@link HasProperties} to look into, unless null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return A {@link String} value or null
	 *
	 * @throws NullPointerException
	 * 	if {@code parameter} is null
	 */
	static String getRawProperty(@Nonnull final ParameterOverride parameter, final HasProperties confProperties, final Map<String, String> environment, final Properties systemProperties) {
		final String parameterKey;
		String result;

		Preconditions.checkArgument(parameter != null, "null parameter");
		parameterKey = parameter.getKey();
		result = systemProperties == null ? null : systemProperties.getProperty(parameterKey);

		if (Strings.isNullOrEmpty(result)) {
			if (environment != null) {
				result = environment.get(parameter.getEnvKey());

				if (Strings.isNullOrEmpty(result)) {
					result = environment.get(parameterKey);
				}
			}

			if (Strings.isNullOrEmpty(result)) {
				final String defaultValue = parameter.getDefaultValue();

				result = confProperties == null ? defaultValue : confProperties.get(parameterKey, defaultValue);
			}
		}

		return result;
	}

	/**
	 * Converts a value for {@code parameter} to a short type. This method finds a value using the {@link ParameterOverride} order or returns the {@code parameter} default value. Conversion is done according to {@link
	 * Short#valueOf(String)}.
	 *
	 * @param parameter
	 * 	The {@link ParameterOverride} to look for, can not be null
	 * @param confProperties
	 * 	The configuration files {@link HasProperties} to look into, unless null
	 * @param environment
	 * 	An environment {@link Map} to look into, unless null
	 * @param systemProperties
	 * 	The system {@link Properties} to look into, unless null
	 *
	 * @return the short value of the {@code parameter}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 * @throws IllegalParameterException
	 * 	if the value is null
	 * @throws IllegalParameterException
	 * 	if the value can not be evaluated to a short
	 */
	static short getShortProperty(@Nonnull final ParameterOverride parameter, final HasProperties confProperties, final Map<String, String> environment, final Properties systemProperties) {
		final String stringValue = getProperty(parameter, confProperties, environment, systemProperties);
		short result;

		if (stringValue == null) {
			throw IllegalParameterException.nullValue(parameter, "short");
		}

		try {
			result = Short.valueOf(stringValue);
		} catch (final RuntimeException ex) {
			throw IllegalParameterException.illegalValue(parameter, "short", stringValue, ex);
		}

		return result;
	}

	/**
	 * Constructs a {@link ParameterOverride} with the provided {@code key}, {@code envKey} and {@code defaultValue}.
	 *
	 * @param key
	 * 	A {@link String} key, which can not be null
	 * @param envKey
	 * 	A {@link String} environment key, which can not be null
	 * @param defaultValue
	 * 	A {@link String} default value
	 *
	 * @return An instance of {@link ParameterOverride}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code key} is null
	 * @throws IllegalArgumentException
	 * 	if {@code envKey} is null
	 */
	@Nonnull
	static ParameterOverride withKeysValue(@Nonnull final String key, @Nonnull final String envKey, final String defaultValue) {
		return new ParameterOverrideImpl(key, envKey, defaultValue);
	}
}
