/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import java.time.Period;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;

/**
 * Composite parameter manager which allows querying for values using {@link System#getProperties()}, {@link System#getenv()} and provided runtime {@link Properties} resources.
 */
public class Parameters implements HasParameters {
	private final Map<String, String> environmentMap;
	private final Properties localProperties = new Properties();
	private final PropertiesLoader propertiesLoader;
	private final PropertiesPool propertiesPool = new PropertiesPool();
	private final Properties systemProperties;

	/**
	 * Initializes a Parameters with a classpath loading strategy for properties resources. This will use {@link System#getenv()} and {@link System#getProperties()} to look for {@link ParameterOverride}s.
	 */
	public Parameters() {
		this(new ClassPathLoader());
	}

	/**
	 * Initializes a Parameters with the provided {@code propertiesLoader} for loading properties resources. This will use {@link System#getenv()} and {@link System#getProperties()} to look for {@link ParameterOverride}s.
	 *
	 * @param propertiesLoader
	 * 	An instance of {@link PropertiesLoader}, can not be null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code propertiesLoader} is null
	 */
	public Parameters(@Nonnull final PropertiesLoader propertiesLoader) {
		this(propertiesLoader, System.getProperties());
	}

	/**
	 * Initializes a Parameters with the provided {@code propertiesLoader} for loading properties resources. This will use {@link System#getenv()} and the provided {@code systemProperties} to look for {@link ParameterOverride}s.
	 *
	 * @param propertiesLoader
	 * 	An instance of {@link PropertiesLoader}, can not be null
	 * @param systemProperties
	 * 	An instance of {@link Properties}, can not be null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code propertiesLoader} is null
	 * @throws IllegalArgumentException
	 * 	if {@code systemProperties} is null
	 */
	public Parameters(@Nonnull final PropertiesLoader propertiesLoader, @Nonnull final Properties systemProperties) {
		this(propertiesLoader, System.getenv(), systemProperties);
	}

	/**
	 * Initializes a Parameters with the provided {@code propertiesLoader} for loading properties resources. This will use the provided {@code environmentMap} and the provided {@code systemProperties} to look for {@link ParameterOverride}s.
	 *
	 * @param propertiesLoader
	 * 	An instance of {@link PropertiesLoader}, can not be null
	 * @param systemProperties
	 * 	An instance of {@link Properties}, can not be null
	 * @param environmentMap
	 * 	An instance of a {@link Map} of {@link String} keys and values, can not be null
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code propertiesLoader} is null
	 * @throws IllegalArgumentException
	 * 	if {@code environmentMap} is null
	 * @throws IllegalArgumentException
	 * 	if {@code systemProperties} is null
	 */
	public Parameters(@Nonnull final PropertiesLoader propertiesLoader, @Nonnull final Map<String, String> environmentMap, @Nonnull final Properties systemProperties) {
		Preconditions.checkArgument(propertiesLoader != null, "null propertiesLoader");
		Preconditions.checkArgument(environmentMap != null, "null environmentMap");
		Preconditions.checkArgument(systemProperties != null, "null systemProperties");
		this.propertiesLoader = propertiesLoader;
		this.environmentMap = environmentMap;
		this.systemProperties = systemProperties;
		propertiesPool.add(localProperties);
	}

	/**
	 * Clears the underlying loaded properties. This will not attempt to modify the environment or system properties.
	 *
	 * @return {@code this}
	 */
	public Parameters clear() {
		propertiesPool.clear();
		return this;
	}

	@Nonnull
	@Override
	public Optional<String> find(@Nonnull final Parameter parameter) {
		return Optional.ofNullable(Parameter.getProperty(parameter, propertiesPool));
	}

	@Nonnull
	@Override
	public Optional<String> find(@Nonnull final ParameterOverride parameter) {
		return Optional.ofNullable(ParameterOverride.getProperty(parameter, propertiesPool, environmentMap, systemProperties));
	}

	@Override
	public String get(@Nonnull final Parameter parameter) {
		return Parameter.getProperty(parameter, propertiesPool);
	}

	@Override
	public String get(@Nonnull final ParameterOverride parameter) {
		return ParameterOverride.getProperty(parameter, propertiesPool, environmentMap, systemProperties);
	}

	@Override
	public Date getDate(@Nonnull final ParameterDate parameter) {
		return ParameterDate.getDate(parameter, propertiesPool);
	}

	@Override
	public double getDouble(@Nonnull final Parameter parameter) {
		return Parameter.getDoubleProperty(parameter, propertiesPool);
	}

	@Override
	public double getDouble(@Nonnull final ParameterOverride parameter) {
		return ParameterOverride.getDoubleProperty(parameter, propertiesPool, environmentMap, systemProperties);
	}

	@Override
	public float getFloat(@Nonnull final Parameter parameter) {
		return Parameter.getFloatProperty(parameter, propertiesPool);
	}

	@Override
	public float getFloat(@Nonnull final ParameterOverride parameter) {
		return ParameterOverride.getFloatProperty(parameter, propertiesPool, environmentMap, systemProperties);
	}

	@Override
	public int getInt(@Nonnull final Parameter parameter) {
		return Parameter.getIntProperty(parameter, propertiesPool);
	}

	@Override
	public int getInt(@Nonnull final ParameterOverride parameter) {
		return ParameterOverride.getIntProperty(parameter, propertiesPool, environmentMap, systemProperties);
	}

	@Override
	public long getLong(@Nonnull final Parameter parameter) {
		return Parameter.getLongProperty(parameter, propertiesPool);
	}

	@Override
	public long getLong(@Nonnull final ParameterOverride parameter) {
		return ParameterOverride.getLongProperty(parameter, propertiesPool, environmentMap, systemProperties);
	}

	@Override
	public Period getPeriod(@Nonnull final Parameter parameter) {
		return Parameter.getPeriodProperty(parameter, propertiesPool);
	}

	@Override
	public Period getPeriod(@Nonnull final ParameterOverride parameter) {
		return ParameterOverride.getPeriodProperty(parameter, propertiesPool, environmentMap, systemProperties);
	}

	@Override
	public short getShort(@Nonnull final Parameter parameter) {
		return Parameter.getShortProperty(parameter, propertiesPool);
	}

	@Override
	public short getShort(@Nonnull final ParameterOverride parameter) {
		return ParameterOverride.getShortProperty(parameter, propertiesPool, environmentMap, systemProperties);
	}

	@Override
	public boolean isEmpty() {
		return propertiesPool.isEmpty();
	}

	@Override
	public boolean isTrue(@Nonnull final Parameter parameter) {
		return Parameter.getBoolProperty(parameter, propertiesPool);
	}

	@Override
	public boolean isTrue(@Nonnull final ParameterOverride parameter) {
		return ParameterOverride.getBoolProperty(parameter, propertiesPool, environmentMap, systemProperties);
	}

	/**
	 * Loads parameter values specified by the value for {@code parameter}. This method looks at {@link System#getProperties()} and {@link System#getenv()} for resource stream name and adds its content into this Parameters configuration
	 * properties. Existing properties are overwritten on sub-sequent calls.
	 *
	 * @param parameter
	 * 	A {@link ParameterOverride} containing the key, environment key and default value to look for
	 *
	 * @return {@code this}
	 *
	 * @throws IllegalResourceException
	 * 	if the value for {@code parameter} is unknown
	 */
	@Nonnull
	public Parameters load(@Nonnull final ParameterOverride parameter) {
		final String resourceName = ParameterOverride.getProperty(parameter, environmentMap, systemProperties);

		if (resourceName == null) {
			throw new IllegalResourceException("unknown resource value");
		}

		return load(resourceName);
	}

	/**
	 * Loads parameter values specified by the value for {@code resourceName}. This method looks for a resource stream and adds its content into this Parameters configuration properties. Previously added key pairs are not overwritten by this
	 * method, but will be overridden by respective parameters.
	 *
	 * @param resourceName
	 * 	A {@link String} resource name, can not be null
	 *
	 * @return {@code this}
	 *
	 * @throws IllegalArgumentException
	 * 	if the specified resource value is null
	 */
	@Nonnull
	public Parameters load(@Nonnull final String resourceName) {
		final Properties resourceProperties;

		Preconditions.checkArgument(resourceName != null, "null resourceName");
		resourceProperties = propertiesLoader.load(resourceName);
		propertiesPool.add(resourceProperties, resourceName);
		return this;
	}

	/**
	 * Adds a local {@code parameter} value to this Parameters configuration. If the value provided is null, this is a no-op. The provided value always has higher precedence than the loaded properties, but less than the environment or system
	 * properties.
	 *
	 * @param parameter
	 * 	An instance of {@link Parameter}, can not be null
	 * @param value
	 * 	A nullable {@link String} value
	 *
	 * @return {@code this}
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	@Nonnull
	public Parameters put(@Nonnull final Parameter parameter, final String value) {
		if (value != null) {
			Preconditions.checkArgument(parameter != null, "null parameter");
			localProperties.put(parameter.getKey(), value);
		}

		return this;
	}

	/**
	 * Removes the value associated with {@code parameter} from this Parameters local properties returning the value previously set, which may be null. This does not alter loaded properties or the environment or the system properties.
	 *
	 * @param parameter
	 * 	A non-null instance of {@link Parameter}
	 *
	 * @return The previously set value
	 *
	 * @throws IllegalArgumentException
	 * 	if {@code parameter} is null
	 */
	public String remove(@Nonnull final Parameter parameter) {
		Preconditions.checkArgument(parameter != null, "null parameter");
		return (String)localProperties.remove(parameter.getKey());
	}

	@Nonnull
	@Override
	public Iterable<String> resolve(@Nonnull final Parameter parameter) {
		final String parameterKey;

		Preconditions.checkArgument(parameter != null, "null parameter");
		parameterKey = parameter.getKey();
		return propertiesPool.resolve(parameterKey);
	}

	@Nonnull
	@Override
	public Iterable<String> resolve(@Nonnull final ParameterOverride parameter) {
		Iterable<String> resolutions = resolve((Parameter)parameter);
		final String envKey = parameter.getEnvKey();
		final String systemValue = systemProperties.getProperty(parameter.getKey());

		if (!Strings.isNullOrEmpty(envKey)) {
			final String envValue = environmentMap.get(envKey);

			if (envValue != null) {
				resolutions = Iterables.concat(resolutions, Collections.singleton("env: [" + envValue + ']'));
			}
		}

		if (systemValue != null) {
			resolutions = Iterables.concat(resolutions, Collections.singleton("sys: [" + systemValue + ']'));
		}

		return resolutions;
	}
}
