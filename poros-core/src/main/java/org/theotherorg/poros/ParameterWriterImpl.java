/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;

/**
 * The default implementation of {@link ParameterWriter} is a braces delimited sequence of [key][defaultValue][envKey] steps.
 */
final class ParameterWriterImpl implements ParameterWriter {
	@Nonnull
	@Override
	public String write(final Parameter parameter) {
		final StringBuilder builder = new StringBuilder("param: ");

		if (parameter == null) {
			builder.append("null");
		} else {
			final String defaultValue = parameter.getDefaultValue();

			builder.append('[').append(parameter.getKey()).append(']');

			if (defaultValue == null) {
				builder.append(" []");
			} else {
				builder.append(" [").append(defaultValue).append(']');
			}
		}

		return builder.toString();
	}

	@Nonnull
	@Override
	public String write(final ParameterOverride parameterOverride) {
		final StringBuilder overrideBuilder = new StringBuilder(write((Parameter)parameterOverride));

		if (parameterOverride != null) {
			overrideBuilder.append(" [").append(parameterOverride.getEnvKey()).append(']');
		}

		return overrideBuilder.toString();
	}

	@Nonnull
	@Override
	public String write(final ParameterDate parameterDate) {
		final StringBuilder builder = new StringBuilder(write((Parameter)parameterDate));

		if (parameterDate != null) {
			final Parameter parameterFormat = parameterDate.getParameterFormat();

			if (parameterFormat != null) {
				builder.append("\n   ");
				builder.append("\u001B[33m").append(write(parameterFormat)).append("\u001B[0m");
			}
		}

		return builder.toString();
	}

	@Nonnull
	@Override
	public String write(@Nonnull final HasParameters parameters, @Nonnull final Parameter parameter) {
		final Iterable<String> paths;
		final String result;

		Preconditions.checkArgument(parameters != null, "null parameters");
		paths = parameter.resolveIn(parameters);

		if (!Iterables.isEmpty(paths)) {
			result = String.join("\n", paths);
		} else {
			result = "\u001B[31mnot found\u001B[0m";
		}

		return result;
	}

	@Nonnull
	@Override
	public String writeParent(@Nonnull final ChildParameter childParameter) {
		Preconditions.checkArgument(childParameter != null, "null childParameter");
		return childParameter.getParent().write(this) + "\n\t";
	}
}
