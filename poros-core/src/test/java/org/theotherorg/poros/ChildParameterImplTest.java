package org.theotherorg.poros;

import java.util.Collections;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.ChildParameters.ChildParameterImpl;

class ChildParameterImplTest {
	@Test
	void testGetDefaultValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedValue = "value";
		final ChildParameterImpl testChild = new ChildParameterImpl(mockedParam, null, expectedValue);

		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		Assertions.assertSame(mockedParam, testChild.getParent());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testGetDefaultValueNull() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedValue = "value";
		final ChildParameterImpl testChild = new ChildParameterImpl(mockedParam, null, null);

		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		Assertions.assertSame(mockedParam, testChild.getParent());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testGetKey() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final ChildParameterImpl testChild = new ChildParameterImpl(mockedParam, expectedKey);

		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertSame(mockedParam, testChild.getParent());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testGetKeyNull() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final ChildParameterImpl testChild = new ChildParameterImpl(mockedParam, null, null);

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertSame(mockedParam, testChild.getParent());
		EasyMock.verify(mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testParentIllegal() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ChildParameterImpl(null, null));
	}

	@Test
	void testResolveIn() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final ChildParameter testChild = new ChildParameterImpl(mockedParam, "key");
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParams.resolve(testChild)).andReturn(Collections.emptyList());
		EasyMock.replay(mockedParams, mockedParam);
		testResolutions = testChild.resolveIn(mockedParams);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertTrue(Iterables.isEmpty(testResolutions));
		EasyMock.verify(mockedParams, mockedParam);
	}

	@Test
	void testWrite() {
		final ParameterWriter mockedWriter = EasyMock.createStrictMock(ParameterWriter.class);
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final ChildParameter testParam = new ChildParameterImpl(mockedParameter, "key");
		final String expectedResult = "result";

		EasyMock.expect(mockedWriter.writeParent(testParam)).andReturn(expectedResult).once();
		EasyMock.expect(mockedWriter.write(testParam)).andReturn("").once();
		EasyMock.replay(mockedWriter, mockedParameter);
		Assertions.assertEquals(expectedResult, testParam.write(mockedWriter));
		EasyMock.verify(mockedWriter, mockedParameter);
	}
}
