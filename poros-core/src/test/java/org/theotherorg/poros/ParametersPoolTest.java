package org.theotherorg.poros;

import java.time.Period;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ParametersPoolTest {
	private HasParameters mockedParameters;
	private ParametersPool testPool;

	@BeforeEach
	void setUp() {
		mockedParameters = EasyMock.createStrictMock(HasParameters.class);
		testPool = new ParametersPool(mockedParameters);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testAddParameters() {
		final ParametersPool testPool = new ParametersPool();

		Assertions.assertFalse(testPool.add(mockedParameters).isEmpty());
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testAddParametersIllegal() {
		EasyMock.replay(mockedParameters);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testPool.add(null));
		EasyMock.verify(mockedParameters);
	}

	@Test
	void testClear() {
		EasyMock.replay(mockedParameters);
		Assertions.assertTrue(testPool.clear().isEmpty());
		EasyMock.verify(mockedParameters);
	}

	@Test
	void testFindOverride() {
		final ParameterOverride mockedParameter = EasyMock.createStrictMock(ParameterOverride.class);
		final Optional<String> testFound;

		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn("found").once();
		EasyMock.replay(mockedParameters, mockedParameter);
		testFound = testPool.find(mockedParameter);
		Assertions.assertNotNull(testFound);
		Assertions.assertTrue(testFound.isPresent());
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testFindOverrideNull() {
		final ParameterOverride mockedParameter = EasyMock.createStrictMock(ParameterOverride.class);
		final Optional<String> testFound;

		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn(null).once();
		EasyMock.replay(mockedParameters, mockedParameter);
		testFound = testPool.find(mockedParameter);
		Assertions.assertNotNull(testFound);
		Assertions.assertFalse(testFound.isPresent());
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testFindOverrideNullParameters() {
		final ParameterOverride mockedParameter = EasyMock.createStrictMock(ParameterOverride.class);
		final Optional<String> testFound;

		testPool = new ParametersPool(null, mockedParameters);
		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn("value").once();
		EasyMock.replay(mockedParameters, mockedParameter);
		testFound = testPool.find(mockedParameter);
		Assertions.assertNotNull(testFound);
		Assertions.assertTrue(testFound.isPresent());
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testFindParameter() {
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final Optional<String> testFound;

		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn("found").once();
		EasyMock.replay(mockedParameters, mockedParameter);
		testFound = testPool.find(mockedParameter);
		Assertions.assertNotNull(testFound);
		Assertions.assertTrue(testFound.isPresent());
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testFindParameterNull() {
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final Optional<String> testFound;

		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn(null).once();
		EasyMock.replay(mockedParameters, mockedParameter);
		testFound = testPool.find(mockedParameter);
		Assertions.assertNotNull(testFound);
		Assertions.assertFalse(testFound.isPresent());
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testFindParameterNullParameters() {
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final Optional<String> testFound;

		testPool = new ParametersPool(null, mockedParameters);
		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn("value").once();
		EasyMock.replay(mockedParameters, mockedParameter);
		testFound = testPool.find(mockedParameter);
		Assertions.assertNotNull(testFound);
		Assertions.assertTrue(testFound.isPresent());
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testGetDateParameter() {
		final ParameterDate mockedParameter = EasyMock.createStrictMock(ParameterDate.class);
		final Date expectedDate = new Date(37);

		EasyMock.expect(mockedParameters.getDate(mockedParameter)).andReturn((Date)expectedDate.clone()).once();
		EasyMock.replay(mockedParameters, mockedParameter);
		Assertions.assertEquals(expectedDate, testPool.getDate(mockedParameter));
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testGetDateParameterNull() {
		final ParameterDate mockedParameter = EasyMock.createStrictMock(ParameterDate.class);

		testPool = new ParametersPool(null, mockedParameters);
		EasyMock.expect(mockedParameters.getDate(mockedParameter)).andReturn(null).once();
		EasyMock.replay(mockedParameters, mockedParameter);
		Assertions.assertNull(testPool.getDate(mockedParameter));
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testGetDoubleOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("13.37d").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(13.37d, testPool.getDouble(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetDoubleOverrideIllegal() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("illegal").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getDouble(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetDoubleOverrideNotFound() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getDouble(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetDoubleParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("13.37d").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(13.37d, testPool.getDouble(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetDoubleParameterNotFound() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getDouble(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetFloatOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("13.37f").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(13.37f, testPool.getFloat(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetFloatOverrideIllegal() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("illegal").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getFloat(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetFloatOverrideNotFound() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getFloat(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetFloatParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("13.37f").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(13.37f, testPool.getFloat(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetFloatParameterNotFound() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getFloat(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetIntOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("37").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(37, testPool.getInt(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetIntOverrideIllegal() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("illegal").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getInt(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetIntOverrideNotFound() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getInt(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetIntParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("37").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(37, testPool.getInt(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetIntParameterNotFound() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getInt(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetLongOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("37").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(37L, testPool.getLong(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetLongOverrideIllegal() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("illegal").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getLong(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetLongOverrideNotFound() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getLong(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetLongParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("37").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(37L, testPool.getLong(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetLongParameterNotFound() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getLong(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetPeriodOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final Period expectedPeriod = Period.ofDays(37);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(expectedPeriod.toString()).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(expectedPeriod, testPool.getPeriod(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetPeriodOverrideIllegal() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("illegal").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getPeriod(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetPeriodOverrideNotFound() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getPeriod(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetPeriodParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final Period expectedPeriod = Period.ofDays(37);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(expectedPeriod.toString()).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(expectedPeriod, testPool.getPeriod(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetPeriodParameterNotFound() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getPeriod(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetShortOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("37").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(37L, testPool.getShort(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetShortOverrideIllegal() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("illegal").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getShort(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetShortOverrideNotFound() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getShort(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetShortParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn("37").once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertEquals(37, testPool.getShort(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetShortParameterNotFound() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> testPool.getShort(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testGetStringOverride() {
		final ParameterOverride mockedParameter = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedValue = "value";

		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn(expectedValue).once();
		EasyMock.replay(mockedParameters, mockedParameter);
		Assertions.assertEquals(expectedValue, testPool.get(mockedParameter));
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testGetStringOverrideNull() {
		final ParameterOverride mockedParameter = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn(null).once();
		EasyMock.replay(mockedParameters, mockedParameter);
		Assertions.assertNull(testPool.get(mockedParameter));
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testGetStringParameter() {
		final ParameterOverride mockedParameter = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedValue = "value";

		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn(expectedValue).once();
		EasyMock.replay(mockedParameters, mockedParameter);
		Assertions.assertEquals(expectedValue, testPool.get(mockedParameter));
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testGetStringParameterNull() {
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameters.get(mockedParameter)).andReturn(null).once();
		EasyMock.replay(mockedParameters, mockedParameter);
		Assertions.assertNull(testPool.get(mockedParameter));
		EasyMock.verify(mockedParameters, mockedParameter);
	}

	@Test
	void testIsEmpty() {
		EasyMock.expect(mockedParameters.isEmpty()).andReturn(true).once();
		EasyMock.replay(mockedParameters);
		Assertions.assertTrue(testPool.isEmpty());
		EasyMock.verify(mockedParameters);
	}

	@Test
	void testIsTrueOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(Boolean.TRUE.toString()).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertTrue(testPool.isTrue(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testIsTrueOverrideFalse() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(Boolean.FALSE.toString()).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertFalse(testPool.isTrue(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testIsTrueOverrideNotFound() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertFalse(testPool.isTrue(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testIsTrueParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(Boolean.TRUE.toString()).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertTrue(testPool.isTrue(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testIsTrueParameterFalse() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(Boolean.FALSE.toString()).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertFalse(testPool.isTrue(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testIsTrueParameterNotFound() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameters.get(mockedParam)).andReturn(null).once();
		EasyMock.replay(mockedParameters, mockedParam);
		Assertions.assertFalse(testPool.isTrue(mockedParam));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testResolveNullOverrides() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final ParametersPool testPool = new ParametersPool(mockedParameters, null);
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParameters.resolve(mockedParam)).andReturn(Collections.singleton("test")).once();
		EasyMock.replay(mockedParameters, mockedParam);
		testResolutions = testPool.resolve(mockedParam);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertEquals(1, Iterables.size(testResolutions));
		EasyMock.verify(mockedParameters, mockedParam);
	}

	@Test
	void testResolveNullParameters() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final ParametersPool testPool = new ParametersPool(mockedParameters, null);
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParameters.resolve(mockedParam)).andReturn(Collections.singleton("test")).once();
		EasyMock.replay(mockedParameters, mockedParam);
		testResolutions = testPool.resolve(mockedParam);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertEquals(1, Iterables.size(testResolutions));
		EasyMock.verify(mockedParameters, mockedParam);
	}
}
