package org.theotherorg.poros;

import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;

class ParametersTest {
	private PropertiesLoader mockedPropertiesLoader;
	private Map<String, String> testEnvMap;
	private Parameters testParameters;
	private Properties testSystemProperties;

	@BeforeEach
	void setUp() {
		mockedPropertiesLoader = EasyMock.createStrictMock(PropertiesLoader.class);
		testEnvMap = new HashMap<>();
		testSystemProperties = new Properties();
		testParameters = new Parameters(mockedPropertiesLoader, testEnvMap, testSystemProperties);
	}

	@Test
	void testClear() {
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);

		EasyMock.expect(mockedParameter.getKey()).andReturn("key").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParameter);
		Assertions.assertFalse(testParameters.put(mockedParameter, "value").isEmpty());
		Assertions.assertTrue(testParameters.clear().isEmpty());
		EasyMock.verify(mockedPropertiesLoader, mockedParameter);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testConstructors() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertDoesNotThrow((ThrowingSupplier<Parameters>)Parameters::new);
		Assertions.assertDoesNotThrow(() -> new Parameters(mockedPropertiesLoader));
		Assertions.assertDoesNotThrow(() -> new Parameters(mockedPropertiesLoader, testSystemProperties));
		Assertions.assertThrows(IllegalArgumentException.class, () -> new Parameters(null));
		Assertions.assertThrows(IllegalArgumentException.class, () -> new Parameters(mockedPropertiesLoader, null));
		Assertions.assertThrows(IllegalArgumentException.class, () -> new Parameters(mockedPropertiesLoader, null, testSystemProperties));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testFindOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";
		final String expectedValue = "value";
		final Optional<String> found;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, expectedValue).isEmpty());
		found = testParameters.find(mockedParam);
		Assertions.assertNotNull(found);
		Assertions.assertTrue(found.isPresent());
		Assertions.assertEquals(expectedValue, found.get());
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@Test
	void testFindParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final Optional<String> found;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("default").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, expectedValue).isEmpty());
		found = testParameters.find(mockedParam);
		Assertions.assertNotNull(found);
		Assertions.assertTrue(found.isPresent());
		Assertions.assertEquals(expectedValue, found.get());
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@Test
	void testGetDate() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final String expectedKey = "key";
		final Date testDate;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDateFormat()).andReturn(new SimpleDateFormat("yyyy-MM-dd")).once();
		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(null).once();
		EasyMock.replay(mockedParam, mockedProps);
		testDate = testParameters.put(mockedParam, "2029-03-14").getDate(mockedParam);
		Assertions.assertNotNull(testDate);
		Assertions.assertTrue(testDate.getTime() > 0);
		EasyMock.verify(mockedParam, mockedProps);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetDateNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getDate(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetDoubleOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";
		final double expectedValue = 13.37d;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getDouble(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetDoubleOverrideNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getDouble(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetDoubleParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final double expectedValue = 13.37d;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("default").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getDouble(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetDoubleParameterNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getDouble((Parameter)null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetFloatOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";
		final float expectedValue = 13.37f;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getFloat(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetFloatOverrideNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getFloat(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetFloatParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final float expectedValue = 13.37f;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("default").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getFloat(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetFloatParameterNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getFloat((Parameter)null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetIntOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";
		final int expectedValue = 37;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getInt(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetIntOverrideNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getInt(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetIntParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final int expectedValue = 37;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("default").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getInt(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetIntParameterNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getInt((Parameter)null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetLongOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";
		final long expectedValue = 37L;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getLong(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetLongOverrideNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getLong(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetLongParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final long expectedValue = 37L;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("default").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getLong(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetLongParameterNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getLong((Parameter)null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";
		final String expectedValue = "value";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, expectedValue).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.get(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetOverrideNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.get(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedValue = "value";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("default").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, expectedValue).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.get(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetParameterNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.get((Parameter)null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetPeriodOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";
		final Period expectedValue = Period.parse("P37D");

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, expectedValue.toString()).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getPeriod(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetPeriodOverrideNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getPeriod(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetPeriodParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final Period expectedValue = Period.parse("P37D");

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("default").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, expectedValue.toString()).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getPeriod(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetPeriodParameterNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getPeriod((Parameter)null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetShortOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";
		final short expectedValue = 37;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getShort(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetShortOverrideNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getShort(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testGetShortParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final short expectedValue = 37;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("default").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, String.valueOf(expectedValue)).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.getShort(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetShortParameterNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.getShort((Parameter)null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testIsEmpty() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertTrue(testParameters.isEmpty());
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testIsTrueOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, Boolean.TRUE.toString()).isEmpty());
		Assertions.assertTrue(testParameters.isTrue(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@Test
	void testIsTrueParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("default").once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, Boolean.TRUE.toString()).isEmpty());
		Assertions.assertTrue(testParameters.isTrue(mockedParam));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@Test
	void testLoad() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedValue = "value";

		testSystemProperties.put(expectedKey, expectedValue);
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedPropertiesLoader.load(expectedValue)).andReturn(new Properties()).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertTrue(testParameters.load(mockedParam).isEmpty());
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testLoadNull() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertThrows(IllegalResourceException.class, () -> testParameters.load(mockedParam));
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.load((String)null));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testPutNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertTrue(testParameters.put(null, null).isEmpty());
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.put(null, "value").isEmpty());
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testPutNullValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertTrue(testParameters.put(mockedParam, null).isEmpty());
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@Test
	void testRemove() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedValue = "value";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		Assertions.assertFalse(testParameters.put(mockedParam, expectedValue).isEmpty());
		Assertions.assertEquals(expectedValue, testParameters.remove(mockedParam));
		Assertions.assertTrue(testParameters.isEmpty());
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testRemoveNull() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.remove(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testResolveNullParameter() {
		EasyMock.replay(mockedPropertiesLoader);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameters.resolve(null));
		EasyMock.verify(mockedPropertiesLoader);
	}

	@Test
	void testResolveOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";
		final Iterable<String> testResolutions;

		testEnvMap.put("envKey", "envValue");
		testSystemProperties.put("key", "sysValue");
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		testResolutions = testParameters.put(mockedParam, "value").resolve(mockedParam);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertEquals(4, Iterables.size(testResolutions));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@Test
	void testResolveOverrideNoEnvNoSys() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		testResolutions = testParameters.put(mockedParam, "value").resolve(mockedParam);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertEquals(2, Iterables.size(testResolutions));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@Test
	void testResolveOverrideNullEnvValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";
		final Iterable<String> testResolutions;

		testEnvMap.put("envKey", null);
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		testResolutions = testParameters.put(mockedParam, "value").resolve(mockedParam);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertEquals(2, Iterables.size(testResolutions));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}

	@Test
	void testResolveParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.replay(mockedPropertiesLoader, mockedParam);
		testResolutions = testParameters.put(mockedParam, "value").resolve(mockedParam);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertFalse(Iterables.isEmpty(testResolutions));
		EasyMock.verify(mockedPropertiesLoader, mockedParam);
	}
}
