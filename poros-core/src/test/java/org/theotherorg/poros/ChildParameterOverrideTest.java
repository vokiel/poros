package org.theotherorg.poros;

import java.util.Collections;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.ChildParameters.ChildParameterOverride;

public class ChildParameterOverrideTest {
	@Test
	void testGetEnvKey() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final ParameterOverride testChild = new ChildParameterOverride(mockedParam, null, expectedEnvKey);

		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedEnvKey, testChild.getEnvKey());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testGetEnvKeyNull() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final ParameterOverride testChild = new ChildParameterOverride(mockedParam, null, null);

		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedEnvKey, testChild.getEnvKey());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testResolveIn() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final ChildParameterOverride testChild = new ChildParameterOverride(mockedParam, null);
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParams.resolve(testChild)).andReturn(Collections.emptyList());
		EasyMock.replay(mockedParams, mockedParam);
		testResolutions = testChild.resolveIn(mockedParams);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertTrue(Iterables.isEmpty(testResolutions));
		EasyMock.verify(mockedParams, mockedParam);
	}

	@Test
	void testWrite() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final ParameterWriter mockedWriter = EasyMock.createStrictMock(ParameterWriter.class);
		final ChildParameterOverride testChild = new ChildParameterOverride(mockedParam, null);
		final String expectedParent = "parent";
		final String expectedChild = "child";

		EasyMock.expect(mockedWriter.writeParent(testChild)).andReturn(expectedParent).once();
		EasyMock.expect(mockedWriter.write(testChild)).andReturn(expectedChild).once();
		EasyMock.replay(mockedParam, mockedWriter);
		Assertions.assertEquals(expectedParent + expectedChild, testChild.write(mockedWriter));
		EasyMock.verify(mockedParam, mockedWriter);
	}
}
