package org.theotherorg.poros;

import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.Arguments.CompositeExceptionHandler;

class CompositeExceptionHandlerTest {
	@Test
	void testUncaughtException() {
		final PrintStream mockedStream = EasyMock.createStrictMock(PrintStream.class);
		final UncaughtExceptionHandler mockedHandler = EasyMock.createStrictMock(UncaughtExceptionHandler.class);
		final CompositeExceptionHandler testHandler = new CompositeExceptionHandler(mockedStream, mockedHandler, null);
		final Thread mockedThread = EasyMock.createStrictMock(Thread.class);
		final Throwable mockedThrowable = EasyMock.createStrictMock(Throwable.class);

		mockedHandler.uncaughtException(mockedThread, mockedThrowable);
		EasyMock.expectLastCall().once();
		mockedStream.print(EasyMock.anyString());
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedStream, mockedHandler, mockedThread, mockedThrowable);
		testHandler.uncaughtException(mockedThread, mockedThrowable);
		EasyMock.verify(mockedStream, mockedHandler, mockedThread, mockedThrowable);
	}
}
