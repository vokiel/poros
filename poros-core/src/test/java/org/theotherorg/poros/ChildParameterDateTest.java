package org.theotherorg.poros;

import java.text.DateFormat;
import java.util.Collections;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.ChildParameters.ChildParameterDate;

class ChildParameterDateTest {
	@Test
	void testGetDateFormat() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final DateFormat mockedDateFormat = EasyMock.createStrictMock(DateFormat.class);
		final ParameterDate testChild = new ChildParameterDate(mockedParam, null, mockedDateFormat);

		EasyMock.replay(mockedParam, mockedDateFormat);
		Assertions.assertSame(mockedDateFormat, testChild.getDateFormat());
		EasyMock.verify(mockedParam, mockedDateFormat);
	}

	@Test
	void testGetDateFormatNull() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final DateFormat mockedDateFormat = EasyMock.createStrictMock(DateFormat.class);
		final ParameterDate testChild = new ChildParameterDate(mockedParam, null, null);

		EasyMock.expect(mockedParam.getDateFormat()).andReturn(mockedDateFormat).once();
		EasyMock.replay(mockedParam, mockedDateFormat);
		Assertions.assertSame(mockedDateFormat, testChild.getDateFormat());
		EasyMock.verify(mockedParam, mockedDateFormat);
	}

	@Test
	void testGetDefaultValue() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);

		EasyMock.replay(mockedParam);
		Assertions.assertNull(new ChildParameterDate(mockedParam, null).getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testGetParameterFormat() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final Parameter mockedParamFormat = EasyMock.createStrictMock(Parameter.class);
		final ParameterDate testChild = new ChildParameterDate(mockedParam, null, null, mockedParamFormat);

		EasyMock.replay(mockedParam, mockedParamFormat);
		Assertions.assertSame(mockedParamFormat, testChild.getParameterFormat());
		EasyMock.verify(mockedParam, mockedParamFormat);
	}

	@Test
	void testGetParameterFormatNull() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final Parameter mockedParamFormat = EasyMock.createStrictMock(Parameter.class);
		final ParameterDate testChild = new ChildParameterDate(mockedParam, null);

		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(mockedParamFormat).once();
		EasyMock.replay(mockedParam, mockedParamFormat);
		Assertions.assertSame(mockedParamFormat, testChild.getParameterFormat());
		EasyMock.verify(mockedParam, mockedParamFormat);
	}

	@Test
	void testResolveIn() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final ChildParameterDate testChild = new ChildParameterDate(mockedParam, null);
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParams.resolve(testChild)).andReturn(Collections.emptyList());
		EasyMock.replay(mockedParams, mockedParam);
		testResolutions = testChild.resolveIn(mockedParams);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertTrue(Iterables.isEmpty(testResolutions));
		EasyMock.verify(mockedParams, mockedParam);
	}

	@Test
	void testWrite() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final Parameter mockedParamFormat = EasyMock.createStrictMock(Parameter.class);
		final ParameterWriter mockedWriter = EasyMock.createStrictMock(ParameterWriter.class);
		final ChildParameterDate testChild = new ChildParameterDate(mockedParam, null);
		final String expectedParent = "parent";
		final String expectedChild = "child";

		EasyMock.expect(mockedWriter.writeParent(testChild)).andReturn(expectedParent).once();
		EasyMock.expect(mockedWriter.write(testChild)).andReturn(expectedChild).once();
		EasyMock.replay(mockedParam, mockedParamFormat, mockedWriter);
		Assertions.assertEquals(expectedParent + expectedChild, testChild.write(mockedWriter));
		EasyMock.verify(mockedParam, mockedParamFormat, mockedWriter);
	}
}
