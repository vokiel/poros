package org.theotherorg.poros;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IllegalParameterExceptionTest {
	@Test
	void testIllegalAllKnown() {
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final IllegalParameterException testException;

		EasyMock.expect(mockedParameter.getKey()).andReturn("key").once();
		EasyMock.replay(mockedParameter);
		testException = IllegalParameterException.illegalValue(mockedParameter, "type", "value", new RuntimeException());
		Assertions.assertNotNull(testException);
		Assertions.assertNotNull(testException.getMessage());
		Assertions.assertSame(mockedParameter, testException.getParameter());
		Assertions.assertThrows(IllegalParameterException.class, () -> {
			throw testException;
		});
		EasyMock.verify(mockedParameter);
	}

	@Test
	void testIllegalAllUnknown() {
		// Things that we don't know, that we don't know
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final IllegalParameterException testException;

		EasyMock.expect(mockedParameter.getKey()).andReturn("key").once();
		EasyMock.replay(mockedParameter);
		testException = IllegalParameterException.illegalValue(mockedParameter, null, null, null);
		Assertions.assertNotNull(testException);
		Assertions.assertNotNull(testException.getMessage());
		Assertions.assertSame(mockedParameter, testException.getParameter());
		Assertions.assertThrows(IllegalParameterException.class, () -> {
			throw testException;
		});
		EasyMock.verify(mockedParameter);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testIllegalUnknownParameter() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> IllegalParameterException.illegalValue(null, null, null, null));
	}

	@Test
	void testNullAllKnown() {
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final IllegalParameterException testException;

		EasyMock.expect(mockedParameter.getKey()).andReturn("key").once();
		EasyMock.replay(mockedParameter);
		testException = IllegalParameterException.nullValue(mockedParameter, "type");
		Assertions.assertNotNull(testException);
		Assertions.assertNotNull(testException.getMessage());
		Assertions.assertSame(mockedParameter, testException.getParameter());
		Assertions.assertThrows(IllegalParameterException.class, () -> {
			throw testException;
		});
		EasyMock.verify(mockedParameter);
	}

	@Test
	void testNullAllUnknown() {
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final IllegalParameterException testException;

		EasyMock.expect(mockedParameter.getKey()).andReturn("key").once();
		EasyMock.replay(mockedParameter);
		testException = IllegalParameterException.nullValue(mockedParameter, null);
		Assertions.assertNotNull(testException);
		Assertions.assertNotNull(testException.getMessage());
		Assertions.assertSame(mockedParameter, testException.getParameter());
		Assertions.assertThrows(IllegalParameterException.class, () -> {
			throw testException;
		});
		EasyMock.verify(mockedParameter);
	}
}
