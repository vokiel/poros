package org.theotherorg.poros;

import java.lang.Thread.UncaughtExceptionHandler;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.Arguments.Builder;
import org.theotherorg.poros.Arguments.Handler;

class ArgumentsBuilderTest {
	private Builder testBuilder;

	@BeforeEach
	void setup() {
		testBuilder = new Builder();
	}

	@Test
	void testFromEverything() throws Exception {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final Handler mockedHandler = EasyMock.createStrictMock(Handler.class);
		final Path tempFile = Files.createTempFile(getClass().getSimpleName(), null);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn("/testParameters.properties").once();
		mockedHandler.handle(testBuilder, "--conf", tempFile.toString());
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedHandler, mockedParam);
		testBuilder.setConfHandler(Arguments::withConfHandler);
		Assertions.assertNotNull(testBuilder.withHandler(mockedHandler).withResource(mockedParam).from("--conf", tempFile.toString()));
		EasyMock.verify(mockedHandler, mockedParam);
	}

	@Test
	void testFromHandle() {
		Arguments.from().handle(Assertions::assertNotNull);
	}

	@Test
	void testFromHandlerDumper() {
		Arguments.from("--dumper").handle(Assertions::assertNotNull);
	}

	@Test
	void testFromNothing() {
		Assertions.assertNotNull(testBuilder.from());
	}

	@Test
	void testGetConfHandler() {
		Assertions.assertNull(testBuilder.getConfHandler());
	}

	@Test
	void testGetExceptionHandler() {
		Assertions.assertNull(testBuilder.getConfHandler());
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testIllegalArguments() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new Arguments(null, null));
	}

	@Test
	void testSetConfHandler() {
		final Handler mockedHandler = EasyMock.createStrictMock(Handler.class);

		EasyMock.replay(mockedHandler);
		testBuilder.setConfHandler(mockedHandler);
		Assertions.assertSame(testBuilder.getConfHandler(), mockedHandler);
		EasyMock.verify(mockedHandler);
	}

	@Test
	void testSetExceptionHandler() {
		final UncaughtExceptionHandler mockedHandler = EasyMock.createStrictMock(UncaughtExceptionHandler.class);

		EasyMock.replay(mockedHandler);
		testBuilder.setExceptionHandler(mockedHandler);
		Assertions.assertSame(testBuilder.getExceptionHandler(), mockedHandler);
		EasyMock.verify(mockedHandler);
	}

	@Test
	void testWithConfigurations() throws Exception {
		final Path tempConf = Files.createTempFile(getClass().getSimpleName(), null);
		final Path tempConf2 = Files.createTempFile(getClass().getSimpleName(), null);

		Assertions.assertNotNull(testBuilder.withConfigurations(Arrays.asList("--conf", tempConf.toString(), "--conf", tempConf2.toString())));
		Files.deleteIfExists(tempConf);
		Files.deleteIfExists(tempConf2);
	}

	@Test
	void testWithConfigurationsLast() {
		testBuilder.withConfigurations(Collections.singletonList("--conf"));
	}

	@Test
	void testWithHandler() {
		final Handler mockedHandler = EasyMock.createStrictMock(Handler.class);

		EasyMock.replay(mockedHandler);
		testBuilder.withHandler(mockedHandler);
		EasyMock.verify(mockedHandler);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testWithHandlerNull() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> testBuilder.withHandler(null));
	}

	@Test
	void testWithResources() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final ParameterOverride mockedParam2 = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.replay(mockedParam, mockedParam2);
		Assertions.assertNotNull(testBuilder.withResource(mockedParam).withResource(mockedParam2));
		EasyMock.verify(mockedParam, mockedParam2);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testWithResourcesNullParameter() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> testBuilder.withResource(null));
	}
}
