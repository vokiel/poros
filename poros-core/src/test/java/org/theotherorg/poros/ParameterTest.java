package org.theotherorg.poros;

import java.time.Period;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ParameterTest {
	private HasProperties mockedProperties;

	@BeforeEach
	void setUp() {
		mockedProperties = EasyMock.createStrictMock(HasProperties.class);
	}

	@Test
	void testGetBoolProperty() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(Boolean.TRUE.toString());
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertTrue(Parameter.getBoolProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetDoubleIllegalValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn("illegalDouble").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getDoubleProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetDoubleNullValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getDoubleProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetDoubleValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedDefault = "-1";
		final double expectedValue = 13.37d;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefault).once();
		EasyMock.expect(mockedProperties.get(expectedKey, expectedDefault)).andReturn(String.valueOf(expectedValue)).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, Parameter.getDoubleProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetEmptyParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedDefault = "default";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefault).once();
		EasyMock.expect(mockedProperties.get(expectedKey, expectedDefault)).andReturn("").once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertNull(Parameter.getProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetFloatIllegalValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn("illegalFloat").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getFloatProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetFloatNullValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getFloatProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetFloatValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedDefault = "-1";
		final float expectedValue = 13.37f;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefault).once();
		EasyMock.expect(mockedProperties.get(expectedKey, expectedDefault)).andReturn(String.valueOf(expectedValue)).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, Parameter.getFloatProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetIntIllegalValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn("illegalInt").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getIntProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetIntNullValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getIntProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetIntValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedDefault = "-1";
		final int expectedValue = 37;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefault).once();
		EasyMock.expect(mockedProperties.get(expectedKey, expectedDefault)).andReturn(String.valueOf(expectedValue)).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, Parameter.getIntProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetLongIllegalValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn("illegalLong").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getLongProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetLongNullValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getLongProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetLongValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedDefault = "-1";
		final long expectedValue = 37L;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefault).once();
		EasyMock.expect(mockedProperties.get(expectedKey, expectedDefault)).andReturn(String.valueOf(expectedValue)).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, Parameter.getLongProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetNullParameter() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> Parameter.getRawProperty(null, null));
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetNullProperties() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalArgumentException.class, () -> Parameter.getRawProperty(mockedParam, null));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetPeriodIllegalValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn("illegalPeriod").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getPeriodProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetPeriodNullValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertNull(Parameter.getPeriodProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetPeriodValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedDefault = "P37D";
		final Period expectedValue = Period.parse(expectedDefault);

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefault).once();
		EasyMock.expect(mockedProperties.get(expectedKey, expectedDefault)).andReturn(expectedDefault).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, Parameter.getPeriodProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetShortIllegalValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn("illegalShort").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getShortProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetShortNullValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> Parameter.getShortProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetShortValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedDefault = "-1";
		final short expectedValue = 37;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefault).once();
		EasyMock.expect(mockedProperties.get(expectedKey, expectedDefault)).andReturn(String.valueOf(expectedValue)).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, Parameter.getShortProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetTrimmedParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedDefault = " default ";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefault).once();
		EasyMock.expect(mockedProperties.get(expectedKey, expectedDefault)).andReturn(expectedDefault).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedDefault.trim(), Parameter.getProperty(mockedParam, mockedProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testWithKey() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedNewKey = "key";
		final String expectedDefaultValue = "defaultValue";
		final Parameter testParam;

		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefaultValue).once();
		EasyMock.replay(mockedParam);
		testParam = Parameter.withKey(mockedParam, expectedNewKey);
		Assertions.assertEquals(expectedNewKey, testParam.getKey());
		Assertions.assertEquals(expectedDefaultValue, testParam.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithKeyValue() {
		final String expectedNewKey = "key";
		final String expectedDefaultValue = "defaultValue";
		final Parameter testParam = Parameter.withKeyValue(expectedNewKey, expectedDefaultValue);

		Assertions.assertEquals(expectedNewKey, testParam.getKey());
		Assertions.assertEquals(expectedDefaultValue, testParam.getDefaultValue());
	}
}
