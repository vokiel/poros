package org.theotherorg.poros;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IllegalParameterDateExceptionTest {
	@SuppressWarnings("ConstantConditions")
	@Test
	void testIllegalValueNoFormat() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final IllegalParameterDateException testException;

		EasyMock.expect(mockedParam.getKey()).andReturn("key").once();
		EasyMock.replay(mockedParam);
		testException = IllegalParameterDateException.illegalValue(mockedParam, (DateFormat)null, "value", new RuntimeException());
		Assertions.assertNotNull(testException);
		Assertions.assertNotNull(testException.getMessage());
		Assertions.assertThrows(IllegalParameterDateException.class, () -> {
			throw testException;
		});
		EasyMock.verify(mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testIllegalValueWithFormat() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final DateFormat testFormat = new SimpleDateFormat("yyyy-dd-MM");
		final IllegalParameterDateException testException;

		EasyMock.expect(mockedParam.getKey()).andReturn("key").once();
		EasyMock.replay(mockedParam);
		testException = IllegalParameterDateException.illegalValue(mockedParam, testFormat, "value", new RuntimeException());
		Assertions.assertNotNull(testException);
		Assertions.assertNotNull(testException.getMessage());
		Assertions.assertThrows(IllegalParameterDateException.class, () -> {
			throw testException;
		});
		EasyMock.verify(mockedParam);
	}
}
