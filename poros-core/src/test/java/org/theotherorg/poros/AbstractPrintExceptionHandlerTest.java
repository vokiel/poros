package org.theotherorg.poros;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.Arguments.AbstractPrintExceptionHandler;

class AbstractPrintExceptionHandlerTest {
	@SuppressWarnings("ConstantConditions")
	@Test
	void testNullPrintStream() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new AbstractPrintExceptionHandler(null) {
			@Override
			public void uncaughtException(final Thread thread, final Throwable throwable) {
			}
		});
	}
}
