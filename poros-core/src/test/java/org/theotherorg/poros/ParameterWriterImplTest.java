package org.theotherorg.poros;

import java.util.Collections;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ParameterWriterImplTest {
	private ParameterWriterImpl testWriter;

	@BeforeEach
	void setUp() {
		testWriter = new ParameterWriterImpl();
	}

	@Test
	void testWriteDate() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final Parameter mockedFormat = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final String expectedFormatKey = "format";
		final String testWritten;

		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(mockedFormat).once();
		EasyMock.expect(mockedFormat.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedFormat.getKey()).andReturn(expectedFormatKey).once();
		EasyMock.replay(mockedParam, mockedFormat);
		testWritten = testWriter.write(mockedParam);
		Assertions.assertNotNull(testWritten);
		Assertions.assertTrue(testWritten.contains(expectedKey));
		Assertions.assertTrue(testWritten.contains(expectedValue));
		Assertions.assertTrue(testWritten.contains(expectedFormatKey));
		EasyMock.verify(mockedParam, mockedFormat);
	}

	@Test
	void testWriteDateNullEverything() {
		final String testWritten = testWriter.write((ParameterDate)null);

		Assertions.assertNotNull(testWritten);
	}

	@Test
	void testWriteDateNullFormat() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final String testWritten;

		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(null).once();
		EasyMock.replay(mockedParam);
		testWritten = testWriter.write(mockedParam);
		Assertions.assertNotNull(testWritten);
		Assertions.assertTrue(testWritten.contains(expectedKey));
		Assertions.assertTrue(testWritten.contains(expectedValue));
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWriteOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final String expectedEnvKey = "envKey";
		final String testWritten;

		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.replay(mockedParam);
		testWritten = testWriter.write(mockedParam);
		Assertions.assertNotNull(testWritten);
		Assertions.assertTrue(testWritten.contains(expectedKey));
		Assertions.assertTrue(testWritten.contains(expectedValue));
		Assertions.assertTrue(testWritten.contains(expectedEnvKey));
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWriteOverrideNull() {
		final String testWritten = testWriter.write((ParameterOverride)null);

		Assertions.assertNotNull(testWritten);
	}

	@Test
	void testWriteParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final String testWritten;

		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParam);
		testWritten = testWriter.write(mockedParam);
		Assertions.assertNotNull(testWritten);
		Assertions.assertTrue(testWritten.contains(expectedKey));
		Assertions.assertTrue(testWritten.contains(expectedValue));
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWriteParameterNull() {
		final String testWritten = testWriter.write((Parameter)null);

		Assertions.assertNotNull(testWritten);
	}

	@Test
	void testWriteParameterNullDefaultValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String testWritten;

		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParam);
		testWritten = testWriter.write(mockedParam);
		Assertions.assertNotNull(testWritten);
		Assertions.assertTrue(testWritten.contains(expectedKey));
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWriteParameters() {
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedRepresentation = "representation";
		final Iterable<String> reps = Collections.singleton(expectedRepresentation);
		final String testWritten;

		EasyMock.expect(mockedParam.resolveIn(mockedParams)).andReturn(reps).once();
		EasyMock.replay(mockedParams, mockedParam);
		testWritten = testWriter.write(mockedParams, mockedParam);
		Assertions.assertNotNull(testWritten);
		Assertions.assertTrue(testWritten.contains(expectedRepresentation));
		EasyMock.verify(mockedParams, mockedParam);
	}

	@Test
	void testWriteParametersNotFound() {
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String testWritten;

		EasyMock.expect(mockedParam.resolveIn(mockedParams)).andReturn(Collections.emptyList()).once();
		EasyMock.replay(mockedParams, mockedParam);
		testWritten = testWriter.write(mockedParams, mockedParam);
		Assertions.assertNotNull(testWritten);
		EasyMock.verify(mockedParams, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testWriteParametersNullParameters() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> testWriter.write(null, null));
	}

	@Test
	void testWriteParent() {
		final ChildParameter mockedChild = EasyMock.createStrictMock(ChildParameter.class);
		final Parameter mockedParent = EasyMock.createStrictMock(Parameter.class);
		final String expectedRep = "rep";
		final String testWritten;

		EasyMock.expect(mockedChild.getParent()).andReturn(mockedParent).once();
		EasyMock.expect(mockedParent.write(testWriter)).andReturn(expectedRep).once();
		EasyMock.replay(mockedChild, mockedParent);
		testWritten = testWriter.writeParent(mockedChild);
		Assertions.assertTrue(testWritten.contains(expectedRep));
		EasyMock.verify(mockedChild, mockedParent);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testWriteParentIllegalChild() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> testWriter.writeParent(null));
	}
}
