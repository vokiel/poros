package org.theotherorg.poros;

import java.util.Properties;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ClassPathLoaderTest {
	private ClassPathLoader testProperties;

	@BeforeEach
	void setUp() {
		testProperties = new ClassPathLoader();
	}

	@Test
	void testLoadResource() {
		final String expectedName = "/testParameters.properties";
		final Properties result = testProperties.load(expectedName);

		Assertions.assertNotNull(result);
		Assertions.assertEquals("Test", result.get("org.theotherorg.poros.test.Test"));
	}
}
