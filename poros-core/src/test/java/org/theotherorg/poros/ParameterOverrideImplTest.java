package org.theotherorg.poros;

import java.util.Collections;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ParameterOverrideImplTest {
	@Test
	void testGetEnvKey() {
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";
		final ParameterOverrideImpl testKey = new ParameterOverrideImpl(expectedKey, expectedEnvKey, null);

		Assertions.assertEquals(expectedEnvKey, testKey.getEnvKey());
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testNullEnvKey() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ParameterOverrideImpl("key", null, null));
	}

	@Test
	void testResolveIn() {
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final ParameterOverrideImpl testParam = new ParameterOverrideImpl("key", "envKey", null);
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParams.resolve(testParam)).andReturn(Collections.emptyList()).once();
		EasyMock.replay(mockedParams);
		testResolutions = testParam.resolveIn(mockedParams);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertTrue(Iterables.isEmpty(testResolutions));
		EasyMock.verify(mockedParams);
	}

	@Test
	void testWrite() {
		final ParameterWriter mockedWriter = EasyMock.createStrictMock(ParameterWriter.class);
		final ParameterOverrideImpl testParam = new ParameterOverrideImpl("key", "envKey", null);
		final String expectedResult = "result";

		EasyMock.expect(mockedWriter.write(testParam)).andReturn(expectedResult).once();
		EasyMock.replay(mockedWriter);
		Assertions.assertEquals(expectedResult, testParam.write(mockedWriter));
		EasyMock.verify(mockedWriter);
	}
}
