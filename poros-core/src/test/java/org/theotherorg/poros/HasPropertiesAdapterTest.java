package org.theotherorg.poros;

import java.util.Properties;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HasPropertiesAdapterTest {
	private Properties mockedProperties;
	private HasProperties testHasProperties;

	@BeforeEach
	void setUp() {
		mockedProperties = EasyMock.createStrictMock(Properties.class);
		testHasProperties = new HasPropertiesAdapter(mockedProperties);
	}

	@Test
	void testGet() {
		final String expectedKey = "key";
		final String expectedValue = "value";

		EasyMock.expect(mockedProperties.getProperty(expectedKey)).andReturn(expectedValue).once();
		EasyMock.replay(mockedProperties);
		Assertions.assertEquals(expectedValue, testHasProperties.get(expectedKey));
		EasyMock.verify(mockedProperties);
	}

	@Test
	void testGetDefaultValue() {
		final String expectedKey = "key";
		final String expectedDefault = "value";

		EasyMock.expect(mockedProperties.getProperty(expectedKey, expectedDefault)).andReturn(expectedDefault).once();
		EasyMock.replay(mockedProperties);
		Assertions.assertEquals(expectedDefault, testHasProperties.get(expectedKey, expectedDefault));
		EasyMock.verify(mockedProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetIllegalKey() {
		EasyMock.replay(mockedProperties);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testHasProperties.get(null));
		EasyMock.verify(mockedProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetIllegalKeyDefaultValue() {
		EasyMock.replay(mockedProperties);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testHasProperties.get(null, null));
		EasyMock.verify(mockedProperties);
	}

	@Test
	void testGetName() {
		EasyMock.replay(mockedProperties);
		Assertions.assertNotNull(testHasProperties.getName());
		EasyMock.verify(mockedProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testIllegalName() {
		EasyMock.replay(mockedProperties);
		Assertions.assertThrows(IllegalArgumentException.class, () -> new HasPropertiesAdapter(mockedProperties, null));
		EasyMock.verify(mockedProperties);
	}

	@Test
	void testIsEmpty() {
		EasyMock.expect(mockedProperties.isEmpty()).andReturn(true).once();
		EasyMock.replay(mockedProperties);
		Assertions.assertTrue(testHasProperties.isEmpty());
		EasyMock.verify(mockedProperties);
	}

	@Test
	void testResolve() {
		final String expectedKey = "key";
		final String expectedValue = "value";
		final Iterable<String> testResolution;

		EasyMock.expect(mockedProperties.getProperty(expectedKey)).andReturn(expectedValue).once();
		EasyMock.replay(mockedProperties);
		testResolution = testHasProperties.resolve(expectedKey);
		Assertions.assertNotNull(testResolution);
		Assertions.assertNotNull(Iterables.get(testResolution, 0));
		EasyMock.verify(mockedProperties);
	}

	@Test
	void testResolveNullValue() {
		final String expectedKey = "key";
		final Iterable<String> testResolution;

		EasyMock.expect(mockedProperties.getProperty(expectedKey)).andReturn(null).once();
		EasyMock.replay(mockedProperties);
		testResolution = testHasProperties.resolve(expectedKey);
		Assertions.assertNotNull(testResolution);
		Assertions.assertTrue(Iterables.isEmpty(testResolution));
		EasyMock.verify(mockedProperties);
	}
}
