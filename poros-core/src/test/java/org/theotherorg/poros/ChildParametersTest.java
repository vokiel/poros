package org.theotherorg.poros;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ChildParametersTest {
	@Test
	void testPrivateConstructor() throws Exception {
		final Constructor<ChildParameters> ctor = ChildParameters.class.getDeclaredConstructor();

		ctor.setAccessible(true);
		Assertions.assertThrows(InvocationTargetException.class, ctor::newInstance);
	}

	@Test
	void testWithDefaultValueOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final Parameter testChild = ChildParameters.withDefaultValue(mockedParam, expectedValue);

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithDefaultValueParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final Parameter testChild = ChildParameters.withDefaultValue(mockedParam, expectedValue);

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithDefaultValueParameterDate() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final ParameterOverride testChild = ChildParameters.withDefaultValue(mockedParam, expectedKey, expectedValue);

		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithEnvKey() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedValue = "value";
		final ParameterOverride testChild = ChildParameters.withEnvKey(mockedParam, expectedEnvKey);

		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedEnvKey, testChild.getEnvKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithEnvKeyDefaultValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedValue = "value";
		final ParameterOverride testChild = ChildParameters.withEnvKey(mockedParam, expectedEnvKey, expectedValue);

		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedEnvKey, testChild.getEnvKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithKeyDefaultValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final Parameter testChild = ChildParameters.withKey(mockedParam, expectedKey, expectedValue);

		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithKeyFormat() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final DateFormat mockedDateFormat = EasyMock.createStrictMock(DateFormat.class);
		final String expectedKey = "key";
		final ParameterDate testChild = ChildParameters.withKeyFormat(mockedParam, expectedKey, mockedDateFormat);

		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertSame(mockedDateFormat, testChild.getDateFormat());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithKeyFormatParameter() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final DateFormat mockedDateFormat = EasyMock.createStrictMock(DateFormat.class);
		final Parameter mockedParamFormat = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final ParameterDate testChild = ChildParameters.withKeyFormat(mockedParam, expectedKey, mockedDateFormat, mockedParamFormat);

		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertSame(mockedDateFormat, testChild.getDateFormat());
		Assertions.assertSame(mockedParamFormat, testChild.getParameterFormat());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithKeyOverride() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final ParameterOverride testChild = ChildParameters.withKey(mockedParam, expectedKey);

		EasyMock.expect(testChild.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithKeyParameter() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final Parameter testChild = ChildParameters.withKey(mockedParam, expectedKey);

		EasyMock.expect(testChild.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithKeyParameterDate() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final DateFormat mockedDateFormat = EasyMock.createStrictMock(DateFormat.class);
		final String expectedKey = "key";
		final ParameterDate testChild = ChildParameters.withKey(mockedParam, expectedKey);

		EasyMock.expect(mockedParam.getDateFormat()).andReturn(mockedDateFormat).once();
		EasyMock.replay(mockedParam, mockedDateFormat);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertSame(mockedDateFormat, testChild.getDateFormat());
		EasyMock.verify(mockedParam, mockedDateFormat);
	}

	@Test
	void testWithKeys() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";
		final String expectedValue = "value";
		final ParameterOverride testChild = ChildParameters.withKeys(mockedParam, expectedKey, expectedEnvKey);

		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedValue).once();
		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertEquals(expectedEnvKey, testChild.getEnvKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}

	@Test
	void testWithKeysDefaultValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";
		final String expectedValue = "value";
		final ParameterOverride testChild = ChildParameters.withKeys(mockedParam, expectedKey, expectedEnvKey, expectedValue);

		EasyMock.replay(mockedParam);
		Assertions.assertEquals(expectedKey, testChild.getKey());
		Assertions.assertEquals(expectedEnvKey, testChild.getEnvKey());
		Assertions.assertEquals(expectedValue, testChild.getDefaultValue());
		EasyMock.verify(mockedParam);
	}
}
