package org.theotherorg.poros;

import java.util.Collections;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ParameterImplTest {
	@Test
	void testCopy() {
		final String expectedKey = "key";
		final String expectedDefaultValue = "defaultValue";
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final ParameterImpl testCopy;

		EasyMock.expect(mockedParameter.getDefaultValue()).andReturn(expectedDefaultValue).once();
		EasyMock.replay(mockedParameter);
		testCopy = new ParameterImpl(mockedParameter, expectedKey);
		Assertions.assertEquals(expectedKey, testCopy.getKey());
		Assertions.assertEquals(expectedDefaultValue, testCopy.getDefaultValue());
		EasyMock.verify(mockedParameter);
	}

	@SuppressWarnings("All")
	@Test
	void testEquals() {
		final ParameterImpl expectedParameter = new ParameterImpl("key", "value");

		Assertions.assertFalse(expectedParameter.equals(null));
		Assertions.assertFalse(expectedParameter.equals(this));
		Assertions.assertTrue(expectedParameter.equals(expectedParameter));
		Assertions.assertNotEquals(expectedParameter, new ParameterImpl("otherKey", "value"));
		Assertions.assertEquals(expectedParameter, new ParameterImpl("key", "value2"));
	}

	@Test
	void testGetDefaultValue() {
		final String expectedDefaultValue = "default";
		final ParameterImpl testDefault = new ParameterImpl("", expectedDefaultValue);

		Assertions.assertEquals(expectedDefaultValue, testDefault.getDefaultValue());
	}

	@Test
	void testGetKey() {
		final String expectedKey = "key";
		final ParameterImpl testKey = new ParameterImpl(expectedKey, null);

		Assertions.assertEquals(expectedKey, testKey.getKey());
	}

	@Test
	void testHashCode() {
		Assertions.assertTrue(new ParameterImpl("key", "value").hashCode() != 0);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testNullCopy() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ParameterImpl((Parameter)null, null));
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testNullCopyKey() {
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);

		EasyMock.replay(mockedParameter);
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ParameterImpl(mockedParameter, null));
		EasyMock.verify(mockedParameter);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testNullKey() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ParameterImpl((String)null, null));
	}

	@Test
	void testWrite() {
		final ParameterWriter mockedWriter = EasyMock.createStrictMock(ParameterWriter.class);
		final ParameterImpl testParam = new ParameterImpl("key", null);
		final String expectedResult = "result";

		EasyMock.expect(mockedWriter.write(testParam)).andReturn(expectedResult).once();
		EasyMock.replay(mockedWriter);
		Assertions.assertEquals(expectedResult, testParam.write(mockedWriter));
		EasyMock.verify(mockedWriter);
	}

	@Test
	void testResolveIn() {
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final ParameterImpl testParam = new ParameterImpl("key", null);
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParams.resolve(testParam)).andReturn(Collections.emptyList()).once();
		EasyMock.replay(mockedParams);
		testResolutions = testParam.resolveIn(mockedParams);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertTrue(Iterables.isEmpty(testResolutions));
		EasyMock.verify(mockedParams);
	}
}
