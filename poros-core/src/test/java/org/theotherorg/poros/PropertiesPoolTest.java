package org.theotherorg.poros;

import java.util.Optional;
import java.util.Properties;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PropertiesPoolTest {
	private PropertiesPool testPool;

	@BeforeEach
	void setUp() {
		testPool = new PropertiesPool();
	}

	@Test
	void testAddHasProperties() {
		final HasProperties mockProperties = EasyMock.createStrictMock(HasProperties.class);
		final String expectedKey = "key";
		final String expectedValue = "value";

		EasyMock.expect(mockProperties.get(expectedKey)).andReturn(expectedValue).once();
		EasyMock.replay(mockProperties);
		Assertions.assertEquals(expectedValue, testPool.add(mockProperties).get(expectedKey, null));
		EasyMock.verify(mockProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testAddHasPropertiesIllegal() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> testPool.add((HasProperties)null));
	}

	@Test
	void testAddProperties() {
		final Properties mockProperties = EasyMock.createStrictMock(Properties.class);

		EasyMock.expect(mockProperties.isEmpty()).andReturn(false).once();
		EasyMock.replay(mockProperties);
		testPool.add(mockProperties);
		Assertions.assertFalse(testPool.isEmpty());
		EasyMock.verify(mockProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testAddPropertiesIllegal() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> testPool.add((Properties)null));
	}

	@Test
	void testFind() {
		final Properties mockProperties = EasyMock.createStrictMock(Properties.class);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final Optional<String> found;

		EasyMock.expect(mockProperties.getProperty(expectedKey)).andReturn(expectedValue).once();
		EasyMock.replay(mockProperties);
		found = testPool.add(mockProperties).find(expectedKey);
		Assertions.assertTrue(found.isPresent());
		Assertions.assertEquals(expectedValue, found.get());
		EasyMock.verify(mockProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testFindNullKey() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new PropertiesPool().find(null));
	}

	@Test
	void testFindNullProperties() {
		final HasProperties mockProperties = EasyMock.createStrictMock(HasProperties.class);
		final HasProperties mockProperties2 = EasyMock.createStrictMock(HasProperties.class);
		final PropertiesPool testPool = new PropertiesPool(null, mockProperties, mockProperties2);
		final String expectedKey = "key";
		final String expectedValue = "value";
		final Optional<String> found;

		EasyMock.expect(mockProperties.get(expectedKey)).andReturn(expectedValue).once();
		EasyMock.replay(mockProperties, mockProperties2);
		found = testPool.find(expectedKey);
		Assertions.assertTrue(found.isPresent());
		Assertions.assertEquals(expectedValue, found.get());
		EasyMock.verify(mockProperties, mockProperties2);
	}

	@Test
	void testGetDefaultValue() {
		final Properties mockProperties = EasyMock.createStrictMock(Properties.class);
		final String expectedKey = "key";
		final String expectedValue = "value";

		EasyMock.expect(mockProperties.getProperty(expectedKey)).andReturn(null).once();
		EasyMock.replay(mockProperties);
		Assertions.assertEquals(expectedValue, testPool.add(mockProperties).get(expectedKey, expectedValue));
		EasyMock.verify(mockProperties);
	}

	@Test
	void testGetNullValue() {
		final Properties mockProperties = EasyMock.createStrictMock(Properties.class);
		final String expectedKey = "key";

		EasyMock.expect(mockProperties.getProperty(expectedKey)).andReturn(null).once();
		EasyMock.replay(mockProperties);
		Assertions.assertNull(testPool.add(mockProperties).get(expectedKey));
		EasyMock.verify(mockProperties);
	}

	@Test
	void testIsEmpty() {
		Assertions.assertTrue(testPool.isEmpty());
	}

	@Test
	void testIsEmptyNullProperties() {
		final HasProperties mockProperties = EasyMock.createStrictMock(HasProperties.class);
		final HasProperties mockProperties2 = EasyMock.createStrictMock(HasProperties.class);

		EasyMock.expect(mockProperties.isEmpty()).andReturn(true).once();
		EasyMock.expect(mockProperties2.isEmpty()).andReturn(true).once();
		EasyMock.replay(mockProperties, mockProperties2);
		Assertions.assertTrue(new PropertiesPool(mockProperties, null, mockProperties2).isEmpty());
		EasyMock.verify(mockProperties, mockProperties2);
	}

	@Test
	void testIterator() {
		Assertions.assertNotNull(testPool.iterator());
		Assertions.assertFalse(testPool.iterator().hasNext());
	}

	@Test
	void testResolveNullResolution() {
		final HasProperties mockedProperties = EasyMock.createStrictMock(HasProperties.class);
		final PropertiesPool testPool = new PropertiesPool(null, mockedProperties);
		final String expectedKey = "key";
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedProperties.resolve(expectedKey)).andReturn(null).once();
		EasyMock.replay(mockedProperties);
		testResolutions = testPool.resolve(expectedKey);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertEquals(1, Iterables.size(testResolutions));
		EasyMock.verify(mockedProperties);
	}
}
