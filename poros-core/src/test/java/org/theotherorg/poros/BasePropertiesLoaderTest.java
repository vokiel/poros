package org.theotherorg.poros;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BasePropertiesLoaderTest {
	private ResourceStreams mockedStreams;
	private BasePropertiesLoader testLoader;

	@BeforeEach
	void setUp() {
		mockedStreams = EasyMock.createStrictMock(ResourceStreams.class);
		testLoader = new BasePropertiesLoader(mockedStreams);
	}

	@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
	@Test
	void testLoadPropertiesResourceIOException() throws Exception {
		final Properties mockedProperties = EasyMock.createStrictMock(Properties.class);
		final String expectedName = "problematic";

		EasyMock.expect(mockedStreams.getInputStream(expectedName)).andThrow(new IOException()).once();
		EasyMock.replay(mockedStreams, mockedProperties);
		Assertions.assertThrows(IllegalResourceException.class, () -> testLoader.load(expectedName, mockedProperties));
		EasyMock.verify(mockedStreams, mockedProperties);
	}

	@Test
	void testLoadPropertiesResourceNotFoundException() throws IOException {
		final Properties mockedProperties = EasyMock.createStrictMock(Properties.class);
		final String expectedName = "name";

		EasyMock.expect(mockedStreams.getInputStream(expectedName)).andReturn(null).once();
		EasyMock.replay(mockedStreams, mockedProperties);
		Assertions.assertThrows(IllegalResourceException.class, () -> testLoader.load(expectedName, mockedProperties));
		EasyMock.verify(mockedStreams, mockedProperties);
	}

	@Test
	void testLoadPropertiesStream() throws Exception {
		final InputStream mockedInput = EasyMock.createStrictMock(InputStream.class);
		final Properties mockedProperties = EasyMock.createStrictMock(Properties.class);

		mockedProperties.load(mockedInput);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedStreams, mockedInput, mockedProperties);
		testLoader.load(mockedInput, mockedProperties);
		EasyMock.verify(mockedStreams, mockedInput, mockedProperties);
	}

	@Test
	void testLoadPropertiesStreamIOException() throws Exception {
		final InputStream mockedInput = EasyMock.createStrictMock(InputStream.class);
		final Properties mockedProperties = EasyMock.createStrictMock(Properties.class);

		mockedProperties.load(mockedInput);
		EasyMock.expectLastCall().andThrow(new IOException()).once();
		EasyMock.replay(mockedStreams, mockedInput, mockedProperties);
		Assertions.assertThrows(IllegalStateException.class, () -> testLoader.load(mockedInput, mockedProperties));
		EasyMock.verify(mockedStreams, mockedInput, mockedProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testLoadPropertiesStreamIllegalProperties() {
		final InputStream mockedStream = EasyMock.createStrictMock(InputStream.class);

		EasyMock.replay(mockedStreams, mockedStream);
		Assertions.assertThrows(NullPointerException.class, () -> testLoader.load(mockedStream, null));
		EasyMock.verify(mockedStreams, mockedStream);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testLoadPropertiesStreamIllegalResource() throws IOException {
		final Properties mockedProperties = EasyMock.createStrictMock(Properties.class);
		final String expectedName = "name";

		EasyMock.expect(mockedStreams.getInputStream(expectedName)).andThrow(new NullPointerException()).once();
		EasyMock.replay(mockedStreams, mockedProperties);
		Assertions.assertThrows(IllegalArgumentException.class, () -> testLoader.load("name"));
		EasyMock.verify(mockedStreams, mockedProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testLoadPropertiesStreamIllegalStream() {
		final Properties mockedProperties = EasyMock.createStrictMock(Properties.class);

		EasyMock.replay(mockedStreams, mockedProperties);
		Assertions.assertThrows(NullPointerException.class, () -> testLoader.load((InputStream)null, mockedProperties));
		EasyMock.verify(mockedStreams, mockedProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testLoadResourceNull() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> testLoader.load(null));
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testResourceStreamsNull() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new BasePropertiesLoader(null));
	}
}
