package org.theotherorg.poros;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ParameterDateTest {
	@Test
	void testGetDateFormatIllegalValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProps.get(expectedKey, null)).andReturn("illegal").once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParam, mockedProps);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterDate.getDateFormat(mockedParam, mockedProps));
		EasyMock.verify(mockedParam, mockedProps);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetDateFormatNullParameter() {
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);

		EasyMock.replay(mockedProps);
		Assertions.assertThrows(IllegalArgumentException.class, () -> ParameterDate.getDateFormat(null, mockedProps));
		EasyMock.verify(mockedProps);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetDateFormatNullProperties() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.replay(mockedParam);
		Assertions.assertThrows(IllegalArgumentException.class, () -> ParameterDate.getDateFormat(mockedParam, null));
		EasyMock.verify(mockedParam);
	}

	@Test
	void testGetDateFormatNullValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProps.get(expectedKey, null)).andReturn(null).once();
		EasyMock.replay(mockedParam, mockedProps);
		Assertions.assertNull(ParameterDate.getDateFormat(mockedParam, mockedProps));
		EasyMock.verify(mockedParam, mockedProps);
	}

	@Test
	void testGetDateFormatValue() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProps.get(expectedKey, null)).andReturn("yyyy/MM/dd").once();
		EasyMock.replay(mockedParam, mockedProps);
		Assertions.assertNotNull(ParameterDate.getDateFormat(mockedParam, mockedProps));
		EasyMock.verify(mockedParam, mockedProps);
	}

	@Test
	void testGetDateIllegalValue() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> ParameterDate.getDate(DateFormat.getDateInstance(), "notADate"));
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetDateNullFormat() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> ParameterDate.getDate(null, "test"));
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetDateNullValue() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> ParameterDate.getDate(DateFormat.getDateInstance(), null));
	}

	@Test
	void testGetDateParameterDefaultAll() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final String expectedKey = "key";
		final Date testDate;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedProps.get(expectedKey)).andReturn("2029-03-14").once();
		EasyMock.expect(mockedParam.getDateFormat()).andReturn(new SimpleDateFormat("yyyy-MM-dd")).once();
		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(null).once();
		EasyMock.replay(mockedParam, mockedProps);
		testDate = ParameterDate.getDate(mockedParam, mockedProps);
		Assertions.assertNotNull(testDate);
		Assertions.assertTrue(testDate.getTime() > 0);
		EasyMock.verify(mockedParam, mockedProps);
	}

	@Test
	void testGetDateParameterNullFormat() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final Parameter mockedFormat = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedFormatKey = "formatKey";
		final Date testDate;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedProps.get(expectedKey)).andReturn("2029-03-14").once();
		EasyMock.expect(mockedParam.getDateFormat()).andReturn(new SimpleDateFormat("yyyy-MM-dd")).once();
		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(mockedFormat).once();
		EasyMock.expect(mockedFormat.getKey()).andReturn(expectedFormatKey).once();
		EasyMock.expect(mockedFormat.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProps.get(expectedFormatKey, null)).andReturn(null).once();
		EasyMock.replay(mockedParam, mockedProps, mockedFormat);
		testDate = ParameterDate.getDate(mockedParam, mockedProps);
		Assertions.assertNotNull(testDate);
		Assertions.assertTrue(testDate.getTime() > 0);
		EasyMock.verify(mockedParam, mockedProps, mockedFormat);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetDateParameterNullParameter() {
		final HasProperties mockedProperties = EasyMock.createStrictMock(HasProperties.class);

		EasyMock.replay(mockedProperties);
		Assertions.assertThrows(IllegalArgumentException.class, () -> ParameterDate.getDate(null, mockedProperties));
		EasyMock.verify(mockedProperties);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetDateParameterNullProperties() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);

		EasyMock.replay(mockedParam);
		Assertions.assertThrows(IllegalArgumentException.class, () -> ParameterDate.getDate(mockedParam, null));
		EasyMock.verify(mockedParam);
	}

	@Test
	void testGetDateParameterNullValue() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedProps.get(expectedKey)).andReturn(null).once();
		EasyMock.replay(mockedParam, mockedProps);
		Assertions.assertNull(ParameterDate.getDate(mockedParam, mockedProps));
		EasyMock.verify(mockedParam, mockedProps);
	}

	@Test
	void testGetDateParameterWithIllegalFormat() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final Parameter mockedFormat = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedFormatKey = "formatKey";
		final String expectedIllegalFormat = "illegal";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedProps.get(expectedKey)).andReturn("2029-03-14").once();
		EasyMock.expect(mockedParam.getDateFormat()).andReturn(new SimpleDateFormat("yyyy-MM-dd")).once();
		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(mockedFormat).once();
		EasyMock.expect(mockedFormat.getKey()).andReturn(expectedFormatKey).once();
		EasyMock.expect(mockedFormat.getDefaultValue()).andReturn(expectedIllegalFormat).once();
		EasyMock.expect(mockedProps.get(expectedFormatKey, expectedIllegalFormat)).andReturn(expectedIllegalFormat).once();
		EasyMock.expect(mockedFormat.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParam, mockedProps, mockedFormat);
		Assertions.assertThrows(IllegalParameterDateException.class, () -> ParameterDate.getDate(mockedParam, mockedProps));
		EasyMock.verify(mockedParam, mockedProps, mockedFormat);
	}

	@Test
	void testGetDateParameterWithIllegalValue() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final Parameter mockedFormat = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedFormatKey = "formatKey";
		final String expectedFormat = "yyyy/MM-dd";
		final Date testDate;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedProps.get(expectedKey)).andReturn("2029-03-14").once();
		EasyMock.expect(mockedParam.getDateFormat()).andReturn(new SimpleDateFormat("yyyy-MM-dd")).once();
		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(mockedFormat).once();
		EasyMock.expect(mockedFormat.getKey()).andReturn(expectedFormatKey).once();
		EasyMock.expect(mockedFormat.getDefaultValue()).andReturn(expectedFormat).once();
		EasyMock.expect(mockedProps.get(expectedFormatKey, expectedFormat)).andReturn(expectedFormat).once();
		EasyMock.replay(mockedParam, mockedProps, mockedFormat);
		testDate = ParameterDate.getDate(mockedParam, mockedProps);
		Assertions.assertNotNull(testDate);
		Assertions.assertTrue(testDate.getTime() > 0);
		EasyMock.verify(mockedParam, mockedProps, mockedFormat);
	}

	@Test
	void testGetDateParameterWithIllegalAll() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final Parameter mockedFormat = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedFormatKey = "formatKey";
		final String expectedFormat = "yyyy/MM-dd";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedProps.get(expectedKey)).andReturn("2029.03-14").once();
		EasyMock.expect(mockedParam.getDateFormat()).andReturn(new SimpleDateFormat("yyyy-MM-dd")).once();
		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(mockedFormat).once();
		EasyMock.expect(mockedFormat.getKey()).andReturn(expectedFormatKey).once();
		EasyMock.expect(mockedFormat.getDefaultValue()).andReturn(expectedFormat).once();
		EasyMock.expect(mockedProps.get(expectedFormatKey, expectedFormat)).andReturn(expectedFormat).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedParam, mockedProps, mockedFormat);
		Assertions.assertThrows(IllegalParameterDateException.class, () -> ParameterDate.getDate(mockedParam, mockedProps));
		EasyMock.verify(mockedParam, mockedProps, mockedFormat);
	}

	@Test
	void testGetDateParameterWithValue() {
		final ParameterDate mockedParam = EasyMock.createStrictMock(ParameterDate.class);
		final HasProperties mockedProps = EasyMock.createStrictMock(HasProperties.class);
		final Parameter mockedFormat = EasyMock.createStrictMock(Parameter.class);
		final String expectedKey = "key";
		final String expectedFormatKey = "formatKey";
		final String expectedFormat = "yyyy-MM-dd";
		final Date testDate;

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedProps.get(expectedKey)).andReturn("2029-03-14").once();
		EasyMock.expect(mockedParam.getDateFormat()).andReturn(new SimpleDateFormat("yyyy/MM/dd")).once();
		EasyMock.expect(mockedParam.getParameterFormat()).andReturn(mockedFormat).once();
		EasyMock.expect(mockedFormat.getKey()).andReturn(expectedFormatKey).once();
		EasyMock.expect(mockedFormat.getDefaultValue()).andReturn(expectedFormat).once();
		EasyMock.expect(mockedProps.get(expectedFormatKey, expectedFormat)).andReturn(expectedFormat).once();
		EasyMock.replay(mockedParam, mockedProps, mockedFormat);
		testDate = ParameterDate.getDate(mockedParam, mockedProps);
		Assertions.assertNotNull(testDate);
		Assertions.assertTrue(testDate.getTime() > 0);
		EasyMock.verify(mockedParam, mockedProps, mockedFormat);
	}

	@Test
	void testGetDateValue() {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM--dd hh.mm.ss");
		final Date testDate = ParameterDate.getDate(dateFormat, "2029-03--14 15.09.16");

		Assertions.assertNotNull(testDate);
		Assertions.assertTrue(testDate.getTime() > 0);
	}

	@Test
	void testWithDateFormat() {
		final String expectedKey = "key";
		final DateFormat expectedFormat = DateFormat.getDateInstance();
		final ParameterDate testParameter = ParameterDate.withDateFormat(expectedKey, (DateFormat)expectedFormat.clone());

		Assertions.assertNotNull(testParameter);
		Assertions.assertEquals(expectedFormat, testParameter.getDateFormat());
		Assertions.assertEquals(expectedKey, testParameter.getKey());
		Assertions.assertNull(testParameter.getDefaultValue());
	}

	@Test
	void testWithKeyValue() {
		final String expectedKey = "key";
		final ParameterDate testParameter = ParameterDate.withKeyValue(expectedKey);

		Assertions.assertNotNull(testParameter);
		Assertions.assertNotNull(testParameter.getDateFormat());
		Assertions.assertEquals(expectedKey, testParameter.getKey());
		Assertions.assertNull(testParameter.getDefaultValue());
	}

	@Test
	void testWithParameterFormat() {
		final String expectedKey = "key";
		final Parameter mockedParameter = EasyMock.createStrictMock(Parameter.class);
		final ParameterDate testParameter = ParameterDate.withParameterFormat(expectedKey, mockedParameter);

		EasyMock.replay(mockedParameter);
		Assertions.assertNotNull(testParameter);
		Assertions.assertNotNull(testParameter.getDateFormat());
		Assertions.assertEquals(expectedKey, testParameter.getKey());
		Assertions.assertSame(mockedParameter, testParameter.getParameterFormat());
		Assertions.assertNull(testParameter.getDefaultValue());
		EasyMock.verify(mockedParameter);
	}
}
