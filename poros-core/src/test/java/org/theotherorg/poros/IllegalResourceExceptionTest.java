package org.theotherorg.poros;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IllegalResourceExceptionTest {
	@Test
	void testFromResourceName() {
		final String expectedName = "name";
		final IllegalResourceException testException = IllegalResourceException.from(expectedName);

		Assertions.assertNotNull(testException);
		Assertions.assertEquals(expectedName, testException.getResourceName());
		Assertions.assertNotNull(testException.getMessage());
		Assertions.assertThrows(IllegalResourceException.class, () -> {
			throw testException;
		});
	}

	@Test
	void testFrom1() {
		final String expectedName = "name";
		final IllegalResourceException testException = IllegalResourceException.from(expectedName, new RuntimeException());

		Assertions.assertNotNull(testException);
		Assertions.assertEquals(expectedName, testException.getResourceName());
		Assertions.assertNotNull(testException.getMessage());
		Assertions.assertThrows(IllegalResourceException.class, () -> {
			throw testException;
		});
	}

	@Test
	void testGetResourceName() {
		final String expectedResource = "resource";
		final IllegalResourceException testException = new IllegalResourceException(expectedResource);

		Assertions.assertEquals(expectedResource, testException.getResourceName());
		Assertions.assertThrows(IllegalResourceException.class, () -> {
			throw testException;
		});
	}
}
