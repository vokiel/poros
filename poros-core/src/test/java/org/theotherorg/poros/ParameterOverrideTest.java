package org.theotherorg.poros;

import java.time.Period;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ParameterOverrideTest {
	private HasProperties mockedProperties;
	private Map<String, String> testEnvironmentMap;
	private Properties testSystemProperties;

	@BeforeEach
	void setUp() {
		mockedProperties = EasyMock.createStrictMock(HasProperties.class);
		testEnvironmentMap = new HashMap<>();
		testSystemProperties = new Properties();
	}

	@Test
	void testGetBoolProperty() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(Boolean.TRUE.toString()).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertTrue(ParameterOverride.getBoolProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetDoubleIllegalValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		testSystemProperties.put(expectedKey, "illegalDouble");
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getDoubleProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetDoubleNullValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getDoubleProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetDoubleValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final double expectedValue = 13.37d;

		testSystemProperties.put(expectedKey, String.valueOf(expectedValue));
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getDoubleProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetFloatIllegalValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		testSystemProperties.put(expectedKey, "illegalFloat");
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getFloatProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetFloatNullValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getFloatProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetFloatValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final double expectedValue = 13.37f;

		testSystemProperties.put(expectedKey, String.valueOf(expectedValue));
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getFloatProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetIntIllegalValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		testSystemProperties.put(expectedKey, "illegalInt");
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getIntProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetIntNullValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getIntProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetIntValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final int expectedValue = 37;

		testSystemProperties.put(expectedKey, String.valueOf(expectedValue));
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getIntProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetLongIllegalValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		testSystemProperties.put(expectedKey, "illegalLong");
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getLongProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetLongNullValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getLongProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetLongValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final long expectedValue = 37L;

		testSystemProperties.put(expectedKey, String.valueOf(expectedValue));
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getLongProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testGetNullParameter() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> ParameterOverride.getRawProperty(null, null, null, null));
	}

	@Test
	void testGetPeriodIllegalValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		testSystemProperties.put(expectedKey, "illegalPeriod");
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getPeriodProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetPeriodNullValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertNull(ParameterOverride.getPeriodProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetPeriodValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final Period expectedValue = Period.parse("P37D");

		testSystemProperties.put(expectedKey, expectedValue.toString());
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getPeriodProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetPropertyEmptyValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedValue = "";

		testSystemProperties.put(expectedKey, expectedValue);
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(null).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertNull(ParameterOverride.getProperty(mockedParam, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetPropertyNullValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertNull(ParameterOverride.getProperty(mockedParam, null, null, null));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetPropertyTrimmedValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedValue = " trimValue ";

		testSystemProperties.put(expectedKey, expectedValue);
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue.trim(), ParameterOverride.getProperty(mockedParam, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetShortIllegalValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";

		testSystemProperties.put(expectedKey, "illegalShort");
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).times(2);
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getShortProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetShortNullValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedEnvKey = "envKey";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(null).once();
		EasyMock.expect(mockedProperties.get(expectedKey, null)).andReturn(null).once();
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertThrows(IllegalParameterException.class, () -> ParameterOverride.getShortProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testGetShortValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final short expectedValue = 37;

		testSystemProperties.put(expectedKey, String.valueOf(expectedValue));
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getShortProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testRawPropertyConfValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedValue = "confValue";
		final String expectedDefaultValue = "defaultValue";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefaultValue).once();
		EasyMock.expect(mockedProperties.get(expectedKey, expectedDefaultValue)).andReturn(expectedValue).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getRawProperty(mockedParam, mockedProperties, null, null));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testRawPropertyDefaultValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedDefaultValue = "defaultValue";

		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getDefaultValue()).andReturn(expectedDefaultValue).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedDefaultValue, ParameterOverride.getRawProperty(mockedParam, null, null, null));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testRawPropertyEnvAltKeyValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";
		final String expectedValue = "envValue";

		testEnvironmentMap.put(expectedKey, expectedValue);
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getRawProperty(mockedParam, mockedProperties, testEnvironmentMap, null));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testRawPropertyEnvValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedEnvKey = "envKey";
		final String expectedKey = "key";
		final String expectedValue = "envValue";

		testEnvironmentMap.put(expectedEnvKey, expectedValue);
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.expect(mockedParam.getEnvKey()).andReturn(expectedEnvKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getRawProperty(mockedParam, mockedProperties, testEnvironmentMap, null));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testRawPropertySystemValue() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);
		final String expectedKey = "key";
		final String expectedValue = "systemValue";

		testSystemProperties.put(expectedKey, expectedValue);
		EasyMock.expect(mockedParam.getKey()).andReturn(expectedKey).once();
		EasyMock.replay(mockedProperties, mockedParam);
		Assertions.assertEquals(expectedValue, ParameterOverride.getRawProperty(mockedParam, mockedProperties, testEnvironmentMap, testSystemProperties));
		EasyMock.verify(mockedProperties, mockedParam);
	}

	@Test
	void testWithKeysValue() {
		Assertions.assertNotNull(ParameterOverride.withKeysValue("key", "envKey", null));
	}
}
