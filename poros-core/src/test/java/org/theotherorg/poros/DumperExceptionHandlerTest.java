package org.theotherorg.poros;

import java.io.PrintStream;
import java.util.Collections;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.Arguments.DumperExceptionHandler;

class DumperExceptionHandlerTest {
	@Test
	void testUncaughtExceptionNullParam() {
		final PrintStream mockedStream = EasyMock.createStrictMock(PrintStream.class);
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final DumperExceptionHandler testHandler = new DumperExceptionHandler(mockedStream, mockedParams);
		final Thread mockedThread = EasyMock.createStrictMock(Thread.class);
		final IllegalParameterException mockedIllegal = EasyMock.createStrictMock(IllegalParameterException.class);

		EasyMock.expect(mockedIllegal.getParameter()).andReturn(null).once();
		EasyMock.replay(mockedStream, mockedParams, mockedThread, mockedIllegal);
		testHandler.uncaughtException(mockedThread, mockedIllegal);
		EasyMock.verify(mockedStream, mockedParams, mockedThread, mockedIllegal);
	}

	@Test
	void testUncaughtException() {
		final PrintStream mockedStream = EasyMock.createStrictMock(PrintStream.class);
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final DumperExceptionHandler testHandler = new DumperExceptionHandler(mockedStream, mockedParams);
		final Thread mockedThread = EasyMock.createStrictMock(Thread.class);
		final IllegalParameterException mockedIllegal = EasyMock.createStrictMock(IllegalParameterException.class);
		final String expectedWritten = "written";
		final String expectedParamsWritten = "params";

		EasyMock.expect(mockedIllegal.getParameter()).andReturn(mockedParam).once();
		EasyMock.expect(mockedParam.write(EasyMock.anyObject(ParameterWriter.class))).andReturn(expectedWritten).once();
		mockedStream.print(EasyMock.eq(expectedWritten));
		EasyMock.expectLastCall().once();
		mockedStream.println();
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedParam.resolveIn(mockedParams)).andReturn(Collections.singleton(expectedParamsWritten)).once();
		mockedStream.print(EasyMock.contains(expectedParamsWritten));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedStream, mockedParams, mockedParam, mockedThread, mockedIllegal);
		testHandler.uncaughtException(mockedThread, mockedIllegal);
		EasyMock.verify(mockedStream, mockedParams, mockedParam, mockedThread, mockedIllegal);
	}

	@Test
	void testUncaughtRuntimeException() {
		final PrintStream mockedStream = EasyMock.createStrictMock(PrintStream.class);
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final DumperExceptionHandler testHandler = new DumperExceptionHandler(mockedStream, mockedParams);
		final Thread mockedThread = EasyMock.createStrictMock(Thread.class);

		EasyMock.replay(mockedStream, mockedParams, mockedThread);
		testHandler.uncaughtException(mockedThread, new RuntimeException());
		EasyMock.verify(mockedStream, mockedParams, mockedThread);
	}
}
