package org.theotherorg.poros;

import java.io.PrintStream;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.Arguments.ArgumentsExceptionHandler;

class ArgumentsExceptionHandlerTest {
	@Test
	void testUncaughtException() {
		final PrintStream mockedStream = EasyMock.createStrictMock(PrintStream.class);
		final ArgumentsExceptionHandler testHandler = new ArgumentsExceptionHandler(mockedStream);
		final Thread mockedThread = EasyMock.createStrictMock(Thread.class);
		final Throwable mockedThrowable = EasyMock.createStrictMock(Throwable.class);
		final String expectedMessage = "message";

		EasyMock.expect(mockedThrowable.getMessage()).andReturn(expectedMessage).once();
		mockedStream.println(EasyMock.contains(expectedMessage));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedStream, mockedThread, mockedThrowable);
		testHandler.uncaughtException(mockedThread, mockedThrowable);
		EasyMock.verify(mockedStream, mockedThread, mockedThrowable);
	}
}
