package org.theotherorg.poros;

import java.io.PrintStream;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.Arguments.TraceExceptionHandler;

class TraceExceptionHandlerTest {
	@Test
	void testUncaughtException() {
		final PrintStream mockedStream = EasyMock.createStrictMock(PrintStream.class);
		final TraceExceptionHandler testHandler = new TraceExceptionHandler(mockedStream);
		final Thread mockedThread = EasyMock.createStrictMock(Thread.class);
		final Throwable mockedThrowable = EasyMock.createStrictMock(Throwable.class);

		mockedThrowable.printStackTrace(mockedStream);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedStream, mockedThread, mockedThrowable);
		testHandler.uncaughtException(mockedThread, mockedThrowable);
		EasyMock.verify(mockedStream, mockedThread, mockedThrowable);
	}
}
