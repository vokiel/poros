package org.theotherorg.poros;

import java.text.DateFormat;
import java.util.Collections;

import com.google.common.collect.Iterables;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ParameterDateImplTest {
	private ParameterDateImpl testParameterDate;

	@BeforeEach
	void setUp() {
		testParameterDate = new ParameterDateImpl("key", DateFormat.getDateInstance());
	}

	@SuppressWarnings("All")
	@Test
	void testEquals() {
		Assertions.assertTrue(testParameterDate.equals(testParameterDate));
		Assertions.assertFalse(testParameterDate.equals(this));
		Assertions.assertFalse(testParameterDate.equals(new ParameterDateImpl("otherKey", DateFormat.getDateInstance())));
		Assertions.assertNotEquals(testParameterDate, new ParameterDateImpl("key", DateFormat.getDateTimeInstance()));
		Assertions.assertEquals(testParameterDate, new ParameterDateImpl("key", DateFormat.getDateInstance()));
	}

	@Test
	void testGetDateFormat() {
		Assertions.assertNotNull(testParameterDate.getDateFormat());
	}

	@Test
	void testGetParameterFormat() {
		Assertions.assertNull(testParameterDate.getParameterFormat());
	}

	@Test
	void testHashCode() {
		Assertions.assertTrue(testParameterDate.hashCode() != 0);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testIllegalDateFormat() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ParameterDateImpl("key", null));
	}

	@Test
	void testResolveIn() {
		final HasParameters mockedParams = EasyMock.createStrictMock(HasParameters.class);
		final DateFormat mockedDateFormat = EasyMock.createStrictMock(DateFormat.class);
		final ParameterDateImpl testParam = new ParameterDateImpl("key", mockedDateFormat);
		final Iterable<String> testResolutions;

		EasyMock.expect(mockedParams.resolve(testParam)).andReturn(Collections.emptyList()).once();
		EasyMock.replay(mockedParams, mockedDateFormat);
		testResolutions = testParam.resolveIn(mockedParams);
		Assertions.assertNotNull(testResolutions);
		Assertions.assertTrue(Iterables.isEmpty(testResolutions));
		EasyMock.verify(mockedParams, mockedDateFormat);
	}

	@Test
	void testSetParameterFormat() {
		final Parameter mockedParam = EasyMock.createStrictMock(Parameter.class);

		EasyMock.replay(mockedParam);
		testParameterDate.setParameterFormat(mockedParam);
		Assertions.assertSame(mockedParam, testParameterDate.getParameterFormat());
		EasyMock.verify(mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testSetParameterFormatIllegal() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> testParameterDate.setParameterFormat(null));
	}

	@Test
	void testWrite() {
		final ParameterWriter mockedWriter = EasyMock.createStrictMock(ParameterWriter.class);
		final DateFormat mockedDateFormat = EasyMock.createStrictMock(DateFormat.class);
		final ParameterDateImpl testParam = new ParameterDateImpl("key", mockedDateFormat);
		final String expectedResult = "result";

		EasyMock.expect(mockedWriter.write(testParam)).andReturn(expectedResult).once();
		EasyMock.replay(mockedWriter, mockedDateFormat);
		Assertions.assertEquals(expectedResult, testParam.write(mockedWriter));
		EasyMock.verify(mockedWriter, mockedDateFormat);
	}
}
