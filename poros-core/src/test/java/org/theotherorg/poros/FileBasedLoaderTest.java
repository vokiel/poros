package org.theotherorg.poros;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FileBasedLoaderTest {
	private FileBasedLoader testLoader;

	@BeforeEach
	void setUp() {
		testLoader = new FileBasedLoader();
	}

	@Test
	void testLoadResourceNotFound() {
		final String expectedName = "/testParameters.properties";

		Assertions.assertThrows(IllegalResourceException.class, () -> testLoader.load(expectedName));
	}


	@Test
	void testLoadResource() throws Exception {
		final Path expectedPath = Files.createTempFile(this.getClass().getSimpleName(), null);
		final String expectedName = expectedPath.toString();
		final Properties props = testLoader.load(expectedName);

		Assertions.assertNotNull(props);
		Assertions.assertTrue(props.isEmpty());
		Files.deleteIfExists(expectedPath);
	}
}
