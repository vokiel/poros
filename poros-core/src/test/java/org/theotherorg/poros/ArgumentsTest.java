package org.theotherorg.poros;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Collections;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.Arguments.Builder;
import org.theotherorg.poros.Arguments.CompositeExceptionHandler;
import org.theotherorg.poros.Arguments.Handler;

class ArgumentsTest {
	@Test
	void testHandleNullConsumer() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> Arguments.from().handle(null));
	}

	@Test
	void testWithConfHandlerNoArgs() {
		final Builder mockedBuilder = EasyMock.createStrictMock(Builder.class);

		EasyMock.expect(mockedBuilder.withConfigurations(Collections.emptyList())).andReturn(mockedBuilder).once();
		EasyMock.replay(mockedBuilder);
		Arguments.withConfHandler(mockedBuilder);
		EasyMock.verify(mockedBuilder);
	}

	@Test
	void testWithDefaults() {
		final Builder testBuilder = Arguments.withDefaults();

		Assertions.assertNotNull(testBuilder);
		Assertions.assertNull(testBuilder.getParameters());
		Assertions.assertNull(testBuilder.getExceptionHandler());
		Assertions.assertNotNull(testBuilder.getConfHandler());
	}

	@Test
	void testWithDumperHandlerComposite() {
		final Builder mockedBuilder = EasyMock.createStrictMock(Builder.class);
		final Parameters mockedParameters = EasyMock.createStrictMock(Parameters.class);
		final UncaughtExceptionHandler mockedHandler = EasyMock.createStrictMock(UncaughtExceptionHandler.class);

		EasyMock.expect(mockedBuilder.getParameters()).andReturn(mockedParameters).once();
		EasyMock.expect(mockedBuilder.getExceptionHandler()).andReturn(mockedHandler).once();
		mockedBuilder.setExceptionHandler(EasyMock.anyObject(CompositeExceptionHandler.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBuilder, mockedHandler, mockedParameters);
		Arguments.withDumperHandler(mockedBuilder, "--dumper");
		EasyMock.verify(mockedBuilder, mockedHandler, mockedParameters);
	}

	@Test
	void testWithDumperHandlerNoArgs() {
		final Builder mockedBuilder = EasyMock.createStrictMock(Builder.class);

		EasyMock.replay(mockedBuilder);
		Arguments.withDumperHandler(mockedBuilder);
		EasyMock.verify(mockedBuilder);
	}

	@Test
	void testWithDumperHandlerNoComposite() {
		final Builder mockedBuilder = EasyMock.createStrictMock(Builder.class);
		final Parameters mockedParameters = EasyMock.createStrictMock(Parameters.class);

		EasyMock.expect(mockedBuilder.getParameters()).andReturn(mockedParameters).once();
		EasyMock.expect(mockedBuilder.getExceptionHandler()).andReturn(null).once();
		mockedBuilder.setExceptionHandler(EasyMock.anyObject(UncaughtExceptionHandler.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBuilder, mockedParameters);
		Arguments.withDumperHandler(mockedBuilder, "--dumper");
		EasyMock.verify(mockedBuilder, mockedParameters);
	}

	@Test
	void testWithHandler() {
		final Handler mockedHandler = EasyMock.createStrictMock(Handler.class);
		final Builder testBuilder;

		EasyMock.replay(mockedHandler);
		testBuilder = Arguments.withHandler(mockedHandler);
		Assertions.assertNotNull(testBuilder);
		Assertions.assertNull(testBuilder.getParameters());
		Assertions.assertNull(testBuilder.getExceptionHandler());
		Assertions.assertNotNull(testBuilder.getConfHandler());
		EasyMock.verify(mockedHandler);
	}

	@Test
	void testWithResource() {
		final ParameterOverride mockedParam = EasyMock.createStrictMock(ParameterOverride.class);

		EasyMock.replay(mockedParam);
		Assertions.assertNotNull(Arguments.withResource(mockedParam));
		EasyMock.verify(mockedParam);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testWithResourceNullParameter() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> Arguments.withResource(null));
	}

	@Test
	void testWithTracerHandlerComposite() {
		final Builder mockedBuilder = EasyMock.createStrictMock(Builder.class);
		final UncaughtExceptionHandler mockedHandler = EasyMock.createStrictMock(UncaughtExceptionHandler.class);

		EasyMock.expect(mockedBuilder.getExceptionHandler()).andReturn(mockedHandler).once();
		mockedBuilder.setExceptionHandler(EasyMock.anyObject(CompositeExceptionHandler.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBuilder, mockedHandler);
		Arguments.withTracerHandler(mockedBuilder, "--stacktrace");
		EasyMock.verify(mockedBuilder, mockedHandler);
	}

	@Test
	void testWithTracerHandlerNoArgs() {
		final Builder mockedBuilder = EasyMock.createStrictMock(Builder.class);

		EasyMock.replay(mockedBuilder);
		Arguments.withTracerHandler(mockedBuilder);
		EasyMock.verify(mockedBuilder);
	}

	@Test
	void testWithTracerHandlerNoComposite() {
		final Builder mockedBuilder = EasyMock.createStrictMock(Builder.class);

		EasyMock.expect(mockedBuilder.getExceptionHandler()).andReturn(null).once();
		mockedBuilder.setExceptionHandler(EasyMock.anyObject(UncaughtExceptionHandler.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBuilder);
		Arguments.withTracerHandler(mockedBuilder, "--stacktrace");
		EasyMock.verify(mockedBuilder);
	}
}
