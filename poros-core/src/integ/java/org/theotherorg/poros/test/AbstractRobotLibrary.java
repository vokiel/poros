package org.theotherorg.poros.test;

import com.google.common.base.Strings;

abstract class AbstractRobotLibrary implements TestOutput {
	private static final class CapturingOutput implements TestOutput {
		private final AbstractRobotLibrary library;

		public CapturingOutput(final AbstractRobotLibrary library) {
			this.library = library;
		}

		@Override
		public void println(final String arg) {
			library.value = arg;
		}

		@Override
		public void println(final Object object) {
			library.value = String.valueOf(object);
		}
	}

	private final TestOutput output;
	private String value;

	public AbstractRobotLibrary() {
		this.output = new CapturingOutput(this);
	}

	public AbstractRobotLibrary(final TestOutput output) {
		this.output = output;
	}

	@Override
	public void println(final Object object) {
		output.println(object);
	}

	public void println(final String line) {
		output.println(line);
	}

	@SuppressWarnings("unused")
	public void shouldEqual(final String value) {
		if (!Strings.nullToEmpty(this.value).equals(value)) {
			throw new IllegalStateException("Expected [" + value + "], but got [" + this.value + "]");
		}
	}
}
