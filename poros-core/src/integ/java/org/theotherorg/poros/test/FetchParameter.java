package org.theotherorg.poros.test;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;

import org.theotherorg.poros.Arguments;
import org.theotherorg.poros.Arguments.Builder;
import org.theotherorg.poros.Arguments.Handler;
import org.theotherorg.poros.HasParameters;
import org.theotherorg.poros.ParameterDate;
import org.theotherorg.poros.ParameterOverride;
import org.theotherorg.poros.Parameters;

/**
 * This procedure simply initiates an {@link Arguments} builder with the provided arguments and a resource parameter to look for. It adds 3 custom arguments --param, --type & --default which are used to print the value of a parameter found
 * in the resulting {@link Parameters} configurations.
 */
@SuppressWarnings("unused")
public class FetchParameter extends AbstractRobotLibrary {
	private static final ParamArgumentHandler paramHandler = new ParamArgumentHandler();

	public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

	public enum Params {
		DateFormat(ParameterOverride.withKeysValue("org.theotherorg.poros.DateFormat", "POROS_DF", "yyyy/MM/dd")),
		Resource(ParameterOverride.withKeysValue("org.theotherorg.poros.Resource", "POROS_RES", "/default.properties"));

		private final ParameterOverride parameterOverride;

		Params(final ParameterOverride parameterOverride) {
			this.parameterOverride = parameterOverride;
		}

		public ParameterOverride asParameter() {
			return parameterOverride;
		}
	}

	private static final class ParamArgumentHandler implements Handler {
		private String defaultValue;
		private String paramKey;
		private String paramType;

		public String getDefaultValue() {
			return defaultValue;
		}

		public String getParamKey() {
			return paramKey;
		}

		public String getParamType() {
			return paramType;
		}

		@Override
		public void handle(@Nonnull final Builder builder, final String... args) {
			final List<String> argsList = Arrays.asList(args);
			final int paramIndex = Iterables.indexOf(argsList, "--param"::equals);
			final int typeIndex;
			final int defaultIndex;

			Preconditions.checkArgument(paramIndex >= 0 && paramIndex + 1 < args.length, FetchParameter.usage("no --param specified"));
			paramKey = args[paramIndex + 1];
			typeIndex = Iterables.indexOf(argsList, "--type"::equals);
			Preconditions.checkArgument(typeIndex + 1 < args.length, FetchParameter.usage("no value for --type"));
			defaultIndex = Iterables.indexOf(argsList, "--default"::equals);

			if (typeIndex < 0) {
				paramType = "string";
			} else {
				paramType = args[typeIndex + 1].toLowerCase();
			}

			if (defaultIndex > 0 && defaultIndex + 1 < args.length) {
				defaultValue = args[defaultIndex + 1];
			}
		}
	}

	public FetchParameter() {
		super();
	}

	public FetchParameter(final PrintStream outStream) {
		super(TestOutput.printStream(outStream));
	}

	public void fetch(final String key, final String type) {
		Arguments.withResource(Params.Resource.asParameter())
			.withHandler(paramHandler)
			.from("--param", key, "--type", type)
			.handle(this::fetch);
	}

	public void fetch(final String key, final String type, final String defaultValue) {
		Arguments.withResource(Params.Resource.asParameter())
			.withHandler(paramHandler)
			.from("--param", key, "--type", type, "--default", defaultValue)
			.handle(this::fetch);
	}

	void fetch(final HasParameters parameters) {
		final String key = paramHandler.getParamKey();
		final String type = paramHandler.getParamType();

		Objects.requireNonNull(parameters);

		if (key != null) {
			final ParameterOverride override = ParameterOverride.withKeysValue(key, key, paramHandler.getDefaultValue());

			if ("string".equals(type)) {
				println(parameters.get(override));
			} else if ("bool".equals(type)) {
				println(parameters.isTrue(override));
			} else if ("date".equals(type)) {
				println(parameters.getDate(ParameterDate.withParameterFormat(key, Params.DateFormat.asParameter())));
			} else if ("double".equals(type)) {
				println(parameters.getDouble(override));
			} else if ("float".equals(type)) {
				println(parameters.getFloat(override));
			} else if ("int".equals(type)) {
				println(parameters.getInt(override));
			} else if ("long".equals(type)) {
				println(parameters.getLong(override));
			} else if ("period".equals(type)) {
				println(parameters.getPeriod(override).toString());
			} else if ("short".equals(type)) {
				println(parameters.getShort(override));
			}
		}
	}

	public static void main(final String... args) {
		Arguments.withResource(Params.Resource.asParameter())
			.withHandler(paramHandler)
			.from(args)
			.handle(new FetchParameter(System.out)::fetch);
	}

	static String usage(final String message) {
		return "Error: " + message + "\nUsage: java -cp <classpath> org.theotherorg.poros.test.FetchParameter [--conf <conf>...][--dumper][--stacktrace] --param <key> [--type <type]";
	}
}
