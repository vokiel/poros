package org.theotherorg.poros.test;

import java.io.PrintStream;

public interface TestOutput {
	void println(String line);

	void println(Object object);

	static TestOutput printStream(final PrintStream printStream) {
		return new TestOutput() {
			@Override
			public void println(final String line) {
				printStream.println(line);
			}

			@Override
			public void println(final Object object) {
				printStream.println(object);
			}
		};
	}
}
