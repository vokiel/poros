*** Settings ***
Documentation    Run default fetch parameter cases
Library          org.theotherorg.poros.test.FetchParameter
Test Template    Fetch parameter with type

*** Test Cases ***
Test Fetch String
    org.theotherorg.poros.Testing        string    One,Two
Test Fetch Boolean
    org.theotherorg.poros.TestBoolean    bool      true
Test Fetch Date
    org.theotherorg.poros.TestDate       date      Thu Dec 13 00:00:00 EST 2007
Test Fetch Double
    org.theotherorg.poros.TestDouble     double    13.37
Test Fetch Float
    org.theotherorg.poros.TestFloat      float     13.07
Test Fetch Integer
    org.theotherorg.poros.TestInt        int       37
Test Fetch Long
    org.theotherorg.poros.TestLong       long      36
Test Fetch Period
    org.theotherorg.poros.TestPeriod     period    P13D
Test Fetch Short
    org.theotherorg.poros.TestShort      short     35


*** Keywords ***
Fetch parameter with type
    [arguments]     ${param}    ${type}    ${value}
    Fetch           ${param}    ${type}
    Should Equal    ${value}
