/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Set;

import org.gradle.api.tasks.InputFiles;

public class AbstractSourcedTask<T> extends AbstractRobotTask<T> {
	private Set<File> sourceFiles;

	@InputFiles
	public Set<File> getSourceFiles() {
		return sourceFiles;
	}

	void setSourceFiles(final Set<File> sourceFiles) {
		this.sourceFiles = sourceFiles;
	}
}
