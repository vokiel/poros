/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.io.File;
import java.util.List;

import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.OutputFile;
import org.theotherorg.poros.robot.RobotRunnerBridge.ReportBuilder.HasReport;

class AbstractReportingProto extends AbstractBaseProto implements HasReport {
	private String consoleColors;
	private List<String> criticals;
	private List<String> flattenKeywords;
	private File log;
	private String logLevel;
	private String logTitle;
	private boolean noStatusRc;
	private List<String> nonCriticals;
	private File output;
	private File outputDir;
	private List<String> preRebotModifiers;
	private List<String> removeKeywords;
	private File report;
	private String reportBackground;
	private String reportTitle;
	private boolean rpa;
	private boolean splitLog;
	private int suiteStatLevel;
	private List<String> tagDocs;
	private List<String> tagStatCombines;
	private List<String> tagStatExcludes;
	private List<String> tagStatIncludes;
	private List<String> tagStatLinks;
	private List<String> tasks;
	private boolean timeStampOutputs;
	private File xunit;
	private boolean xunitSkipNonCritical;

	@Optional
	@Input
	@Override
	public String getConsoleColors() {
		return consoleColors;
	}

	public void setConsoleColors(final String consoleColors) {
		this.consoleColors = consoleColors;
	}

	@Optional
	@Input
	@Override
	public List<String> getCriticals() {
		return criticals;
	}

	public void setCriticals(final List<String> criticals) {
		this.criticals = criticals;
	}

	@Optional
	@Input
	@Override
	public List<String> getFlattenKeywords() {
		return flattenKeywords;
	}

	public void setFlattenKeywords(final List<String> flattenKeywords) {
		this.flattenKeywords = flattenKeywords;
	}

	@Optional
	@OutputFile
	@Override
	public File getLog() {
		return log;
	}

	public void setLog(final File log) {
		this.log = log;
	}

	@Optional
	@Input
	@Override
	public String getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(final String logLevel) {
		this.logLevel = logLevel;
	}

	@Optional
	@Input
	@Override
	public String getLogTitle() {
		return logTitle;
	}

	public void setLogTitle(final String logTitle) {
		this.logTitle = logTitle;
	}

	@Optional
	@Input
	@Override
	public List<String> getNonCriticals() {
		return nonCriticals;
	}

	public void setNonCriticals(final List<String> nonCriticals) {
		this.nonCriticals = nonCriticals;
	}

	@Optional
	@OutputFile
	@Override
	public File getOutput() {
		return output;
	}

	public void setOutput(final File output) {
		this.output = output;
	}

	@OutputDirectory
	@Override
	public File getOutputDir() {
		return outputDir;
	}

	public void setOutputDir(final File outputDir) {
		this.outputDir = outputDir;
	}

	@Optional
	@Input
	@Override
	public List<String> getPreRebotModifiers() {
		return preRebotModifiers;
	}

	public void setPreRebotModifiers(final List<String> preRebotModifiers) {
		this.preRebotModifiers = preRebotModifiers;
	}

	@Optional
	@Input
	@Override
	public List<String> getRemoveKeywords() {
		return removeKeywords;
	}

	public void setRemoveKeywords(final List<String> removeKeywords) {
		this.removeKeywords = removeKeywords;
	}

	@Optional
	@OutputFile
	@Override
	public File getReport() {
		return report;
	}

	public void setReport(final File report) {
		this.report = report;
	}

	@Optional
	@Input
	@Override
	public String getReportBackground() {
		return reportBackground;
	}

	public void setReportBackground(final String reportBackground) {
		this.reportBackground = reportBackground;
	}

	@Optional
	@Input
	@Override
	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(final String reportTitle) {
		this.reportTitle = reportTitle;
	}

	@Input
	@Override
	public int getSuiteStatLevel() {
		return suiteStatLevel;
	}

	public void setSuiteStatLevel(final int suiteStatLevel) {
		this.suiteStatLevel = suiteStatLevel;
	}

	@Optional
	@Input
	@Override
	public List<String> getTagDocs() {
		return tagDocs;
	}

	public void setTagDocs(final List<String> tagDocs) {
		this.tagDocs = tagDocs;
	}

	@Optional
	@Input
	@Override
	public List<String> getTagStatCombines() {
		return tagStatCombines;
	}

	public void setTagStatCombines(final List<String> tagStatCombines) {
		this.tagStatCombines = tagStatCombines;
	}

	@Optional
	@Input
	@Override
	public List<String> getTagStatExcludes() {
		return tagStatExcludes;
	}

	public void setTagStatExcludes(final List<String> tagStatExcludes) {
		this.tagStatExcludes = tagStatExcludes;
	}

	@Optional
	@Input
	@Override
	public List<String> getTagStatIncludes() {
		return tagStatIncludes;
	}

	public void setTagStatIncludes(final List<String> tagStatIncludes) {
		this.tagStatIncludes = tagStatIncludes;
	}

	@Optional
	@Input
	@Override
	public List<String> getTagStatLinks() {
		return tagStatLinks;
	}

	public void setTagStatLinks(final List<String> tagStatLinks) {
		this.tagStatLinks = tagStatLinks;
	}

	@Optional
	@Input
	@Override
	public List<String> getTasks() {
		return tasks;
	}

	public void setTasks(final List<String> tasks) {
		this.tasks = tasks;
	}

	@Optional
	@InputFile
	@Override
	public File getXunit() {
		return xunit;
	}

	public void setXunit(final File xunit) {
		this.xunit = xunit;
	}

	@Input
	@Override
	public boolean isNoStatusRc() {
		return noStatusRc;
	}

	public void setNoStatusRc(final boolean noStatusRc) {
		this.noStatusRc = noStatusRc;
	}

	@Input
	@Override
	public boolean isRpa() {
		return rpa;
	}

	public void setRpa(final boolean rpa) {
		this.rpa = rpa;
	}

	@Input
	@Override
	public boolean isSplitLog() {
		return splitLog;
	}

	public void setSplitLog(final boolean splitLog) {
		this.splitLog = splitLog;
	}

	@Input
	@Override
	public boolean isTimeStampOutputs() {
		return timeStampOutputs;
	}

	public void setTimeStampOutputs(final boolean timeStampOutputs) {
		this.timeStampOutputs = timeStampOutputs;
	}

	@Input
	@Override
	public boolean isXunitSkipNonCritical() {
		return xunitSkipNonCritical;
	}

	public void setXunitSkipNonCritical(final boolean xunitSkipNonCritical) {
		this.xunitSkipNonCritical = xunitSkipNonCritical;
	}
}
