/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.io.File;

import javax.inject.Inject;

import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputDirectory;

class RobotLibdocProto {
	private String docFormat;
	private String format;
	private File outputDir;
	private String version;

	@Inject
	public RobotLibdocProto() {
	}

	@Optional
	@Input
	public String getDocFormat() {
		return docFormat;
	}

	public void setDocFormat(final String docFormat) {
		this.docFormat = docFormat;
	}

	@Optional
	@Input
	public String getFormat() {
		return format;
	}

	public void setFormat(final String format) {
		this.format = format;
	}

	@OutputDirectory
	public File getOutputDir() {
		return outputDir;
	}

	public void setOutputDir(final File outputDir) {
		this.outputDir = outputDir;
	}

	@Optional
	@Input
	public String getVersion() {
		return version;
	}

	public void setVersion(final String version) {
		this.version = version;
	}
}
