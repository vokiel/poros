/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import javax.inject.Inject;

import org.gradle.api.tasks.CacheableTask;
import org.gradle.api.tasks.TaskAction;

@CacheableTask
public class RobotRebotTask extends AbstractRobotTask<RobotRebotProto> {
	private final RobotRunnerBridge robotBridge;

	@Inject
	public RobotRebotTask(final RobotRunnerBridge robotBridge) {
		this.robotBridge = robotBridge;
	}

	@TaskAction
	public void robotRebot() {
		robotBridge.rebot(getRobotProto(), getLibraryClasspath());
	}
}
