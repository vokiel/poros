/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Date;
import java.util.Set;

import javax.inject.Inject;

import org.gradle.api.tasks.CacheableTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.PathSensitive;
import org.gradle.api.tasks.PathSensitivity;
import org.gradle.api.tasks.SkipWhenEmpty;
import org.gradle.api.tasks.TaskAction;

@CacheableTask
public class RobotRunTask extends AbstractRobotTask<RobotRunProto> {
	private final RobotRunnerBridge robotBridge;

	private Date runTimestamp;
	private Set<File> sourceFiles;

	@Inject
	public RobotRunTask(final RobotRunnerBridge robotBridge) {
		this.robotBridge = robotBridge;
	}

	@SkipWhenEmpty
	@InputFiles
	@PathSensitive(PathSensitivity.RELATIVE)
	public Set<File> getSourceFiles() {
		return sourceFiles;
	}

	void setSourceFiles(final Set<File> sourceFiles) {
		this.sourceFiles = sourceFiles;
	}

	@Optional
	@Input
	public Date getRunTimestamp() {
		return runTimestamp;
	}

	@TaskAction
	public void robotRun() {
		final RobotRunProto proto = getRobotProto();

		runTimestamp = proto.isDryRun() ? new Date() : null;
		robotBridge.run(getRobotProto(), sourceFiles, getLibraryClasspath());
	}
}
