/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Set;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.DependencySet;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.reporting.ReportingExtension;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.workers.WorkerExecutor;

public class RobotPlugin implements Plugin<Project> {
	static final class ClasspathConfiguration implements Supplier<Iterable<File>> {
		private final Configuration config;
		private final Supplier<SourceSet> sourceSetSupplier;
		private Iterable<File> classpath;

		public ClasspathConfiguration(final Configuration config, final Supplier<SourceSet> sourceSetSupplier) {
			this.config = config;
			this.sourceSetSupplier = sourceSetSupplier;
		}

		@Override
		public Iterable<File> get() {
			if (classpath == null) {
				final SourceSet sourceSet = sourceSetSupplier.get();
				final Set<File> configJars = config.getResolvedConfiguration().getFiles();

				configJars.addAll(sourceSet.getOutput().getFiles());
				configJars.addAll(sourceSet.getCompileClasspath().getFiles());
				classpath = configJars;
			}

			return classpath;
		}
	}

	static final class ConfigureDryRunAction implements Action<RobotRunTask> {
		private final Supplier<Iterable<File>> classpathConfig;
		private final RobotRunProto runProto;
		private final Supplier<SourceSet> sourceConfig;

		public ConfigureDryRunAction(final Supplier<Iterable<File>> classpathConfig, final Supplier<SourceSet> sourceConfig, final RobotRunProto runProto) {
			this.sourceConfig = sourceConfig;
			this.classpathConfig = classpathConfig;
			this.runProto = runProto;
		}

		@Override
		public void execute(final RobotRunTask task) {
			task.dependsOn(task.getProject().getTasks().findByName("compileIntegJava"));
			task.setDescription("Verifies test data without running tests");
			runProto.setDryRun(true);
			task.setRobotProto(runProto);
			task.setSourceFiles(sourceConfig.get().getResources().getFiles());
			task.setLibraryClasspath(classpathConfig.get());
		}
	}

	static final class ConfigureLibdocAction implements Action<RobotLibdocTask> {
		private final Supplier<Iterable<File>> classpathConfig;
		private final RobotLibdocProto libdocProto;
		private final Supplier<SourceSet> sourceConfig;

		public ConfigureLibdocAction(final Supplier<Iterable<File>> classpathConfig, final Supplier<SourceSet> sourceConfig, final RobotLibdocProto libdocProto) {
			this.classpathConfig = classpathConfig;
			this.sourceConfig = sourceConfig;
			this.libdocProto = libdocProto;
		}

		@Override
		public void execute(final RobotLibdocTask task) {
			task.setDescription("Generates keyword documentation in HTML or XML");
			task.setRobotProto(libdocProto);
			task.setLibraryClasspath(classpathConfig.get());
			task.setLibraryFiles(sourceConfig.get().getAllJava().getFiles());
		}
	}

	static final class ConfigureRebotAction implements Action<RobotRebotTask> {
		private final Supplier<Iterable<File>> classpathConfig;
		private final RobotRebotProto rebotProto;

		public ConfigureRebotAction(final Supplier<Iterable<File>> classpathConfig, final RobotRebotProto rebotProto) {
			this.classpathConfig = classpathConfig;
			this.rebotProto = rebotProto;
		}

		@Override
		public void execute(final RobotRebotTask task) {
			task.setDescription("Reformat logs and reports to HTML or XML");
			task.setRobotProto(rebotProto);
			task.setLibraryClasspath(classpathConfig.get());
		}
	}

	static final class ConfigureRunAction implements Action<RobotRunTask> {
		private final Supplier<Iterable<File>> classpathConfig;
		private final RobotRunProto runProto;
		private final Supplier<SourceSet> sourceConfig;

		public ConfigureRunAction(final Supplier<Iterable<File>> classpathConfig, final Supplier<SourceSet> sourceConfig, final RobotRunProto runProto) {
			this.classpathConfig = classpathConfig;
			this.sourceConfig = sourceConfig;
			this.runProto = runProto;
		}

		@Override
		public void execute(@Nonnull final RobotRunTask task) {
			task.dependsOn(task.getProject().getTasks().findByName("compileIntegJava"));
			task.setDescription("Runs robot tests");
			task.setRobotProto(runProto);
			task.setSourceFiles(sourceConfig.get().getResources().getFiles());
			task.setLibraryClasspath(classpathConfig.get());
		}
	}

	static final class ConfigureTestdocAction implements Action<RobotTestdocTask> {
		private final Supplier<Iterable<File>> classpathConfig;
		private final Supplier<SourceSet> sourceConfig;
		private final RobotTestdocProto testdocProto;

		public ConfigureTestdocAction(final Supplier<Iterable<File>> classpathConfig, final Supplier<SourceSet> sourceConfig, final RobotTestdocProto testdocProto) {
			this.classpathConfig = classpathConfig;
			this.sourceConfig = sourceConfig;
			this.testdocProto = testdocProto;
		}

		@Override
		public void execute(final RobotTestdocTask task) {
			task.setDescription("Generates test data documentation in HTML");
			task.setRobotProto(testdocProto);
			task.setSourceFiles(sourceConfig.get().getResources().getFiles());
			task.setLibraryClasspath(classpathConfig.get());
		}
	}

	static final class ConfigureTidyAction implements Action<RobotTidyTask> {
		private final Supplier<Iterable<File>> classpathConfig;
		private final Supplier<SourceSet> sourceConfig;
		private final RobotTidyProto tidyProto;

		public ConfigureTidyAction(final Supplier<Iterable<File>> classpathConfig, final Supplier<SourceSet> sourceConfig, final RobotTidyProto tidyProto) {
			this.classpathConfig = classpathConfig;
			this.sourceConfig = sourceConfig;
			this.tidyProto = tidyProto;
		}

		@Override
		public void execute(final RobotTidyTask task) {
			task.setDescription("Cleans up test data files");
			task.setRobotProto(tidyProto);
			task.setSourceFiles(sourceConfig.get().getResources().getFiles());
			task.setLibraryClasspath(classpathConfig.get());
		}
	}

	static final class SourceSetConfiguration implements Supplier<SourceSet> {
		private SourceSet robotSrc;

		public void configureProject(final Project project, final Configuration config) {
			project.getPlugins().withType(JavaPlugin.class, plugin -> {
				final JavaPluginConvention javaConvention = project.getConvention().getPlugin(JavaPluginConvention.class);
				final Set<File> resourceDirs;
				final SourceDirectorySet resources;

				robotSrc = javaConvention.getSourceSets().create("integ");
				robotSrc.setCompileClasspath(config);
				robotSrc.setRuntimeClasspath(config);
				resources = robotSrc.getResources();
				resourceDirs = resources.getSrcDirs();

				for (final File resource : resources.getSrcDirs()) {
					final File robotResources = resource.getParentFile().toPath().resolve("robot").toFile();

					resourceDirs.add(robotResources);
				}

				resources.setSrcDirs(resourceDirs);
			});
		}

		@Override
		public SourceSet get() {
			return robotSrc;
		}
	}

	private final RobotRunnerBridge robotBridge;
	private final SourceSetConfiguration sourceConfig;

	RobotPlugin(final RobotRunnerBridge robotBridge) {
		this.robotBridge = robotBridge;
		sourceConfig = new SourceSetConfiguration();
	}

	@Inject
	public RobotPlugin(final WorkerExecutor workerExecutor) {
		this(new RobotRunnerBridge(workerExecutor));
	}

	@Override
	public void apply(@Nonnull final Project target) {
		final Configuration config = target.getConfigurations().create("robot");
		final ExtensionContainer extensions = target.getExtensions();
		final RobotPluginExtension extension = extensions.create("robot", RobotPluginExtension.class);

		extension.setReportsDir(target.provider(() -> provideReportsDir(target)));
		config.setDescription("The Robot Framework jar to be used for this project.");
		config.setVisible(false);
		config.setTransitive(true);
		configureDependencies(target, config, extension);
		sourceConfig.configureProject(target, config);
		configureTasks(target, config, extension);
	}

	private void configureDependencies(final Project project, final Configuration config, final RobotPluginExtension extension) {
		final String toolVersion = extension.getToolVersion();
		final String version = toolVersion == null ? "3.1.2" : toolVersion;

		config.defaultDependencies((DependencySet dependencies) -> dependencies.add(
			project.getDependencies().create("org.robotframework:robotframework:" + version))
		);
	}

	private void configureTasks(final Project project, final Configuration config, final RobotPluginExtension extension) {
		final TaskContainer tasks = project.getTasks();
		final RobotRunProto runProto = extension.getRunProto();
		final ClasspathConfiguration classpathConfiguration = new ClasspathConfiguration(config, sourceConfig);
		final Action<RobotRunTask> dryRunConfig = new ConfigureDryRunAction(classpathConfiguration, sourceConfig, runProto);
		final Action<RobotRunTask> runConfig = new ConfigureRunAction(classpathConfiguration, sourceConfig, runProto);
		final Action<RobotRebotTask> rebotConfig = new ConfigureRebotAction(classpathConfiguration, extension.getRebotProto());
		final Action<RobotLibdocTask> libdocConfig = new ConfigureLibdocAction(classpathConfiguration, sourceConfig, extension.getLibdocProto());
		final Action<RobotTidyTask> tidyConfig = new ConfigureTidyAction(classpathConfiguration, sourceConfig, extension.getTidyProto());
		final Action<RobotTestdocTask> testdocConfig = new ConfigureTestdocAction(classpathConfiguration, sourceConfig, extension.getTestdocProto());

		tasks.register("robotDryRun", RobotRunTask.class, robotBridge).configure(dryRunConfig);
		tasks.register("robotRun", RobotRunTask.class, robotBridge).configure(runConfig);
		tasks.register("robotRebot", RobotRebotTask.class, robotBridge).configure(rebotConfig);
		tasks.register("robotLibdoc", RobotLibdocTask.class, robotBridge).configure(libdocConfig);
		tasks.register("robotTidy", RobotTidyTask.class, robotBridge).configure(tidyConfig);
		tasks.register("robotTestdoc", RobotTestdocTask.class, robotBridge).configure(testdocConfig);
	}

	private File provideReportsDir(final Project target) {
		final ExtensionContainer extensions = target.getExtensions();
		final ReportingExtension reportingExtension = extensions.findByType(ReportingExtension.class);

		return reportingExtension == null ? target.file("build/reports/robot") : reportingExtension.file("robot");
	}
}
