/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.util.List;
import java.util.Map;

import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.theotherorg.poros.robot.RobotRunnerBridge.BaseBuilder.HasBase;

public abstract class AbstractBaseProto implements HasBase {
	private List<String> argumentFiles;
	private String doc;
	private List<String> excludes;
	private List<String> includes;
	private Map<String, String> metadata;
	private String name;
	private List<String> pythonPaths;
	private List<String> suites;
	private List<String> tags;
	private List<String> tests;

	@Optional
	@Input
	@Override
	public List<String> getArgumentFiles() {
		return argumentFiles;
	}

	public void setArgumentFiles(final List<String> argumentFiles) {
		this.argumentFiles = argumentFiles;
	}

	@Optional
	@Input
	@Override
	public String getDoc() {
		return doc;
	}

	public void setDoc(final String doc) {
		this.doc = doc;
	}

	@Optional
	@Input
	@Override
	public List<String> getExcludes() {
		return excludes;
	}

	public void setExcludes(final List<String> excludes) {
		this.excludes = excludes;
	}

	@Optional
	@Input
	@Override
	public List<String> getIncludes() {
		return includes;
	}

	public void setIncludes(final List<String> includes) {
		this.includes = includes;
	}

	@Optional
	@Input
	@Override
	public Map<String, String> getMetadata() {
		return metadata;
	}

	public void setMetadata(final Map<String, String> metadata) {
		this.metadata = metadata;
	}

	@Optional
	@Input
	@Override
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Optional
	@Input
	@Override
	public List<String> getPythonPaths() {
		return pythonPaths;
	}

	public void setPythonPaths(final List<String> pythonPaths) {
		this.pythonPaths = pythonPaths;
	}

	@Optional
	@Input
	@Override
	public List<String> getSuites() {
		return suites;
	}

	public void setSuites(final List<String> suites) {
		this.suites = suites;
	}

	@Optional
	@Input
	@Override
	public List<String> getTags() {
		return tags;
	}

	public void setTags(final List<String> tags) {
		this.tags = tags;
	}

	@Optional
	@Input
	@Override
	public List<String> getTests() {
		return tests;
	}

	public void setTests(final List<String> tests) {
		this.tests = tests;
	}
}
