/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.io.File;
import java.nio.file.Path;

import javax.inject.Inject;

import org.gradle.api.Action;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;

public class RobotPluginExtension {
	private final RobotLibdocProto libdocProto;
	private final RobotRebotProto rebotProto;
	private final Property<File> reportsDir;
	private final RobotRunProto runProto;
	private final RobotTestdocProto testdocProto;
	private final RobotTidyProto tidyProto;
	private String toolVersion;

	@Inject
	public RobotPluginExtension(final ObjectFactory objectFactory) {
		this.libdocProto = objectFactory.newInstance(RobotLibdocProto.class);
		this.rebotProto = objectFactory.newInstance(RobotRebotProto.class);
		this.reportsDir = objectFactory.property(File.class);
		this.runProto = objectFactory.newInstance(RobotRunProto.class);
		this.testdocProto = objectFactory.newInstance(RobotTestdocProto.class);
		this.tidyProto = objectFactory.newInstance(RobotTidyProto.class);
	}

	public RobotLibdocProto getLibdocProto() {
		if (libdocProto.getOutputDir() == null) {
			libdocProto.setOutputDir(reportsDir.get());
		}

		return libdocProto;
	}

	public RobotRebotProto getRebotProto() {
		if (rebotProto.getOutputDir() == null) {
			rebotProto.setOutputDir(reportsDir.get());
		}

		if (rebotProto.getOutputXml() == null) {
			rebotProto.setOutputXml(reportsDir.get().toPath().resolve("output.xml").toFile());
		}

		return rebotProto;
	}

	public File getReportsDir() {
		return reportsDir.get();
	}

	public void setReportsDir(final File reportsDir) {
		this.reportsDir.set(reportsDir);
	}

	public void setReportsDir(final Provider<File> reportsDirProvider) {
		this.reportsDir.set(reportsDirProvider);
	}

	public RobotRunProto getRunProto() {
		if (runProto.getOutputDir() == null) {
			runProto.setOutputDir(reportsDir.get());
		}

		return runProto;
	}

	public RobotTestdocProto getTestdocProto() {
		if (testdocProto.getOutputFile() == null) {
			final Path defaultOutput = reportsDir.get().toPath().resolve("testdoc.html");

			testdocProto.setOutputFile(defaultOutput.toFile());
		}

		return testdocProto;
	}

	public RobotTidyProto getTidyProto() {
		return tidyProto;
	}

	public String getToolVersion() {
		return toolVersion;
	}

	public void setToolVersion(final String toolVersion) {
		this.toolVersion = toolVersion;
	}

	void common(final Action<? super AbstractBaseProto> action) {
		action.execute(rebotProto);
		action.execute(runProto);
		action.execute(testdocProto);
	}

	void libdoc(final Action<? super RobotLibdocProto> action) {
		action.execute(libdocProto);
	}

	void rebot(final Action<? super RobotRebotProto> action) {
		action.execute(rebotProto);
	}

	void report(final Action<? super AbstractReportingProto> action) {
		action.execute(rebotProto);
		action.execute(runProto);
	}

	void run(final Action<? super RobotRunProto> action) {
		action.execute(runProto);
	}

	void testdoc(final Action<? super RobotTestdocProto> action) {
		action.execute(testdocProto);
	}

	void tidy(final Action<? super RobotTidyProto> action) {
		action.execute(tidyProto);
	}
}
