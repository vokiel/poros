/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.io.File;

import javax.inject.Inject;

import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputFile;

class RobotRebotProto extends AbstractReportingProto {
	private String endTime;
	private File outputXml;
	private boolean processEmptySuite;
	private String startTime;

	@Inject
	public RobotRebotProto() {
	}

	@Optional
	@Input
	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(final String endTime) {
		this.endTime = endTime;
	}

	@Optional
	@OutputFile
	public File getOutputXml() {
		return outputXml;
	}

	public void setOutputXml(final File outputXml) {
		this.outputXml = outputXml;
	}

	@Optional
	@Input
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(final String startTime) {
		this.startTime = startTime;
	}

	@Input
	public boolean isProcessEmptySuite() {
		return processEmptySuite;
	}

	public void setProcessEmptySuite(final boolean processEmptySuite) {
		this.processEmptySuite = processEmptySuite;
	}
}
