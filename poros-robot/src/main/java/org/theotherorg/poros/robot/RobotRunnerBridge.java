/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.gradle.api.Action;
import org.gradle.api.GradleException;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.workers.ForkMode;
import org.gradle.workers.IsolationMode;
import org.gradle.workers.WorkerConfiguration;
import org.gradle.workers.WorkerExecutor;
import org.robotframework.RobotRunner;

public class RobotRunnerBridge {
	private static final Logger log = Logging.getLogger(RobotRunnerBridge.class);

	static abstract class AbstractBuilder {
		private final List<String> options = new ArrayList<>();

		public List<String> getOptions() {
			return options;
		}

		protected void addFlag(final String flag) {
			options.add(flag);
		}

		protected void addMap(final String arg, final Map<String, String> values) {
			for (final Map.Entry<String, String> entry : values.entrySet()) {
				addOption(arg, entry.getKey() + ":" + entry.getValue());
			}
		}

		protected void addOption(final String arg, final String value) {
			options.add(arg);
			options.add(value);
		}

		protected void addOptions(final String arg, final Iterable<String> values) {
			for (final String value : values) {
				addOption(arg, value);
			}
		}
	}

	static class BaseBuilder extends AbstractBuilder {
		interface HasBase {
			List<String> getArgumentFiles();

			String getDoc();

			List<String> getExcludes();

			List<String> getIncludes();

			Map<String, String> getMetadata();

			String getName();

			List<String> getPythonPaths();

			List<String> getSuites();

			List<String> getTags();

			List<String> getTests();
		}

		public void addArgumentFiles(final Iterable<String> files) {
			addOptions("-A", files);
		}

		public void addBase(final HasBase base) {
			final String name = base.getName();
			final String doc = base.getDoc();
			final Map<String, String> metadata = base.getMetadata();
			final List<String> tags = base.getTags();
			final List<String> tests = base.getTests();
			final List<String> suites = base.getSuites();
			final List<String> includes = base.getIncludes();
			final List<String> excludes = base.getExcludes();
			final List<String> pythonPaths = base.getPythonPaths();
			final List<String> argFiles = base.getArgumentFiles();

			if (argFiles != null) {
				addArgumentFiles(argFiles);
			}

			if (name != null) {
				addName(name);
			}

			if (doc != null) {
				addDoc(doc);
			}

			if (metadata != null) {
				addMetadata(metadata);
			}

			if (tags != null) {
				addTags(tags);
			}

			if (tests != null) {
				addTests(tests);
			}

			if (suites != null) {
				addSuites(suites);
			}

			if (includes != null) {
				addIncludes(includes);
			}

			if (excludes != null) {
				addExcludes(excludes);
			}

			if (pythonPaths != null) {
				addPythonPaths(pythonPaths);
			}
		}

		public void addDoc(final String doc) {
			addOption("-D", doc);
		}

		public void addExcludes(final Iterable<String> excludes) {
			addOptions("-e", excludes);
		}

		public void addIncludes(final Iterable<String> includes) {
			addOptions("-i", includes);
		}

		public void addMetadata(final Map<String, String> metadata) {
			addMap("-M", metadata);
		}

		public void addName(final String name) {
			addOption("-N", name);
		}

		public void addPythonPaths(final Iterable<String> pythonPaths) {
			addOptions("-P", pythonPaths);
		}

		public void addSuites(final Iterable<String> suites) {
			addOptions("-s", suites);
		}

		public void addTags(final Iterable<String> tags) {
			addOptions("-G", tags);
		}

		public void addTests(final Iterable<String> tests) {
			addOptions("-t", tests);
		}

		public String[] toArgs() {
			return getOptions().toArray(new String[0]);
		}
	}

	static class IsolationConfig implements Action<WorkerConfiguration> {
		private final String[] args;
		private final Iterable<File> classpath;

		public IsolationConfig(final Iterable<File> classpath, final String... args) {
			this.args = args;
			this.classpath = classpath;
		}

		@Override
		public void execute(@Nonnull final WorkerConfiguration config) {
			final List<String> args = Arrays.asList(this.args);

			RobotRunnerBridge.log.debug(Arrays.toString(this.args));
			config.setForkMode(ForkMode.NEVER);
			config.setIsolationMode(IsolationMode.CLASSLOADER);
			config.setParams(args);
			config.setClasspath(classpath);
		}
	}

	static class IsolationRunnable implements Runnable {
		private final String[] args;
		private final Supplier<RobotRunner> runnerSupplier;

		IsolationRunnable(final Supplier<RobotRunner> runnerSupplier, final String... args) {
			this.args = args;
			this.runnerSupplier = runnerSupplier;
		}

		@Inject
		public IsolationRunnable(final List<String> args) {
			this(RobotRunner::new, args.toArray(new String[0]));
		}

		@Override
		public void run() {
			try (final RobotRunner robotRunner = runnerSupplier.get()) {
				if (args.length != 0) {
					int returnCode = robotRunner.run(args);

					if (returnCode > 0) {
						throw new GradleException("Failed with code " + returnCode);
					}
				}
			}
		}
	}

	static class LibdocBuilder extends BaseBuilder {
		public void addDocFormat(final String docFormat) {
			addOption("-F", docFormat);
		}

		public void addFormat(final String format) {
			addOption("-f", format);
		}

		public void addVersion(final String version) {
			addOption("-v", version);
		}

		public String[] toArgs(final File libraryFile, final File outputFile) {
			final List<String> options = new ArrayList<>();

			options.add("libdoc");
			options.addAll(getOptions());

			if (libraryFile != null) {
				options.add(libraryFile.getPath());

				if (outputFile != null) {
					options.add(outputFile.getPath());
				}
			}

			return options.toArray(new String[0]);
		}

		public static LibdocBuilder from(final RobotLibdocProto proto) {
			final LibdocBuilder libdocBuilder = new LibdocBuilder();
			final String format = proto.getFormat();
			final String docFormat = proto.getDocFormat();
			final String version = proto.getVersion();

			if (format != null) {
				libdocBuilder.addFormat(format);
			}

			if (docFormat != null) {
				libdocBuilder.addDocFormat(docFormat);
			}

			if (version != null) {
				libdocBuilder.addVersion(version);
			}

			return libdocBuilder;
		}
	}

	static class RebotBuilder extends ReportBuilder {
		public void addEndTime(final String endTime) {
			addOption("--endtime", endTime);
		}

		public void addProcessEmptySuite() {
			addFlag("--processemptysuite");
		}

		public void addStartTime(final String startTime) {
			addOption("--starttime", startTime);
		}

		public String[] toArgs(final File output) {
			final List<String> options = new ArrayList<>();

			options.add("rebot");
			options.addAll(getOptions());

			if (output != null) {
				options.add(output.getPath());
			}

			return options.toArray(new String[0]);
		}

		public static RebotBuilder from(final RobotRebotProto proto) {
			final RebotBuilder rebotBuilder = new RebotBuilder();
			final String startTime = proto.getStartTime();
			final String endTime = proto.getEndTime();

			rebotBuilder.addBase(proto);

			if (proto.isProcessEmptySuite()) {
				rebotBuilder.addProcessEmptySuite();
			}

			if (startTime != null) {
				rebotBuilder.addStartTime(startTime);
			}

			if (endTime != null) {
				rebotBuilder.addEndTime(endTime);
			}

			rebotBuilder.addReport(proto);
			return rebotBuilder;
		}
	}

	static class ReportBuilder extends BaseBuilder {
		interface HasReport {
			String getConsoleColors();

			List<String> getCriticals();

			List<String> getFlattenKeywords();

			File getLog();

			String getLogLevel();

			String getLogTitle();

			List<String> getNonCriticals();

			File getOutput();

			File getOutputDir();

			List<String> getPreRebotModifiers();

			List<String> getRemoveKeywords();

			File getReport();

			String getReportBackground();

			String getReportTitle();

			int getSuiteStatLevel();

			List<String> getTagDocs();

			List<String> getTagStatCombines();

			List<String> getTagStatExcludes();

			List<String> getTagStatIncludes();

			List<String> getTagStatLinks();

			List<String> getTasks();

			File getXunit();

			boolean isNoStatusRc();

			boolean isRpa();

			boolean isSplitLog();

			boolean isTimeStampOutputs();

			boolean isXunitSkipNonCritical();
		}

		public void addConsoleColors(final String colors) {
			addOption("-C", colors);
		}

		public void addCriticals(final Iterable<String> criticals) {
			addOptions("-c", criticals);
		}

		public void addFlattenKeywords(final Iterable<String> keywords) {
			addOptions("--flattenkeywords", keywords);
		}

		public void addLog(final File logFile) {
			addOption("-l", logFile.getPath());
		}

		public void addLogLevel(final String logLevel) {
			addOption("-L", logLevel);
		}

		public void addLogTitle(final String logTitle) {
			addOption("--logtitle", logTitle);
		}

		public void addNoStatusRc() {
			addFlag("--nostatusrc");
		}

		public void addNonCriticals(final Iterable<String> nonCriticals) {
			addOptions("-n", nonCriticals);
		}

		public void addOutput(final File outputFile) {
			addOption("-o", outputFile.getPath());
		}

		public void addOutputDir(final File outputDir) {
			addOption("-d", outputDir.getPath());
		}

		public void addPreRebotModifiers(final Iterable<String> modifiers) {
			addOptions("--prerebotmodifier", modifiers);
		}

		public void addRemoveKeywords(final Iterable<String> keywords) {
			addOptions("--removekeywords", keywords);
		}

		public void addReport(final File reportFile) {
			addOption("-r", reportFile.getPath());
		}

		public void addReport(final HasReport report) {
			final List<String> tasks = report.getTasks();
			final List<String> criticals = report.getCriticals();
			final List<String> nonCriticals = report.getNonCriticals();
			final File output = report.getOutput();
			final File outputDir = report.getOutputDir();
			final File logFile = report.getLog();
			final String logLevel = report.getLogLevel();
			final String logTitle = report.getLogTitle();
			final File reportFile = report.getReport();
			final String reportTitle = report.getReportTitle();
			final File xunitFile = report.getXunit();
			final String reportBackground = report.getReportBackground();
			final int suiteStatLevel = report.getSuiteStatLevel();
			final List<String> tagDocs = report.getTagDocs();
			final List<String> tagStatCombines = report.getTagStatCombines();
			final List<String> tagStatExcludes = report.getTagStatExcludes();
			final List<String> tagStatIncludes = report.getTagStatIncludes();
			final List<String> tagStatLinks = report.getTagStatLinks();
			final List<String> removeKeywords = report.getRemoveKeywords();
			final List<String> flattenKeywords = report.getFlattenKeywords();
			final List<String> preRebotModifiers = report.getPreRebotModifiers();
			final String consoleColors = report.getConsoleColors();

			if (report.isRpa()) {
				addRpa();
			}

			if (tasks != null) {
				addTasks(tasks);
			}

			if (criticals != null) {
				addCriticals(criticals);
			}

			if (nonCriticals != null) {
				addNonCriticals(nonCriticals);
			}

			if (output != null) {
				addOutput(output);
			}

			if (outputDir != null) {
				addOutputDir(outputDir);
			}

			if (logFile != null) {
				addLog(logFile);
			}

			if (logLevel != null) {
				addLogLevel(logLevel);
			}

			if (logTitle != null) {
				addLogTitle(logTitle);
			}

			if (reportFile != null) {
				addReport(reportFile);
			}

			if (reportTitle != null) {
				addReportTitle(reportTitle);
			}

			if (reportBackground != null) {
				addReportBackground(reportBackground);
			}

			if (report.isSplitLog()) {
				addSplitLog();
			}

			if (report.isTimeStampOutputs()) {
				addTimeStampOutputs();
			}

			if (xunitFile != null) {
				addXunit(xunitFile);
			}

			if (report.isXunitSkipNonCritical()) {
				addXunitSkipNonCritical();
			}

			if (suiteStatLevel > 0) {
				addSuiteStatLevel(suiteStatLevel);
			}

			if (tagDocs != null) {
				addTagDocs(tagDocs);
			}

			if (tagStatCombines != null) {
				addTagStatCombines(tagStatCombines);
			}

			if (tagStatExcludes != null) {
				addTagStatExcludes(tagStatExcludes);
			}

			if (tagStatIncludes != null) {
				addTagStatIncludes(tagStatIncludes);
			}

			if (tagStatLinks != null) {
				addTagStatLinks(tagStatLinks);
			}

			if (removeKeywords != null) {
				addRemoveKeywords(removeKeywords);
			}

			if (flattenKeywords != null) {
				addFlattenKeywords(flattenKeywords);
			}

			if (report.isNoStatusRc()) {
				addNoStatusRc();
			}

			if (preRebotModifiers != null) {
				addPreRebotModifiers(preRebotModifiers);
			}

			if (consoleColors != null) {
				addConsoleColors(consoleColors);
			}
		}

		public void addReportBackground(final String reportBackground) {
			addOption("--reportbackground", reportBackground);
		}

		public void addReportTitle(final String reportTitle) {
			addOption("--reporttitle", reportTitle);
		}

		public void addRpa() {
			addFlag("--rpa");
		}

		public void addSplitLog() {
			addFlag("--splitlog");
		}

		public void addSuiteStatLevel(final int level) {
			addOption("--suitestatlevel", String.valueOf(level));
		}

		public void addTagDocs(final Iterable<String> tagDocs) {
			addOptions("--tagdoc", tagDocs);
		}

		public void addTagStatCombines(final Iterable<String> combines) {
			addOptions("--tagstatcombine", combines);
		}

		public void addTagStatExcludes(final Iterable<String> excludes) {
			addOptions("--tagstatexclude", excludes);
		}

		public void addTagStatIncludes(final Iterable<String> includes) {
			addOptions("--tagstatinclude", includes);
		}

		public void addTagStatLinks(final Iterable<String> links) {
			addOptions("--tagstatlink", links);
		}

		public void addTasks(final Iterable<String> tasks) {
			addTests(tasks);
		}

		public void addTimeStampOutputs() {
			addFlag("-T");
		}

		public void addXunit(final File xunitFile) {
			addOption("-x", xunitFile.getPath());
		}

		public void addXunitSkipNonCritical() {
			addFlag("--xunitskipnoncritical");
		}
	}

	static class RunBuilder extends ReportBuilder {
		public void addConsoleMarkers(final String markers) {
			addOption("-K", markers);
		}

		public void addConsoleType(final String type) {
			addOption("--console", type);
		}

		public void addConsoleWidth(final int consoleWidth) {
			addOption("-W", String.valueOf(consoleWidth));
		}

		public void addDebugFile(final File debugFile) {
			addOption("-b", debugFile.getPath());
		}

		public void addDryRun() {
			addFlag("--dryrun");
		}

		public void addExitOnError() {
			addFlag("--exitonerror");
		}

		public void addExitOnFailures() {
			addFlag("--exitonfailure");
		}

		public void addExtensions(final List<String> extensions) {
			if (!extensions.isEmpty()) {
				addOption("-F", String.join(":", extensions));
			}
		}

		public void addListeners(final Iterable<String> listeners) {
			addOptions("--listener", listeners);
		}

		public void addMaxErrorLines(final int maximum) {
			addOption("--maxerrorlines", String.valueOf(maximum));
		}

		public void addPreRunModifiers(final Iterable<String> preRunModifiers) {
			addOptions("--prerunmodifier", preRunModifiers);
		}

		public void addRandomize(final String randomize) {
			addOption("--randomize", randomize);
		}

		public void addRerunFailed(final File file) {
			addOption("-R", file.getAbsolutePath());
		}

		public void addRerunFailedSuites(final File file) {
			addOption("-S", file.getAbsolutePath());
		}

		public void addRunEmptySuite() {
			addFlag("--runemptysuite");
		}

		public void addSkipTearDownOnExit() {
			addFlag("--skipteardownonexit");
		}

		public void addVariableFiles(final Iterable<String> variableFiles) {
			addOptions("-V", variableFiles);
		}

		public void addVariables(final Map<String, String> variables) {
			addMap("-v", variables);
		}

		public String[] toArgs(final Iterable<File> dataFiles) {
			final List<String> options = new ArrayList<>();

			options.add("run");
			options.addAll(getOptions());

			if (dataFiles != null) {
				for (final File file : dataFiles) {
					if (file.toPath().getFileName().toString().endsWith(".robot")) {
						options.add(file.getPath());
					}
				}
			}

			return options.toArray(new String[0]);
		}

		public static RunBuilder from(final RobotRunProto proto) {
			final RunBuilder runBuilder = new RunBuilder();
			final List<String> extensions = proto.getExtensions();
			final File rerunFailed = proto.getRerunFailed();
			final File rerunFailedSuites = proto.getRerunFailedSuites();
			final Map<String, String> variables = proto.getVariables();
			final List<String> variableFiles = proto.getVariableFiles();
			final File debugFile = proto.getDebugFile();
			final int maxErrorLines = proto.getMaxErrorLines();
			final List<String> listeners = proto.getListeners();
			final String randomize = proto.getRandomize();
			final List<String> preRunModifiers = proto.getPreRunModifiers();
			final String consoleMarkers = proto.getConsoleMarkers();
			final String consoleType = proto.getConsoleType();
			final int consoleWidth = proto.getConsoleWidth();

			runBuilder.addBase(proto);

			if (extensions != null) {
				runBuilder.addExtensions(extensions);
			}

			if (rerunFailed != null) {
				runBuilder.addRerunFailed(rerunFailed);
			}

			if (rerunFailedSuites != null) {
				runBuilder.addRerunFailedSuites(rerunFailedSuites);
			}

			if (variables != null) {
				runBuilder.addVariables(variables);
			}

			if (variableFiles != null) {
				runBuilder.addVariableFiles(variableFiles);
			}

			if (debugFile != null) {
				runBuilder.addDebugFile(debugFile);
			}

			if (maxErrorLines > 0) {
				runBuilder.addMaxErrorLines(maxErrorLines);
			}

			if (listeners != null) {
				runBuilder.addListeners(listeners);
			}

			if (proto.isRunEmptySuite()) {
				runBuilder.addRunEmptySuite();
			}

			if (proto.isDryRun()) {
				runBuilder.addDryRun();
			}

			if (proto.isExitOnFailure()) {
				runBuilder.addExitOnFailures();
			}

			if (proto.isExitOnError()) {
				runBuilder.addExitOnError();
			}

			if (proto.isSkipTearDownOnExit()) {
				runBuilder.addSkipTearDownOnExit();
			}

			if (randomize != null) {
				runBuilder.addRandomize(randomize);
			}

			if (preRunModifiers != null) {
				runBuilder.addPreRunModifiers(preRunModifiers);
			}

			if (consoleMarkers != null) {
				runBuilder.addConsoleMarkers(consoleMarkers);
			}

			if (consoleType != null) {
				runBuilder.addConsoleType(consoleType);
			}

			if (consoleWidth > 0) {
				runBuilder.addConsoleWidth(consoleWidth);
			}

			runBuilder.addReport(proto);
			return runBuilder;
		}
	}

	static class TestDocBuilder extends BaseBuilder {
		public void addTitle(final String title) {
			addOption("-T", title);
		}

		public String[] toArgs(final Iterable<File> dataFiles, final File outputFile) {
			final List<String> options = new ArrayList<>();

			options.add("testdoc");
			options.addAll(getOptions());

			if (dataFiles != null) {
				for (final File file : dataFiles) {
					options.add(file.getPath());
				}

				if (outputFile != null) {
					options.add(outputFile.getPath());
				}
			}

			return options.toArray(new String[0]);
		}

		public static TestDocBuilder from(final RobotTestdocProto proto) {
			final TestDocBuilder testBuilder = new TestDocBuilder();
			final String title = proto.getTitle();

			testBuilder.addBase(proto);

			if (title != null) {
				testBuilder.addTitle(title);
			}

			return testBuilder;
		}
	}

	static class TidyBuilder extends AbstractBuilder {
		public void addFormat(final String format) {
			addOption("-f", format);
		}

		public void addInPlace() {
			addFlag("-i");
		}

		public void addLineSeparator(final String lineSeparator) {
			addOption("-l", lineSeparator);
		}

		public void addRecursive() {
			addFlag("-r");
		}

		public void addSpaceCounts(final int count) {
			addOption("-s", String.valueOf(count));
		}

		public void addUsePipes() {
			addFlag("-p");
		}

		public String[] toArgs(final Iterable<File> dataFiles) {
			final List<String> options = new ArrayList<>();

			options.add("tidy");
			options.addAll(getOptions());

			if (dataFiles != null) {
				for (final File file : dataFiles) {
					options.add(file.getPath());
				}
			}

			return options.toArray(new String[0]);
		}

		public static TidyBuilder from(final RobotTidyProto proto, final boolean inPlace) {
			final TidyBuilder tidyBuilder = new TidyBuilder();
			final String format = proto.getFormat();
			final String lineSeparator = proto.getLineSeparator();
			final int spaceCount = proto.getSpaceCount();

			if (format != null) {
				tidyBuilder.addFormat(format);
			}

			if (lineSeparator != null) {
				tidyBuilder.addLineSeparator(lineSeparator);
			}

			if (spaceCount > 0) {
				tidyBuilder.addSpaceCounts(spaceCount);
			}

			if (proto.isUsePipes()) {
				tidyBuilder.addUsePipes();
			}

			if (inPlace) {
				tidyBuilder.addInPlace();
			}

			return tidyBuilder;
		}
	}

	private final WorkerExecutor workerExecutor;

	@Inject
	public RobotRunnerBridge(final WorkerExecutor workerExecutor) {
		this.workerExecutor = workerExecutor;
	}

	public void libDoc(final RobotLibdocProto libdocProto, final Iterable<File> libraryFiles, final Iterable<File> classpath) {
		final File outputDir = libdocProto.getOutputDir();

		if (outputDir != null && libraryFiles != null) {
			final String format = libdocProto.getFormat();
			final LibdocBuilder builder = LibdocBuilder.from(libdocProto);
			final Path outputPath = outputDir.toPath();

			for (final File library : libraryFiles) {
				final String filename = library.toPath().getFileName().toString();
				final int lastDot = filename.lastIndexOf(".");
				final String outFileName = (lastDot < 0 ? filename : filename.substring(0, lastDot))
					+ ("xml".equalsIgnoreCase(format) ? ".xml" : ".html");
				final File outputFile = outputPath.resolve(outFileName).toFile();
				final String[] args = builder.toArgs(library, outputFile);

				workerExecutor.submit(IsolationRunnable.class, new IsolationConfig(classpath, args));
			}
		}
	}

	public void rebot(final RobotRebotProto rebotProto, final Iterable<File> classpath) {
		final File output = rebotProto.getOutputXml();
		final String[] args = RebotBuilder.from(rebotProto).toArgs(output);

		workerExecutor.submit(IsolationRunnable.class, new IsolationConfig(classpath, args));
	}

	public void run(final RobotRunProto runProto, final Iterable<File> dataFiles, final Iterable<File> classpath) {
		final String[] args = RunBuilder.from(runProto).toArgs(dataFiles);

		workerExecutor.submit(IsolationRunnable.class, new IsolationConfig(classpath, args));
	}

	public void testDoc(final RobotTestdocProto testdocProto, final Iterable<File> dataFiles, final Iterable<File> classpath) {
		final String[] args = TestDocBuilder.from(testdocProto).toArgs(dataFiles, testdocProto.getOutputFile());

		workerExecutor.submit(IsolationRunnable.class, new IsolationConfig(classpath, args));
	}

	public void tidy(final RobotTidyProto tidyProto, final Iterable<File> dataFiles, final Iterable<File> classpath) {
		final String format = tidyProto.getFormat();

		if (format == null) {
			final String[] args = TidyBuilder.from(tidyProto, true).toArgs(dataFiles);

			workerExecutor.submit(IsolationRunnable.class, new IsolationConfig(classpath, args));
		} else if (dataFiles != null) {
			for (final File dataFile : dataFiles) {
				final Path dataPath = dataFile.toPath();
				final String dataFilename = dataPath.getFileName().toString();
				final int extLocation = dataFilename.lastIndexOf('.');

				if (extLocation > 0 && extLocation + 1 < dataFilename.length()) {
					final String siblingType = dataFilename.substring(extLocation + 1);

					if (format.equalsIgnoreCase(siblingType)) {
						final Iterable<File> inPlaced = Collections.singleton(dataFile);
						final String[] args = TidyBuilder.from(tidyProto, true).toArgs(inPlaced);

						workerExecutor.submit(IsolationRunnable.class, new IsolationConfig(classpath, args));
					} else {
						final String siblingName = dataFilename.substring(0, extLocation);
						final Path sibling = dataPath.resolveSibling(siblingName + '.' + format);
						final Iterable<File> fileArgs = Arrays.asList(dataFile, sibling.toFile());
						final String[] args = TidyBuilder.from(tidyProto, false).toArgs(fileArgs);

						workerExecutor.submit(IsolationRunnable.class, new IsolationConfig(classpath, args));
					}
				} else {
					final Path sibling = dataPath.resolveSibling(dataFilename + '.' + format);
					final Iterable<File> fileArgs = Arrays.asList(dataFile, sibling.toFile());
					final String[] args = TidyBuilder.from(tidyProto, false).toArgs(fileArgs);

					workerExecutor.submit(IsolationRunnable.class, new IsolationConfig(classpath, args));
				}
			}
		}
	}
}
