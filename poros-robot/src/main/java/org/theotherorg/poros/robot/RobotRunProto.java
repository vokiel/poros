/*
 * Copyright 2019 Guillaume Pelletier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.theotherorg.poros.robot;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputFile;

class RobotRunProto extends AbstractReportingProto {
	private String consoleMarkers;
	private String consoleType;
	private int consoleWidth;
	private File debugFile;
	private boolean dryRun;
	private boolean exitOnError;
	private boolean exitOnFailure;
	private List<String> extensions;
	private List<String> listeners;
	private int maxErrorLines;
	private List<String> preRunModifiers;
	private String randomize;
	private File rerunFailed;
	private File rerunFailedSuites;
	private boolean runEmptySuite;
	private boolean skipTearDownOnExit;
	private List<String> variableFiles;
	private Map<String, String> variables;

	@Inject
	public RobotRunProto() {
	}

	@Optional
	@Input
	public String getConsoleMarkers() {
		return consoleMarkers;
	}

	public void setConsoleMarkers(final String consoleMarkers) {
		this.consoleMarkers = consoleMarkers;
	}

	@Optional
	@Input
	public String getConsoleType() {
		return consoleType;
	}

	public void setConsoleType(final String consoleType) {
		this.consoleType = consoleType;
	}

	@Optional
	@Input
	public int getConsoleWidth() {
		return consoleWidth;
	}

	public void setConsoleWidth(final int consoleWidth) {
		this.consoleWidth = consoleWidth;
	}

	@Optional
	@OutputFile
	public File getDebugFile() {
		return debugFile;
	}

	public void setDebugFile(final File debugFile) {
		this.debugFile = debugFile;
	}

	@Optional
	@Input
	public List<String> getExtensions() {
		return extensions;
	}

	public void setExtensions(final List<String> extensions) {
		this.extensions = extensions;
	}

	@Optional
	@Input
	public List<String> getListeners() {
		return listeners;
	}

	public void setListeners(final List<String> listeners) {
		this.listeners = listeners;
	}

	@Input
	public int getMaxErrorLines() {
		return maxErrorLines;
	}

	public void setMaxErrorLines(final int maxErrorLines) {
		this.maxErrorLines = maxErrorLines;
	}

	@Optional
	@Input
	public List<String> getPreRunModifiers() {
		return preRunModifiers;
	}

	public void setPreRunModifiers(final List<String> preRunModifiers) {
		this.preRunModifiers = preRunModifiers;
	}

	@Optional
	@Input
	public String getRandomize() {
		return randomize;
	}

	public void setRandomize(final String randomize) {
		this.randomize = randomize;
	}

	@Optional
	@InputFile
	public File getRerunFailed() {
		return rerunFailed;
	}

	public void setRerunFailed(final File rerunFailed) {
		this.rerunFailed = rerunFailed;
	}

	@Optional
	@InputFile
	public File getRerunFailedSuites() {
		return rerunFailedSuites;
	}

	public void setRerunFailedSuites(final File rerunFailedSuites) {
		this.rerunFailedSuites = rerunFailedSuites;
	}

	@Optional
	@Input
	public List<String> getVariableFiles() {
		return variableFiles;
	}

	public void setVariableFiles(final List<String> variableFiles) {
		this.variableFiles = variableFiles;
	}

	@Optional
	@Input
	public Map<String, String> getVariables() {
		return variables;
	}

	public void setVariables(final Map<String, String> variables) {
		this.variables = variables;
	}

	@Internal
	public boolean isDryRun() {
		return dryRun;
	}

	public void setDryRun(final boolean dryRun) {
		this.dryRun = dryRun;
	}

	@Input
	public boolean isExitOnError() {
		return exitOnError;
	}

	public void setExitOnError(final boolean exitOnError) {
		this.exitOnError = exitOnError;
	}

	@Input
	public boolean isExitOnFailure() {
		return exitOnFailure;
	}

	public void setExitOnFailure(final boolean exitOnFailure) {
		this.exitOnFailure = exitOnFailure;
	}

	@Input
	public boolean isRunEmptySuite() {
		return runEmptySuite;
	}

	public void setRunEmptySuite(final boolean runEmptySuite) {
		this.runEmptySuite = runEmptySuite;
	}

	@Input
	public boolean isSkipTearDownOnExit() {
		return skipTearDownOnExit;
	}

	public void setSkipTearDownOnExit(final boolean skipTearDownOnExit) {
		this.skipTearDownOnExit = skipTearDownOnExit;
	}
}
