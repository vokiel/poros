package org.theotherorg.poros.robot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotTidyProtoTest {
	private RobotTidyProto testTidy;

	@BeforeEach
	void setUp() {
		testTidy = new RobotTidyProto();
	}

	@Test
	void testGetFormat() {
		Assertions.assertNull(testTidy.getFormat());
	}

	@Test
	void testGetLineSeparator() {
		Assertions.assertNull(testTidy.getLineSeparator());
	}

	@Test
	void testGetSpaceCount() {
		Assertions.assertEquals(0, testTidy.getSpaceCount());
	}

	@Test
	void testIsUsePipes() {
		Assertions.assertFalse(testTidy.isUsePipes());
	}

	@Test
	void testSetFormat() {
		final String expectedFormat = "format";

		testTidy.setFormat(expectedFormat);
		Assertions.assertEquals(expectedFormat, testTidy.getFormat());
	}

	@Test
	void testSetLineSeparator() {
		final String expectedSeparator = "separator";

		testTidy.setLineSeparator(expectedSeparator);
		Assertions.assertEquals(expectedSeparator, testTidy.getLineSeparator());
	}

	@Test
	void testSetSpaceCounts() {
		final int expectedCount = 37;

		testTidy.setSpaceCount(expectedCount);
		Assertions.assertEquals(expectedCount, testTidy.getSpaceCount());
	}

	@Test
	void testSetUsePipes() {
		testTidy.setUsePipes(true);
		Assertions.assertTrue(testTidy.isUsePipes());
	}
}
