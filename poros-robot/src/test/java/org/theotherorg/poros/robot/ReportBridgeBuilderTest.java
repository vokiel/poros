package org.theotherorg.poros.robot;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.ReportBuilder;
import org.theotherorg.poros.robot.RobotRunnerBridge.ReportBuilder.HasReport;

public class ReportBridgeBuilderTest {
	private ReportBuilder testBuilder;

	@BeforeEach
	void setUp() {
		testBuilder = new ReportBuilder();
	}

	@Test
	void testAddConsoleColors() {
		final String expectedColors = "colors";
		final List<String> testOptions;

		testBuilder.addConsoleColors(expectedColors);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertEquals(expectedColors, testOptions.get(1));
	}

	@Test
	void testAddCriticals() {
		final List<String> expectedCriticals = new ArrayList<>();
		final String expectedCritical = "critical";
		final List<String> testOptions;

		expectedCriticals.add(expectedCritical);
		testBuilder.addCriticals(expectedCriticals);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedCritical, testOptions.get(1));
	}

	@Test
	void testAddFlattenKeywords() {
		final List<String> expectedKeywords = new ArrayList<>();
		final String expectedKeyword = "keyword";
		final List<String> testOptions;

		expectedKeywords.add(expectedKeyword);
		testBuilder.addFlattenKeywords(expectedKeywords);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedKeyword, testOptions.get(1));
	}

	@Test
	void testAddHasReport() {
		final HasReport mockedReport = EasyMock.createStrictMock(HasReport.class);
		final List<String> expectedTasks = Collections.singletonList("task");
		final List<String> expectedCriticals = Collections.singletonList("critical");
		final List<String> expectedNonCriticals = Collections.singletonList("nonCritical");
		final File mockedOutput = EasyMock.createStrictMock(File.class);
		final String expectedOutputPath = "outputPath";
		final File mockedOutputDir = EasyMock.createStrictMock(File.class);
		final String expectedOutputDirPath = "outputDir";
		final File mockedLog = EasyMock.createStrictMock(File.class);
		final String expectedLogPath = "logPath";
		final String expectedLogLevel = "level";
		final String expectedLogTitle = "title";
		final File mockedReportFile = EasyMock.createStrictMock(File.class);
		final String expectedReportPath = "reportPath";
		final String expectedReportTitle = "reportTitle";
		final File mockedXunit = EasyMock.createStrictMock(File.class);
		final String expectedXunitPath = "xunitPath";
		final String expectedReportBackground = "background";
		final int expectedStatLevel = 37;
		final List<String> expectedTagDocs = Collections.singletonList("doc");
		final List<String> expectedCombines = Collections.singletonList("combine");
		final List<String> expectedExcludes = Collections.singletonList("exclude");
		final List<String> expectedIncludes = Collections.singletonList("include");
		final List<String> expectedLinks = Collections.singletonList("link");
		final List<String> expectedRemoveKeywords = Collections.singletonList("removeKeyword");
		final List<String> expectedFlattenKeywords = Collections.singletonList("flattenKeyword");
		final List<String> expectedModifiers = Collections.singletonList("modifier");
		final String expectedColors = "colors";
		final Set<String> testOptions;

		EasyMock.expect(mockedReport.getTasks()).andReturn(expectedTasks).once();
		EasyMock.expect(mockedReport.getCriticals()).andReturn(expectedCriticals).once();
		EasyMock.expect(mockedReport.getNonCriticals()).andReturn(expectedNonCriticals).once();
		EasyMock.expect(mockedReport.getOutput()).andReturn(mockedOutput).once();
		EasyMock.expect(mockedReport.getOutputDir()).andReturn(mockedOutputDir).once();
		EasyMock.expect(mockedReport.getLog()).andReturn(mockedLog).once();
		EasyMock.expect(mockedReport.getLogLevel()).andReturn(expectedLogLevel).once();
		EasyMock.expect(mockedReport.getLogTitle()).andReturn(expectedLogTitle).once();
		EasyMock.expect(mockedReport.getReport()).andReturn(mockedReportFile).once();
		EasyMock.expect(mockedReport.getReportTitle()).andReturn(expectedReportTitle).once();
		EasyMock.expect(mockedReport.getXunit()).andReturn(mockedXunit).once();
		EasyMock.expect(mockedReport.getReportBackground()).andReturn(expectedReportBackground).once();
		EasyMock.expect(mockedReport.getSuiteStatLevel()).andReturn(expectedStatLevel).once();
		EasyMock.expect(mockedReport.getTagDocs()).andReturn(expectedTagDocs).once();
		EasyMock.expect(mockedReport.getTagStatCombines()).andReturn(expectedCombines).once();
		EasyMock.expect(mockedReport.getTagStatExcludes()).andReturn(expectedExcludes).once();
		EasyMock.expect(mockedReport.getTagStatIncludes()).andReturn(expectedIncludes).once();
		EasyMock.expect(mockedReport.getTagStatLinks()).andReturn(expectedLinks).once();
		EasyMock.expect(mockedReport.getRemoveKeywords()).andReturn(expectedRemoveKeywords).once();
		EasyMock.expect(mockedReport.getFlattenKeywords()).andReturn(expectedFlattenKeywords).once();
		EasyMock.expect(mockedReport.getPreRebotModifiers()).andReturn(expectedModifiers).once();
		EasyMock.expect(mockedReport.getConsoleColors()).andReturn(expectedColors).once();
		EasyMock.expect(mockedReport.isRpa()).andReturn(true).once();
		EasyMock.expect(mockedOutput.getPath()).andReturn(expectedOutputPath).once();
		EasyMock.expect(mockedOutputDir.getPath()).andReturn(expectedOutputDirPath).once();
		EasyMock.expect(mockedLog.getPath()).andReturn(expectedLogPath).once();
		EasyMock.expect(mockedReportFile.getPath()).andReturn(expectedReportPath).once();
		EasyMock.expect(mockedReport.isSplitLog()).andReturn(true).once();
		EasyMock.expect(mockedReport.isTimeStampOutputs()).andReturn(true).once();
		EasyMock.expect(mockedXunit.getPath()).andReturn(expectedXunitPath).once();
		EasyMock.expect(mockedReport.isXunitSkipNonCritical()).andReturn(true).once();
		EasyMock.expect(mockedReport.isNoStatusRc()).andReturn(true).once();
		EasyMock.replay(mockedReport, mockedOutput, mockedOutputDir, mockedLog, mockedReportFile, mockedXunit);
		testBuilder.addReport(mockedReport);
		testOptions = new HashSet<>(testBuilder.getOptions());
		Assertions.assertNotNull(testOptions);
		Assertions.assertTrue(testOptions.contains(expectedTasks.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedCriticals.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedNonCriticals.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedOutputPath));
		Assertions.assertTrue(testOptions.contains(expectedOutputDirPath));
		Assertions.assertTrue(testOptions.contains(expectedLogPath));
		Assertions.assertTrue(testOptions.contains(expectedLogLevel));
		Assertions.assertTrue(testOptions.contains(expectedLogTitle));
		Assertions.assertTrue(testOptions.contains(expectedReportPath));
		Assertions.assertTrue(testOptions.contains(expectedReportTitle));
		Assertions.assertTrue(testOptions.contains(expectedXunitPath));
		Assertions.assertTrue(testOptions.contains(expectedReportBackground));
		Assertions.assertTrue(testOptions.contains(String.valueOf(expectedStatLevel)));
		Assertions.assertTrue(testOptions.contains(expectedTagDocs.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedCombines.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedExcludes.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedIncludes.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedLinks.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedRemoveKeywords.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedFlattenKeywords.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedModifiers.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedColors));
		Assertions.assertTrue(testOptions.contains("--rpa"));
		Assertions.assertTrue(testOptions.contains("--splitlog"));
		Assertions.assertTrue(testOptions.contains("-T"));
		Assertions.assertTrue(testOptions.contains("--xunitskipnoncritical"));
		Assertions.assertTrue(testOptions.contains("--nostatusrc"));
		EasyMock.verify(mockedReport, mockedOutput, mockedOutputDir, mockedLog, mockedReportFile, mockedXunit);
	}

	@Test
	void testAddLog() {
		final File mockedLog = EasyMock.createStrictMock(File.class);
		final String expectedPath = "path";
		final List<String> testOptions;

		EasyMock.expect(mockedLog.getPath()).andReturn(expectedPath).once();
		EasyMock.replay(mockedLog);
		testBuilder.addLog(mockedLog);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedPath, testOptions.get(1));
		EasyMock.verify(mockedLog);
	}

	@Test
	void testAddLogLevel() {
		final String expectedLevel = "level";
		final List<String> testOptions;

		testBuilder.addLogLevel(expectedLevel);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertEquals(expectedLevel, testOptions.get(1));
	}

	@Test
	void testAddLogTitle() {
		final String expectedTitle = "title";
		final List<String> testOptions;

		testBuilder.addLogTitle(expectedTitle);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertEquals(expectedTitle, testOptions.get(1));
	}

	@Test
	void testAddNoStatusRc() {
		final List<String> testOptions;

		testBuilder.addNoStatusRc();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--nostatusrc", testOptions.get(0));
	}

	@Test
	void testAddNonCriticals() {
		final List<String> expectedNonCriticals = new ArrayList<>();
		final String expectedNonCritical = "nonCritical";
		final List<String> testOptions;

		expectedNonCriticals.add(expectedNonCritical);
		testBuilder.addNonCriticals(expectedNonCriticals);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedNonCritical, testOptions.get(1));
	}

	@Test
	void testAddOutput() {
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final String expectedPath = "path";
		final List<String> testOptions;

		EasyMock.expect(mockedFile.getPath()).andReturn(expectedPath).once();
		EasyMock.replay(mockedFile);
		testBuilder.addOutput(mockedFile);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedPath, testOptions.get(1));
		EasyMock.verify(mockedFile);
	}

	@Test
	void testAddOutputDir() {
		final File mockedDir = EasyMock.createStrictMock(File.class);
		final String expectedPath = "path";
		final List<String> testOptions;

		EasyMock.expect(mockedDir.getPath()).andReturn(expectedPath).once();
		EasyMock.replay(mockedDir);
		testBuilder.addOutputDir(mockedDir);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedPath, testOptions.get(1));
		EasyMock.verify(mockedDir);
	}

	@Test
	void testAddPreRebotModifiers() {
		final List<String> expectedModifiers = new ArrayList<>();
		final String expectedModifier = "modifier";
		final List<String> testOptions;

		expectedModifiers.add(expectedModifier);
		testBuilder.addPreRebotModifiers(expectedModifiers);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedModifier, testOptions.get(1));
	}

	@Test
	void testAddRemoveKeywords() {
		final List<String> expectedKeywords = new ArrayList<>();
		final String expectedKeyword = "keyword";
		final List<String> testOptions;

		expectedKeywords.add(expectedKeyword);
		testBuilder.addRemoveKeywords(expectedKeywords);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedKeyword, testOptions.get(1));
	}

	@Test
	void testAddReport() {
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final String expectedPath = "path";
		final List<String> testOptions;

		EasyMock.expect(mockedFile.getPath()).andReturn(expectedPath).once();
		EasyMock.replay(mockedFile);
		testBuilder.addReport(mockedFile);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedPath, testOptions.get(1));
		EasyMock.verify(mockedFile);
	}

	@Test
	void testAddReportBackground() {
		final String expectedBackground = "background";
		final List<String> testOptions;

		testBuilder.addReportBackground(expectedBackground);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertEquals(expectedBackground, testOptions.get(1));
	}

	@Test
	void testAddReportTitle() {
		final String expectedTitle = "background";
		final List<String> testOptions;

		testBuilder.addReportTitle(expectedTitle);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertEquals(expectedTitle, testOptions.get(1));
	}

	@Test
	void testAddRpa() {
		final List<String> testOptions;

		testBuilder.addRpa();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--rpa", testOptions.get(0));
	}

	@Test
	void testAddSplitLog() {
		final List<String> testOptions;

		testBuilder.addSplitLog();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--splitlog", testOptions.get(0));
	}

	@Test
	void testAddSuiteStatLevel() {
		final int expectedLevel = 37;
		final List<String> testOptions;

		testBuilder.addSuiteStatLevel(expectedLevel);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertEquals(String.valueOf(expectedLevel), testOptions.get(1));
	}

	@Test
	void testAddTagDocs() {
		final List<String> expectedDocs = new ArrayList<>();
		final String expectedDoc = "doc";
		final List<String> testOptions;

		expectedDocs.add(expectedDoc);
		testBuilder.addTagDocs(expectedDocs);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedDoc, testOptions.get(1));
	}

	@Test
	void testAddTagStatCombines() {
		final List<String> expectedCombines = new ArrayList<>();
		final String expectedCombine = "combine";
		final List<String> testOptions;

		expectedCombines.add(expectedCombine);
		testBuilder.addTagStatCombines(expectedCombines);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedCombine, testOptions.get(1));
	}

	@Test
	void testAddTagStatExcludes() {
		final List<String> expectedExcludes = new ArrayList<>();
		final String expectedExclude = "exclude";
		final List<String> testOptions;

		expectedExcludes.add(expectedExclude);
		testBuilder.addTagStatExcludes(expectedExcludes);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedExclude, testOptions.get(1));
	}

	@Test
	void testAddTagStatIncludes() {
		final List<String> expectedIncludes = new ArrayList<>();
		final String expectedInclude = "include";
		final List<String> testOptions;

		expectedIncludes.add(expectedInclude);
		testBuilder.addTagStatIncludes(expectedIncludes);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedInclude, testOptions.get(1));
	}

	@Test
	void testAddTagStatLinks() {
		final List<String> expectedLinks = new ArrayList<>();
		final String expectedLink = "link";
		final List<String> testOptions;

		expectedLinks.add(expectedLink);
		testBuilder.addTagStatLinks(expectedLinks);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedLink, testOptions.get(1));
	}

	@Test
	void testAddTasks() {
		final List<String> expectedTasks = new ArrayList<>();
		final String expectedTask = "task";
		final List<String> testOptions;

		expectedTasks.add(expectedTask);
		testBuilder.addTasks(expectedTasks);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedTask, testOptions.get(1));
	}

	@Test
	void testAddTimeStampOutputs() {
		final List<String> testOptions;

		testBuilder.addTimeStampOutputs();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("-T", testOptions.get(0));
	}

	@Test
	void testAddXunit() {
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final String expectedXunit = "xunit";
		final List<String> testOptions;

		EasyMock.expect(mockedFile.getPath()).andReturn(expectedXunit).once();
		EasyMock.replay(mockedFile);
		testBuilder.addXunit(mockedFile);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedXunit, testOptions.get(1));
		EasyMock.verify(mockedFile);
	}

	@Test
	void testAddXunitSkipNonCritical() {
		final List<String> testOptions;

		testBuilder.addXunitSkipNonCritical();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--xunitskipnoncritical", testOptions.get(0));
	}
}
