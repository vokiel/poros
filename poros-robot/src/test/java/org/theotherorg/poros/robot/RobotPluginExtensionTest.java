package org.theotherorg.poros.robot;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.easymock.EasyMock;
import org.gradle.api.Action;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotPluginExtensionTest {
	private interface ActionBase extends Action<AbstractBaseProto> {
	}

	private interface ActionLibDoc extends Action<RobotLibdocProto> {
	}

	private interface ActionRebot extends Action<RobotRebotProto> {
	}

	private interface ActionReport extends Action<AbstractReportingProto> {
	}

	private interface ActionRun extends Action<RobotRunProto> {
	}

	private interface ActionTestdoc extends Action<RobotTestdocProto> {
	}

	private interface ActionTidy extends Action<RobotTidyProto> {
	}

	private interface FileProperty extends Property<File> {
	}

	private interface FileProvider extends Provider<File> {
	}

	private ObjectFactory mockedFactory;
	private RobotLibdocProto mockedLibdoc;
	private RobotRebotProto mockedRebot;
	private Property<File> mockedReportsDir;
	private RobotRunProto mockedRun;
	private RobotTestdocProto mockedTestdoc;
	private RobotTidyProto mockedTidy;
	private RobotPluginExtension testExtension;

	@BeforeEach
	void setUp() {
		mockedFactory = EasyMock.createStrictMock(ObjectFactory.class);
		mockedLibdoc = EasyMock.createStrictMock(RobotLibdocProto.class);
		mockedRebot = EasyMock.createStrictMock(RobotRebotProto.class);
		mockedReportsDir = EasyMock.createStrictMock(FileProperty.class);
		mockedRun = EasyMock.createStrictMock(RobotRunProto.class);
		mockedTestdoc = EasyMock.createStrictMock(RobotTestdocProto.class);
		mockedTidy = EasyMock.createStrictMock(RobotTidyProto.class);
		EasyMock.expect(mockedFactory.newInstance(RobotLibdocProto.class)).andReturn(mockedLibdoc).once();
		EasyMock.expect(mockedFactory.newInstance(RobotRebotProto.class)).andReturn(mockedRebot).once();
		EasyMock.expect(mockedFactory.property(File.class)).andReturn(mockedReportsDir).once();
		EasyMock.expect(mockedFactory.newInstance(RobotRunProto.class)).andReturn(mockedRun).once();
		EasyMock.expect(mockedFactory.newInstance(RobotTestdocProto.class)).andReturn(mockedTestdoc).once();
		EasyMock.expect(mockedFactory.newInstance(RobotTidyProto.class)).andReturn(mockedTidy).once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy);
		testExtension = new RobotPluginExtension(mockedFactory);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy);
		EasyMock.reset(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy);
	}

	@Test
	void testCommon() {
		final Action<AbstractBaseProto> mockedAction = EasyMock.createStrictMock(ActionBase.class);

		mockedAction.execute(mockedRebot);
		mockedAction.execute(mockedRun);
		mockedAction.execute(mockedTestdoc);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
		testExtension.common(mockedAction);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
	}

	@Test
	void testGetLibdocProto() {
		final File mockedFile = EasyMock.createStrictMock(File.class);

		EasyMock.expect(mockedLibdoc.getOutputDir()).andReturn(mockedFile).once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedFile);
		Assertions.assertSame(mockedLibdoc, testExtension.getLibdocProto());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedFile);
	}

	@Test
	void testGetLibdocProtoWithReportsDir() {
		final File mockedFile = EasyMock.createStrictMock(File.class);

		EasyMock.expect(mockedLibdoc.getOutputDir()).andReturn(null).once();
		EasyMock.expect(mockedReportsDir.get()).andReturn(mockedFile).once();
		mockedLibdoc.setOutputDir(mockedFile);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedFile);
		Assertions.assertSame(mockedLibdoc, testExtension.getLibdocProto());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedFile);
	}

	@Test
	void testGetRebotProto() {
		final File mockedDir = EasyMock.createStrictMock(File.class);
		final File mockedXml = EasyMock.createStrictMock(File.class);

		EasyMock.expect(mockedRebot.getOutputDir()).andReturn(mockedDir).once();
		EasyMock.expect(mockedRebot.getOutputXml()).andReturn(mockedXml).once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir, mockedXml);
		Assertions.assertSame(mockedRebot, testExtension.getRebotProto());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir, mockedXml);
	}

	@Test
	void testGetRebotProtoDefault() {
		final File mockedDir = EasyMock.createStrictMock(File.class);

		EasyMock.expect(mockedRebot.getOutputDir()).andReturn(null).once();
		EasyMock.expect(mockedReportsDir.get()).andReturn(mockedDir);
		mockedRebot.setOutputDir(mockedDir);
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedRebot.getOutputXml()).andReturn(null).once();
		EasyMock.expect(mockedReportsDir.get()).andReturn(mockedDir);
		EasyMock.expect(mockedDir.toPath()).andReturn(Paths.get("/")).once();
		mockedRebot.setOutputXml(EasyMock.anyObject(File.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir);
		Assertions.assertSame(mockedRebot, testExtension.getRebotProto());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir);
	}

	@Test
	void testGetReportsDir() {
		final File mockedDir = EasyMock.createStrictMock(File.class);

		EasyMock.expect(mockedReportsDir.get()).andReturn(mockedDir).once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy);
		Assertions.assertSame(mockedDir, testExtension.getReportsDir());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy);
	}

	@Test
	void testGetRunProto() {
		final File mockedDir = EasyMock.createStrictMock(File.class);

		EasyMock.expect(mockedRun.getOutputDir()).andReturn(mockedDir).once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir);
		Assertions.assertSame(mockedRun, testExtension.getRunProto());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir);
	}

	@Test
	void testGetRunProtoDefault() {
		final File mockedDir = EasyMock.createStrictMock(File.class);

		EasyMock.expect(mockedRun.getOutputDir()).andReturn(null).once();
		EasyMock.expect(mockedReportsDir.get()).andReturn(mockedDir);
		mockedRun.setOutputDir(mockedDir);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir);
		Assertions.assertSame(mockedRun, testExtension.getRunProto());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir);
	}

	@Test
	void testGetTestdocProto() {
		final File mockedDir = EasyMock.createStrictMock(File.class);
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Path mockedPath = EasyMock.createStrictMock(Path.class);

		EasyMock.expect(mockedTestdoc.getOutputFile()).andReturn(null).once();
		EasyMock.expect(mockedReportsDir.get()).andReturn(mockedDir);
		EasyMock.expect(mockedDir.toPath()).andReturn(mockedPath).once();
		EasyMock.expect(mockedPath.resolve(EasyMock.anyString())).andReturn(mockedPath).once();
		EasyMock.expect(mockedPath.toFile()).andReturn(mockedFile).once();
		mockedTestdoc.setOutputFile(mockedFile);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir, mockedFile, mockedPath);
		Assertions.assertSame(mockedTestdoc, testExtension.getTestdocProto());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedDir, mockedFile, mockedPath);
	}

	@Test
	void testGetTestdocProtoDefault() {
		final File mockedFile = EasyMock.createStrictMock(File.class);

		EasyMock.expect(mockedTestdoc.getOutputFile()).andReturn(mockedFile).once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedFile);
		Assertions.assertSame(mockedTestdoc, testExtension.getTestdocProto());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedFile);
	}

	@Test
	void testGetTidyProto() {
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy);
		Assertions.assertSame(mockedTidy, testExtension.getTidyProto());
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy);
	}

	@Test
	void testGetToolVersion() {
		Assertions.assertNull(testExtension.getToolVersion());
	}

	@Test
	void testLibdoc() {
		final Action<RobotLibdocProto> mockedAction = EasyMock.createStrictMock(ActionLibDoc.class);

		mockedAction.execute(mockedLibdoc);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
		testExtension.libdoc(mockedAction);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
	}

	@Test
	void testRebot() {
		final Action<RobotRebotProto> mockedAction = EasyMock.createStrictMock(ActionRebot.class);

		mockedAction.execute(mockedRebot);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
		testExtension.rebot(mockedAction);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
	}

	@Test
	void testReport() {
		final Action<AbstractReportingProto> mockedAction = EasyMock.createStrictMock(ActionReport.class);

		mockedAction.execute(mockedRebot);
		mockedAction.execute(mockedRun);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
		testExtension.report(mockedAction);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
	}

	@Test
	void testRun() {
		final Action<RobotRunProto> mockedAction = EasyMock.createStrictMock(ActionRun.class);

		mockedAction.execute(mockedRun);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
		testExtension.run(mockedAction);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
	}

	@Test
	void testSetReportsDir() {
		final File mockedFile = EasyMock.createStrictMock(File.class);

		mockedReportsDir.set(mockedFile);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedFile);
		testExtension.setReportsDir(mockedFile);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedFile);
	}

	@Test
	void testSetReportsDirProvider() {
		final Provider<File> mockedProvider = EasyMock.createStrictMock(FileProvider.class);

		mockedReportsDir.set(mockedProvider);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedProvider);
		testExtension.setReportsDir(mockedProvider);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedProvider);
	}

	@Test
	void testSetToolVersion() {
		final String expectedToolVersion = "version";

		testExtension.setToolVersion(expectedToolVersion);
		Assertions.assertEquals(expectedToolVersion, testExtension.getToolVersion());
	}

	@Test
	void testTestdoc() {
		final Action<RobotTestdocProto> mockedAction = EasyMock.createStrictMock(ActionTestdoc.class);

		mockedAction.execute(mockedTestdoc);
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
		testExtension.testdoc(mockedAction);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
	}

	@Test
	void testTidy() {
		final Action<RobotTidyProto> mockedAction = EasyMock.createStrictMock(ActionTidy.class);

		mockedAction.execute(mockedTidy);
		EasyMock.replay(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
		testExtension.tidy(mockedAction);
		EasyMock.verify(mockedFactory, mockedLibdoc, mockedRebot, mockedReportsDir, mockedRun, mockedTestdoc, mockedTidy, mockedAction);
	}
}
