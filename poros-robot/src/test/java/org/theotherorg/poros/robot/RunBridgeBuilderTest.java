package org.theotherorg.poros.robot;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.EasyMock;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.tasks.SourceSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.RunBuilder;

public class RunBridgeBuilderTest {
	private RunBuilder testBuilder;

	@BeforeEach
	void setUp() {
		testBuilder = new RunBuilder();
	}

	@Test
	void testAddConsoleMarkers() {
		final String expectedMarkers = "markers";
		final List<String> testOptions;

		testBuilder.addConsoleMarkers(expectedMarkers);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedMarkers, testOptions.get(1));
	}

	@Test
	void testAddConsoleType() {
		final String expectedType = "type";
		final List<String> testOptions;

		testBuilder.addConsoleType(expectedType);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedType, testOptions.get(1));
	}

	@Test
	void testAddConsoleWidth() {
		final int expectedWidth = 37;
		final List<String> testOptions;

		testBuilder.addConsoleWidth(expectedWidth);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(String.valueOf(expectedWidth), testOptions.get(1));
	}

	@Test
	void testAddDebugFile() {
		final File mockedDebug = EasyMock.createStrictMock(File.class);
		final String expectedDebugPath = "path";
		final List<String> testOptions;

		EasyMock.expect(mockedDebug.getPath()).andReturn(expectedDebugPath).once();
		EasyMock.replay(mockedDebug);
		testBuilder.addDebugFile(mockedDebug);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedDebugPath, testOptions.get(1));
		EasyMock.verify(mockedDebug);
	}

	@Test
	void testAddDryRun() {
		final List<String> testOptions;

		testBuilder.addDryRun();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--dryrun", testOptions.get(0));
	}

	@Test
	void testAddExitOnError() {
		final List<String> testOptions;

		testBuilder.addExitOnError();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--exitonerror", testOptions.get(0));
	}

	@Test
	void testAddExitOnFailures() {
		final List<String> testOptions;

		testBuilder.addExitOnFailures();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--exitonfailure", testOptions.get(0));
	}

	@Test
	void testAddExtensions() {
		final List<String> expectedExtensions = new ArrayList<>();
		final String expectedExtension1 = "extension1";
		final String expectedExtension2 = "extension2";
		final List<String> testOptions;

		expectedExtensions.add(expectedExtension1);
		expectedExtensions.add(expectedExtension2);
		testBuilder.addExtensions(expectedExtensions);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedExtension1 + ":" + expectedExtension2, testOptions.get(1));
	}

	@Test
	void testAddListeners() {
		final List<String> expectedListeners = new ArrayList<>();
		final String expectedListener = "listener";
		final List<String> testOptions;

		expectedListeners.add(expectedListener);
		testBuilder.addListeners(expectedListeners);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedListener, testOptions.get(1));
	}

	@Test
	void testAddMaxErrorLines() {
		final int expectedMax = 37;
		final List<String> testOptions;

		testBuilder.addMaxErrorLines(expectedMax);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(String.valueOf(expectedMax), testOptions.get(1));
	}

	@Test
	void testAddNoExtensions() {
		final List<String> testOptions;

		testBuilder.addExtensions(new ArrayList<>());
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertTrue(testOptions.isEmpty());
	}

	@Test
	void testAddPreRunModifiers() {
		final List<String> expectedModifiers = new ArrayList<>();
		final String expectedModifier = "modifier";
		final List<String> testOptions;

		expectedModifiers.add(expectedModifier);
		testBuilder.addPreRunModifiers(expectedModifiers);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedModifier, testOptions.get(1));
	}

	@Test
	void testAddRandomize() {
		final String expectedRandomize = "random";
		final List<String> testOptions;

		testBuilder.addRandomize(expectedRandomize);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedRandomize, testOptions.get(1));
	}

	@Test
	void testAddRerunFailed() {
		final File mockedFailed = EasyMock.createStrictMock(File.class);
		final String expectedFailedPath = "path";
		final List<String> testOptions;

		EasyMock.expect(mockedFailed.getAbsolutePath()).andReturn(expectedFailedPath).once();
		EasyMock.replay(mockedFailed);
		testBuilder.addRerunFailed(mockedFailed);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedFailedPath, testOptions.get(1));
		EasyMock.verify(mockedFailed);
	}

	@Test
	void testAddRerunFailedSuites() {
		final File mockedFailedSuites = EasyMock.createStrictMock(File.class);
		final String expectedFailedSuitesPath = "path";
		final List<String> testOptions;

		EasyMock.expect(mockedFailedSuites.getAbsolutePath()).andReturn(expectedFailedSuitesPath).once();
		EasyMock.replay(mockedFailedSuites);
		testBuilder.addRerunFailedSuites(mockedFailedSuites);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedFailedSuitesPath, testOptions.get(1));
		EasyMock.verify(mockedFailedSuites);
	}

	@Test
	void testAddRunEmptySuite() {
		final List<String> testOptions;

		testBuilder.addRunEmptySuite();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--runemptysuite", testOptions.get(0));
	}

	@Test
	void testAddSkipTearDownOnExit() {
		final List<String> testOptions;

		testBuilder.addSkipTearDownOnExit();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--skipteardownonexit", testOptions.get(0));
	}

	@Test
	void testAddVariableFiles() {
		final List<String> expectedFiles = new ArrayList<>();
		final String expectedFile = "file";
		final List<String> testOptions;

		expectedFiles.add(expectedFile);
		testBuilder.addVariableFiles(expectedFiles);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedFile, testOptions.get(1));
	}

	@Test
	void testAddVariables() {
		final Map<String, String> expectedVariables = new HashMap<>();
		final String expectedKey = "key";
		final String expectedVariable = "variable";
		final List<String> testOptions;

		expectedVariables.put(expectedKey, expectedVariable);
		testBuilder.addVariables(expectedVariables);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedKey + ":" + expectedVariable, testOptions.get(1));
	}

	@Test
	void testFromAll() {
		final RobotRunProto mockedRun = EasyMock.createStrictMock(RobotRunProto.class);
		final SourceSet mockedSourceSet = EasyMock.createStrictMock(SourceSet.class);
		final SourceDirectorySet mockedSourceDir = EasyMock.createStrictMock(SourceDirectorySet.class);
		final List<String> expectedExtensions = Collections.singletonList("extension");
		final File mockedFailed = EasyMock.createStrictMock(File.class);
		final String expectedFailedPath = "failedPath";
		final File mockedFailedSuites = EasyMock.createStrictMock(File.class);
		final String expectedFailedSuitesPath = "failedSuitesPath";
		final Map<String, String> expectedVariables = new HashMap<>();
		final String expectedKey = "key";
		final String expectedVariable = "variable";
		final List<String> expectedFiles = Collections.singletonList("file");
		final File mockedDebug = EasyMock.createStrictMock(File.class);
		final String expectedDebugPath = "debugPath";
		final int expectedMaxLines = 37;
		final List<String> expectedListeners = Collections.singletonList("listener");
		final String expectedRandom = "random";
		final List<String> expectedModifiers = Collections.singletonList("modifier");
		final String expectedMarkers = "markers";
		final String expectedType = "type";
		final int expectedWidth = 36;
		final RunBuilder testBuilder;
		final Set<String> testOptions;

		expectedVariables.put(expectedKey, expectedVariable);
		EasyMock.expect(mockedRun.getExtensions()).andReturn(expectedExtensions).once();
		EasyMock.expect(mockedRun.getRerunFailed()).andReturn(mockedFailed).once();
		EasyMock.expect(mockedRun.getRerunFailedSuites()).andReturn(mockedFailedSuites).once();
		EasyMock.expect(mockedRun.getVariables()).andReturn(expectedVariables).once();
		EasyMock.expect(mockedRun.getVariableFiles()).andReturn(expectedFiles).once();
		EasyMock.expect(mockedRun.getDebugFile()).andReturn(mockedDebug).once();
		EasyMock.expect(mockedRun.getMaxErrorLines()).andReturn(expectedMaxLines).once();
		EasyMock.expect(mockedRun.getListeners()).andReturn(expectedListeners).once();
		EasyMock.expect(mockedRun.getRandomize()).andReturn(expectedRandom).once();
		EasyMock.expect(mockedRun.getPreRunModifiers()).andReturn(expectedModifiers).once();
		EasyMock.expect(mockedRun.getConsoleMarkers()).andReturn(expectedMarkers).once();
		EasyMock.expect(mockedRun.getConsoleType()).andReturn(expectedType).once();
		EasyMock.expect(mockedRun.getConsoleWidth()).andReturn(expectedWidth).once();

		// Tested in BaseBridgeBuilderTest
		EasyMock.expect(mockedRun.getName()).andReturn(null).once();
		EasyMock.expect(mockedRun.getDoc()).andReturn(null).once();
		EasyMock.expect(mockedRun.getMetadata()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTags()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTests()).andReturn(null).once();
		EasyMock.expect(mockedRun.getSuites()).andReturn(null).once();
		EasyMock.expect(mockedRun.getIncludes()).andReturn(null).once();
		EasyMock.expect(mockedRun.getExcludes()).andReturn(null).once();
		EasyMock.expect(mockedRun.getPythonPaths()).andReturn(null).once();
		EasyMock.expect(mockedRun.getArgumentFiles()).andReturn(null).once();

		EasyMock.expect(mockedRun.isRunEmptySuite()).andReturn(true).once();
		EasyMock.expect(mockedRun.isDryRun()).andReturn(true).once();
		EasyMock.expect(mockedRun.isExitOnFailure()).andReturn(true).once();
		EasyMock.expect(mockedRun.isExitOnError()).andReturn(true).once();
		EasyMock.expect(mockedRun.isSkipTearDownOnExit()).andReturn(true).once();

		// Tested in ReportBridgeBuilderTest
		EasyMock.expect(mockedRun.getTasks()).andReturn(null).once();
		EasyMock.expect(mockedRun.getCriticals()).andReturn(null).once();
		EasyMock.expect(mockedRun.getNonCriticals()).andReturn(null).once();
		EasyMock.expect(mockedRun.getOutput()).andReturn(null).once();
		EasyMock.expect(mockedRun.getOutputDir()).andReturn(null).once();
		EasyMock.expect(mockedRun.getLog()).andReturn(null).once();
		EasyMock.expect(mockedRun.getLogLevel()).andReturn(null).once();
		EasyMock.expect(mockedRun.getLogTitle()).andReturn(null).once();
		EasyMock.expect(mockedRun.getReport()).andReturn(null).once();
		EasyMock.expect(mockedRun.getReportTitle()).andReturn(null).once();
		EasyMock.expect(mockedRun.getXunit()).andReturn(null).once();
		EasyMock.expect(mockedRun.getReportBackground()).andReturn(null).once();
		EasyMock.expect(mockedRun.getSuiteStatLevel()).andReturn(0).once();
		EasyMock.expect(mockedRun.getTagDocs()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTagStatCombines()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTagStatExcludes()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTagStatIncludes()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTagStatLinks()).andReturn(null).once();
		EasyMock.expect(mockedRun.getRemoveKeywords()).andReturn(null).once();
		EasyMock.expect(mockedRun.getFlattenKeywords()).andReturn(null).once();
		EasyMock.expect(mockedRun.getPreRebotModifiers()).andReturn(null).once();
		EasyMock.expect(mockedRun.getConsoleColors()).andReturn(null).once();
		EasyMock.expect(mockedFailed.getAbsolutePath()).andReturn(expectedFailedPath).once();
		EasyMock.expect(mockedFailedSuites.getAbsolutePath()).andReturn(expectedFailedSuitesPath).once();
		EasyMock.expect(mockedDebug.getPath()).andReturn(expectedDebugPath).once();
		EasyMock.expect(mockedRun.isRpa()).andReturn(false).once();
		EasyMock.expect(mockedRun.isSplitLog()).andReturn(false).once();
		EasyMock.expect(mockedRun.isTimeStampOutputs()).andReturn(false).once();
		EasyMock.expect(mockedRun.isXunitSkipNonCritical()).andReturn(false).once();
		EasyMock.expect(mockedRun.isNoStatusRc()).andReturn(false).once();
		EasyMock.replay(mockedRun, mockedFailed, mockedFailedSuites, mockedDebug, mockedSourceSet, mockedSourceDir);
		testBuilder = RunBuilder.from(mockedRun);
		Assertions.assertNotNull(testBuilder);
		testOptions = new HashSet<>(testBuilder.getOptions());
		Assertions.assertFalse(testBuilder.getOptions().isEmpty());
		Assertions.assertTrue(testOptions.contains(expectedExtensions.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedFailedPath));
		Assertions.assertTrue(testOptions.contains(expectedFailedSuitesPath));
		Assertions.assertTrue(testOptions.contains(expectedKey + ":" + expectedVariable));
		Assertions.assertTrue(testOptions.contains(expectedFiles.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedDebugPath));
		Assertions.assertTrue(testOptions.contains(String.valueOf(expectedMaxLines)));
		Assertions.assertTrue(testOptions.contains(expectedListeners.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedRandom));
		Assertions.assertTrue(testOptions.contains(expectedModifiers.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedMarkers));
		Assertions.assertTrue(testOptions.contains(expectedType));
		Assertions.assertTrue(testOptions.contains(String.valueOf(expectedWidth)));
		Assertions.assertTrue(testOptions.contains("--runemptysuite"));
		Assertions.assertTrue(testOptions.contains("--dryrun"));
		Assertions.assertTrue(testOptions.contains("--exitonfailure"));
		Assertions.assertTrue(testOptions.contains("--exitonerror"));
		Assertions.assertTrue(testOptions.contains("--skipteardownonexit"));
		EasyMock.verify(mockedRun, mockedFailed, mockedFailedSuites, mockedDebug, mockedSourceSet, mockedSourceDir);
	}

	@Test
	void testFromEmpty() {
		final RobotRunProto mockedRun = EasyMock.createStrictMock(RobotRunProto.class);
		final RunBuilder testBuilder;

		EasyMock.expect(mockedRun.getExtensions()).andReturn(null).once();
		EasyMock.expect(mockedRun.getRerunFailed()).andReturn(null).once();
		EasyMock.expect(mockedRun.getRerunFailedSuites()).andReturn(null).once();
		EasyMock.expect(mockedRun.getVariables()).andReturn(null).once();
		EasyMock.expect(mockedRun.getVariableFiles()).andReturn(null).once();
		EasyMock.expect(mockedRun.getDebugFile()).andReturn(null).once();
		EasyMock.expect(mockedRun.getMaxErrorLines()).andReturn(0).once();
		EasyMock.expect(mockedRun.getListeners()).andReturn(null).once();
		EasyMock.expect(mockedRun.getRandomize()).andReturn(null).once();
		EasyMock.expect(mockedRun.getPreRunModifiers()).andReturn(null).once();
		EasyMock.expect(mockedRun.getConsoleMarkers()).andReturn(null).once();
		EasyMock.expect(mockedRun.getConsoleType()).andReturn(null).once();
		EasyMock.expect(mockedRun.getConsoleWidth()).andReturn(0).once();

		EasyMock.expect(mockedRun.getName()).andReturn(null).once();
		EasyMock.expect(mockedRun.getDoc()).andReturn(null).once();
		EasyMock.expect(mockedRun.getMetadata()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTags()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTests()).andReturn(null).once();
		EasyMock.expect(mockedRun.getSuites()).andReturn(null).once();
		EasyMock.expect(mockedRun.getIncludes()).andReturn(null).once();
		EasyMock.expect(mockedRun.getExcludes()).andReturn(null).once();
		EasyMock.expect(mockedRun.getPythonPaths()).andReturn(null).once();
		EasyMock.expect(mockedRun.getArgumentFiles()).andReturn(null).once();

		EasyMock.expect(mockedRun.isRunEmptySuite()).andReturn(false).once();
		EasyMock.expect(mockedRun.isDryRun()).andReturn(false).once();
		EasyMock.expect(mockedRun.isExitOnFailure()).andReturn(false).once();
		EasyMock.expect(mockedRun.isExitOnError()).andReturn(false).once();
		EasyMock.expect(mockedRun.isSkipTearDownOnExit()).andReturn(false).once();

		EasyMock.expect(mockedRun.getTasks()).andReturn(null).once();
		EasyMock.expect(mockedRun.getCriticals()).andReturn(null).once();
		EasyMock.expect(mockedRun.getNonCriticals()).andReturn(null).once();
		EasyMock.expect(mockedRun.getOutput()).andReturn(null).once();
		EasyMock.expect(mockedRun.getOutputDir()).andReturn(null).once();
		EasyMock.expect(mockedRun.getLog()).andReturn(null).once();
		EasyMock.expect(mockedRun.getLogLevel()).andReturn(null).once();
		EasyMock.expect(mockedRun.getLogTitle()).andReturn(null).once();
		EasyMock.expect(mockedRun.getReport()).andReturn(null).once();
		EasyMock.expect(mockedRun.getReportTitle()).andReturn(null).once();
		EasyMock.expect(mockedRun.getXunit()).andReturn(null).once();
		EasyMock.expect(mockedRun.getReportBackground()).andReturn(null).once();
		EasyMock.expect(mockedRun.getSuiteStatLevel()).andReturn(0).once();
		EasyMock.expect(mockedRun.getTagDocs()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTagStatCombines()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTagStatExcludes()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTagStatIncludes()).andReturn(null).once();
		EasyMock.expect(mockedRun.getTagStatLinks()).andReturn(null).once();
		EasyMock.expect(mockedRun.getRemoveKeywords()).andReturn(null).once();
		EasyMock.expect(mockedRun.getFlattenKeywords()).andReturn(null).once();
		EasyMock.expect(mockedRun.getPreRebotModifiers()).andReturn(null).once();
		EasyMock.expect(mockedRun.getConsoleColors()).andReturn(null).once();
		EasyMock.expect(mockedRun.isRpa()).andReturn(false).once();
		EasyMock.expect(mockedRun.isSplitLog()).andReturn(false).once();
		EasyMock.expect(mockedRun.isTimeStampOutputs()).andReturn(false).once();
		EasyMock.expect(mockedRun.isXunitSkipNonCritical()).andReturn(false).once();
		EasyMock.expect(mockedRun.isNoStatusRc()).andReturn(false).once();
		EasyMock.replay(mockedRun);
		testBuilder = RunBuilder.from(mockedRun);
		Assertions.assertNotNull(testBuilder);
		Assertions.assertTrue(testBuilder.getOptions().isEmpty());
		EasyMock.verify(mockedRun);
	}

	@Test
	void testToArgs() {
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final File mockedFile2 = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = new LinkedHashSet<>();
		final String[] expectedArgs = new String[] {"run", "path.robot"};
		final String[] testArgs;

		testFiles.add(mockedFile);
		testFiles.add(mockedFile2);
		EasyMock.expect(mockedFile.toPath()).andReturn(Paths.get(expectedArgs[1])).once();
		EasyMock.expect(mockedFile.getPath()).andReturn(expectedArgs[1]).once();
		EasyMock.expect(mockedFile2.toPath()).andReturn(Paths.get("notvalid")).once();
		EasyMock.replay(mockedFile, mockedFile2);
		testArgs = testBuilder.toArgs(testFiles);
		Assertions.assertNotNull(testArgs);
		Assertions.assertArrayEquals(expectedArgs, testArgs);
		EasyMock.verify(mockedFile, mockedFile2);
	}

	@Test
	void testToArgsNull() {
		final String[] testArgs = testBuilder.toArgs(null);

		Assertions.assertNotNull(testArgs);
		Assertions.assertEquals(1, testArgs.length);
	}
}
