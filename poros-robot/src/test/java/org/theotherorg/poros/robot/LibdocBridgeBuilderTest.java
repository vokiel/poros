package org.theotherorg.poros.robot;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.LibdocBuilder;

public class LibdocBridgeBuilderTest {
	private LibdocBuilder testBuilder;

	@BeforeEach
	void setUp() {
		testBuilder = new LibdocBuilder();
	}

	@Test
	void testAddDocFormat() {
		final String expectedFormat = "format";
		final List<String> testOptions;

		testBuilder.addDocFormat(expectedFormat);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedFormat, testOptions.get(1));
	}

	@Test
	void testAddFormat() {
		final String expectedFormat = "format";
		final List<String> testOptions;

		testBuilder.addFormat(expectedFormat);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedFormat, testOptions.get(1));
	}

	@Test
	void testFromAll() {
		final RobotLibdocProto mockedLibdoc = EasyMock.createStrictMock(RobotLibdocProto.class);
		final String expectedFormat = "format";
		final String expectedDocFormat = "docFormat";
		final String expectedVersion = "version";
		final LibdocBuilder testBuilder;
		final Set<String> testOptions;

		EasyMock.expect(mockedLibdoc.getFormat()).andReturn(expectedFormat).once();
		EasyMock.expect(mockedLibdoc.getDocFormat()).andReturn(expectedDocFormat).once();
		EasyMock.expect(mockedLibdoc.getVersion()).andReturn(expectedVersion).once();
		EasyMock.replay(mockedLibdoc);
		testBuilder = LibdocBuilder.from(mockedLibdoc);
		Assertions.assertNotNull(testBuilder);
		testOptions = new HashSet<>(testBuilder.getOptions());
		Assertions.assertTrue(testOptions.contains(expectedFormat));
		Assertions.assertTrue(testOptions.contains(expectedDocFormat));
		Assertions.assertTrue(testOptions.contains(expectedVersion));
		EasyMock.verify(mockedLibdoc);
	}

	@Test
	void testFromEmpty() {
		final RobotLibdocProto mockedLibdoc = EasyMock.createStrictMock(RobotLibdocProto.class);
		final LibdocBuilder testBuilder;

		EasyMock.expect(mockedLibdoc.getFormat()).andReturn(null).once();
		EasyMock.expect(mockedLibdoc.getDocFormat()).andReturn(null).once();
		EasyMock.expect(mockedLibdoc.getVersion()).andReturn(null).once();
		EasyMock.replay(mockedLibdoc);
		testBuilder = LibdocBuilder.from(mockedLibdoc);
		Assertions.assertNotNull(testBuilder);
		Assertions.assertTrue(testBuilder.getOptions().isEmpty());
		EasyMock.verify(mockedLibdoc);
	}

	@Test
	void testToArgs() {
		final File mockedInput = EasyMock.createStrictMock(File.class);
		final File mockedOutput = EasyMock.createStrictMock(File.class);
		final String expectedInput = "input";
		final String expectedOutput = "output";
		final String[] testArgs;

		EasyMock.expect(mockedInput.getPath()).andReturn(expectedInput).once();
		EasyMock.expect(mockedOutput.getPath()).andReturn(expectedOutput).once();
		EasyMock.replay(mockedInput, mockedOutput);
		testArgs = testBuilder.toArgs(mockedInput, mockedOutput);
		Assertions.assertNotNull(testArgs);
		Assertions.assertEquals(3, testArgs.length);
		Assertions.assertArrayEquals(new String[] {"libdoc", expectedInput, expectedOutput}, testArgs);
		EasyMock.verify(mockedInput, mockedOutput);
	}

	@Test
	void testToArgsNullInput() {
		final String[] testArgs = testBuilder.toArgs(null, null);

		Assertions.assertNotNull(testArgs);
		Assertions.assertEquals(1, testArgs.length);
	}

	@Test
	void testToArgsNullOutput() {
		final File mockedInput = EasyMock.createStrictMock(File.class);
		final String expectedInput = "input";
		final String[] testArgs;

		EasyMock.expect(mockedInput.getPath()).andReturn(expectedInput).once();
		EasyMock.replay(mockedInput);
		testArgs = testBuilder.toArgs(mockedInput, null);
		Assertions.assertNotNull(testArgs);
		Assertions.assertEquals(2, testArgs.length);
		Assertions.assertEquals(expectedInput, testArgs[1]);
		EasyMock.verify(mockedInput);
	}
}
