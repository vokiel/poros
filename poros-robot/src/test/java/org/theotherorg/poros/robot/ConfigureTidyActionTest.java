package org.theotherorg.poros.robot;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import org.easymock.EasyMock;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.tasks.SourceSet;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureTidyAction;

public class ConfigureTidyActionTest {
	private interface ClasspathSupplier extends Supplier<Iterable<File>> {
	}

	private interface SourceSetSupplier extends Supplier<SourceSet> {
	}

	@Test
	void testExecute() {
		final Supplier<Iterable<File>> mockedClasspath = EasyMock.createStrictMock(ClasspathSupplier.class);
		final Supplier<SourceSet> mockedSources = EasyMock.createStrictMock(SourceSetSupplier.class);
		final RobotTidyProto mockedProto = EasyMock.createStrictMock(RobotTidyProto.class);
		final RobotRunnerBridge mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		final RobotTidyTask testTask = ProjectBuilder.builder().build().getTasks().create("testTidy", RobotTidyTask.class, mockedBridge);
		final SourceSet mockedSourceSet = EasyMock.createStrictMock(SourceSet.class);
		final SourceDirectorySet mockedDirSet = EasyMock.createStrictMock(SourceDirectorySet.class);
		final Set<File> expectedClasspath = new HashSet<>();
		final ConfigureTidyAction testAction;

		EasyMock.expect(mockedSources.get()).andReturn(mockedSourceSet).once();
		EasyMock.expect(mockedSourceSet.getResources()).andReturn(mockedDirSet).once();
		EasyMock.expect(mockedDirSet.getFiles()).andReturn(new HashSet<>()).once();
		EasyMock.expect(mockedClasspath.get()).andReturn(expectedClasspath).once();
		EasyMock.replay(mockedClasspath, mockedSources, mockedProto, mockedBridge, mockedSourceSet, mockedDirSet);
		testAction = new ConfigureTidyAction(mockedClasspath, mockedSources, mockedProto);
		testAction.execute(testTask);
		Assertions.assertNotNull(testTask.getDescription());
		Assertions.assertSame(mockedProto, testTask.getRobotProto());
		Assertions.assertNotNull(testTask.getSourceFiles());
		Assertions.assertTrue(testTask.getSourceFiles().isEmpty());
		Assertions.assertSame(expectedClasspath, testTask.getLibraryClasspath());
		EasyMock.verify(mockedClasspath, mockedSources, mockedProto, mockedBridge, mockedSourceSet, mockedDirSet);
	}
}
