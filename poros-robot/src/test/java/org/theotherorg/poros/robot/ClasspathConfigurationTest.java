package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Set;
import java.util.function.Supplier;

import org.easymock.EasyMock;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.ResolvedConfiguration;
import org.gradle.api.file.FileCollection;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetOutput;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.python.google.common.collect.Sets;
import org.theotherorg.poros.robot.RobotPlugin.ClasspathConfiguration;

public class ClasspathConfigurationTest {
	private interface SourceSetSupplier extends Supplier<SourceSet> {
	}

	@Test
	void testGet() {
		final Supplier<SourceSet> mockedSupplier = EasyMock.createStrictMock(SourceSetSupplier.class);
		final SourceSet mockedSourceSet = EasyMock.createStrictMock(SourceSet.class);
		final Configuration mockedConfig = EasyMock.createStrictMock(Configuration.class);
		final ResolvedConfiguration mockedResolution = EasyMock.createStrictMock(ResolvedConfiguration.class);
		final SourceSetOutput mockedOutput = EasyMock.createStrictMock(SourceSetOutput.class);
		final FileCollection mockedClasspath = EasyMock.createStrictMock(FileCollection.class);
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> expectedFiles = Sets.newHashSet(mockedFile);
		final ClasspathConfiguration testSupplier;
		final Iterable<File> testFiles;

		EasyMock.expect(mockedSupplier.get()).andReturn(mockedSourceSet).once();
		EasyMock.expect(mockedConfig.getResolvedConfiguration()).andReturn(mockedResolution).once();
		EasyMock.expect(mockedResolution.getFiles()).andReturn(expectedFiles).once();
		EasyMock.expect(mockedSourceSet.getOutput()).andReturn(mockedOutput).once();
		EasyMock.expect(mockedOutput.getFiles()).andReturn(Sets.newHashSet()).once();
		EasyMock.expect(mockedSourceSet.getCompileClasspath()).andReturn(mockedClasspath).once();
		EasyMock.expect(mockedClasspath.getFiles()).andReturn(Sets.newHashSet()).once();
		EasyMock.replay(mockedConfig, mockedFile, mockedSupplier, mockedSourceSet, mockedOutput, mockedResolution, mockedClasspath);
		testSupplier = new ClasspathConfiguration(mockedConfig, mockedSupplier);
		testFiles = testSupplier.get();
		Assertions.assertEquals(expectedFiles, testFiles);
		Assertions.assertNotNull(testSupplier.get());
		EasyMock.verify(mockedConfig, mockedFile, mockedSupplier, mockedSourceSet, mockedOutput, mockedResolution, mockedClasspath);
	}
}
