package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Set;

import org.easymock.EasyMock;
import org.gradle.api.file.FileCollection;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.python.google.common.collect.Sets;

class RobotLibdocTaskTest {
	private RobotRunnerBridge mockedBridge;
	private RobotLibdocTask testTask;

	@BeforeEach
	void setUp() {
		mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		testTask = ProjectBuilder.builder().build().getTasks().create("testLibdoc", RobotLibdocTask.class, mockedBridge);
	}

	@Test
	void testGetLibraryFiles() {
		EasyMock.replay(mockedBridge);
		Assertions.assertNull(testTask.getLibraryFiles());
		EasyMock.verify(mockedBridge);
	}

	@Test
	void testRobotLibdoc() {
		final RobotLibdocProto mockedProto = EasyMock.createStrictMock(RobotLibdocProto.class);
		final FileCollection mockedClasspath = EasyMock.createStrictMock(FileCollection.class);
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = Sets.newHashSet(mockedFile);

		mockedBridge.libDoc(mockedProto, testFiles, mockedClasspath);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBridge, mockedProto, mockedClasspath, mockedFile);
		testTask.setRobotProto(mockedProto);
		testTask.setLibraryClasspath(mockedClasspath);
		testTask.setLibraryFiles(testFiles);
		testTask.robotLibdoc();
		EasyMock.verify(mockedBridge, mockedProto, mockedClasspath, mockedFile);
	}

	@Test
	void testSetLibraryFiles() {
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = Sets.newHashSet(mockedFile);

		EasyMock.replay(mockedBridge, mockedFile);
		testTask.setLibraryFiles(testFiles);
		Assertions.assertEquals(1, testTask.getLibraryFiles().size());
		Assertions.assertSame(mockedFile, testTask.getLibraryFiles().iterator().next());
		EasyMock.verify(mockedBridge, mockedFile);
	}
}
