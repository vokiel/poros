package org.theotherorg.poros.robot;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AbstractReportingProtoTest {
	private static final class TestReportingProto extends AbstractReportingProto {
	}

	private TestReportingProto testReporting;

	@BeforeEach
	void setUp() {
		testReporting = new TestReportingProto();
	}

	@Test
	void testGetConsoleColors() {
		Assertions.assertNull(testReporting.getConsoleColors());
	}

	@Test
	void testGetCriticals() {
		Assertions.assertNull(testReporting.getCriticals());
	}

	@Test
	void testGetFlattenKeywords() {
		Assertions.assertNull(testReporting.getFlattenKeywords());
	}

	@Test
	void testGetLog() {
		Assertions.assertNull(testReporting.getLog());
	}

	@Test
	void testGetLogLevel() {
		Assertions.assertNull(testReporting.getLogLevel());
	}

	@Test
	void testGetLogTitle() {
		Assertions.assertNull(testReporting.getLogTitle());
	}

	@Test
	void testGetNonCriticals() {
		Assertions.assertNull(testReporting.getNonCriticals());
	}

	@Test
	void testGetOutput() {
		Assertions.assertNull(testReporting.getOutput());
	}

	@Test
	void testGetOutputDir() {
		Assertions.assertNull(testReporting.getOutputDir());
	}

	@Test
	void testGetPreRebotModifiers() {
		Assertions.assertNull(testReporting.getPreRebotModifiers());
	}

	@Test
	void testGetRemoveKeywords() {
		Assertions.assertNull(testReporting.getRemoveKeywords());
	}

	@Test
	void testGetReport() {
		Assertions.assertNull(testReporting.getReport());
	}

	@Test
	void testGetReportBackground() {
		Assertions.assertNull(testReporting.getReportBackground());
	}

	@Test
	void testGetReportTitle() {
		Assertions.assertNull(testReporting.getReportTitle());
	}

	@Test
	void testGetSuiteStatLevel() {
		Assertions.assertEquals(0, testReporting.getSuiteStatLevel());
	}

	@Test
	void testGetTagDocs() {
		Assertions.assertNull(testReporting.getTagDocs());
	}

	@Test
	void testGetTagStatCombines() {
		Assertions.assertNull(testReporting.getTagStatCombines());
	}

	@Test
	void testGetTagStatExcludes() {
		Assertions.assertNull(testReporting.getTagStatExcludes());
	}

	@Test
	void testGetTagStatIncludes() {
		Assertions.assertNull(testReporting.getTagStatIncludes());
	}

	@Test
	void testGetTagStatLinks() {
		Assertions.assertNull(testReporting.getTagStatLinks());
	}

	@Test
	void testGetTasks() {
		Assertions.assertNull(testReporting.getTasks());
	}

	@Test
	void testGetXunit() {
		Assertions.assertNull(testReporting.getXunit());
	}

	@Test
	void testIsNoStatusRc() {
		Assertions.assertFalse(testReporting.isNoStatusRc());
	}

	@Test
	void testIsRpa() {
		Assertions.assertFalse(testReporting.isRpa());
	}

	@Test
	void testIsSplitLog() {
		Assertions.assertFalse(testReporting.isSplitLog());
	}

	@Test
	void testIsTimeStampOutputs() {
		Assertions.assertFalse(testReporting.isTimeStampOutputs());
	}

	@Test
	void testIsXunitSkipNonCritical() {
		Assertions.assertFalse(testReporting.isXunitSkipNonCritical());
	}

	@Test
	void testSetConsoleColors() {
		final String expectedColors = "colors";

		testReporting.setConsoleColors(expectedColors);
		Assertions.assertEquals(expectedColors, testReporting.getConsoleColors());
	}

	@Test
	void testSetCriticals() {
		final List<String> expectedCriticals = new ArrayList<>();
		final List<String> testCriticals = new ArrayList<>();

		expectedCriticals.add("critical");
		testCriticals.add("critical");
		testReporting.setCriticals(testCriticals);
		Assertions.assertEquals(expectedCriticals, testReporting.getCriticals());
	}

	@Test
	void testSetFlattenKeywords() {
		final List<String> expectedKeywords = new ArrayList<>();
		final List<String> testKeywords = new ArrayList<>();

		expectedKeywords.add("keyword");
		testKeywords.add("keyword");
		testReporting.setFlattenKeywords(testKeywords);
		Assertions.assertEquals(expectedKeywords, testReporting.getFlattenKeywords());
	}

	@Test
	void testSetLog() {
		final File mockedLog = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedLog);
		testReporting.setLog(mockedLog);
		Assertions.assertSame(mockedLog, testReporting.getLog());
		EasyMock.verify(mockedLog);
	}

	@Test
	void testSetLogLevel() {
		final String expectedLevel = "level";

		testReporting.setLogLevel(expectedLevel);
		Assertions.assertEquals(expectedLevel, testReporting.getLogLevel());
	}

	@Test
	void testSetLogTitle() {
		final String expectedTitle = "title";

		testReporting.setLogTitle(expectedTitle);
		Assertions.assertEquals(expectedTitle, testReporting.getLogTitle());
	}

	@Test
	void testSetNoStatusRc() {
		testReporting.setNoStatusRc(true);
		Assertions.assertTrue(testReporting.isNoStatusRc());
	}

	@Test
	void testSetNonCriticals() {
		final List<String> expectedNonCriticals = new ArrayList<>();
		final List<String> testNonCriticals = new ArrayList<>();

		expectedNonCriticals.add("nonCritical");
		testNonCriticals.add("nonCritical");
		testReporting.setNonCriticals(testNonCriticals);
		Assertions.assertEquals(expectedNonCriticals, testReporting.getNonCriticals());
	}

	@Test
	void testSetOutput() {
		final File mockedOutput = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedOutput);
		testReporting.setOutput(mockedOutput);
		Assertions.assertSame(mockedOutput, testReporting.getOutput());
		EasyMock.verify(mockedOutput);
	}

	@Test
	void testSetOutputDir() {
		final File mockedOutputDir = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedOutputDir);
		testReporting.setOutputDir(mockedOutputDir);
		Assertions.assertSame(mockedOutputDir, testReporting.getOutputDir());
		EasyMock.verify(mockedOutputDir);
	}

	@Test
	void testSetPreRebotModifiers() {
		final List<String> expectedModifiers = new ArrayList<>();
		final List<String> testModifiers = new ArrayList<>();

		expectedModifiers.add("modifier");
		testModifiers.add("modifier");
		testReporting.setPreRebotModifiers(testModifiers);
		Assertions.assertEquals(expectedModifiers, testReporting.getPreRebotModifiers());
	}

	@Test
	void testSetRemoveKeywords() {
		final List<String> expectedKeywords = new ArrayList<>();
		final List<String> testKeywords = new ArrayList<>();

		expectedKeywords.add("keyword");
		testKeywords.add("keyword");
		testReporting.setRemoveKeywords(testKeywords);
		Assertions.assertEquals(expectedKeywords, testReporting.getRemoveKeywords());
	}

	@Test
	void testSetReport() {
		final File mockedReport = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedReport);
		testReporting.setReport(mockedReport);
		Assertions.assertSame(mockedReport, testReporting.getReport());
		EasyMock.verify(mockedReport);
	}

	@Test
	void testSetReportBackground() {
		final String expectedBackground = "background";

		testReporting.setReportBackground(expectedBackground);
		Assertions.assertEquals(expectedBackground, testReporting.getReportBackground());
	}

	@Test
	void testSetReportTitle() {
		final String expectedTitle = "title";

		testReporting.setReportTitle(expectedTitle);
		Assertions.assertEquals(expectedTitle, testReporting.getReportTitle());
	}

	@Test
	void testSetRpa() {
		testReporting.setRpa(true);
		Assertions.assertTrue(testReporting.isRpa());
	}

	@Test
	void testSetSplitLog() {
		testReporting.setSplitLog(true);
		Assertions.assertTrue(testReporting.isSplitLog());
	}

	@Test
	void testSetSuiteStatLevel() {
		final int expectedLevel = 37;

		testReporting.setSuiteStatLevel(expectedLevel);
		Assertions.assertEquals(37, testReporting.getSuiteStatLevel());
	}

	@Test
	void testSetTagDocs() {
		final List<String> expectedDoc = new ArrayList<>();
		final List<String> testDoc = new ArrayList<>();

		expectedDoc.add("tagDoc");
		testDoc.add("tagDoc");
		testReporting.setTagDocs(testDoc);
		Assertions.assertEquals(expectedDoc, testReporting.getTagDocs());
	}

	@Test
	void testSetTagStatCombines() {
		final List<String> expectedCombines = new ArrayList<>();
		final List<String> testCombines = new ArrayList<>();

		expectedCombines.add("combine");
		testCombines.add("combine");
		testReporting.setTagStatCombines(testCombines);
		Assertions.assertEquals(expectedCombines, testReporting.getTagStatCombines());
	}

	@Test
	void testSetTagStatExcludes() {
		final List<String> expectedExcludes = new ArrayList<>();
		final List<String> testExcludes = new ArrayList<>();

		expectedExcludes.add("exclude");
		testExcludes.add("exclude");
		testReporting.setTagStatExcludes(testExcludes);
		Assertions.assertEquals(expectedExcludes, testReporting.getTagStatExcludes());
	}

	@Test
	void testSetTagStatIncludes() {
		final List<String> expectedIncludes = new ArrayList<>();
		final List<String> testIncludes = new ArrayList<>();

		expectedIncludes.add("include");
		testIncludes.add("include");
		testReporting.setTagStatIncludes(testIncludes);
		Assertions.assertEquals(expectedIncludes, testReporting.getTagStatIncludes());
	}

	@Test
	void testSetTagStatLinks() {
		final List<String> expectedLinks = new ArrayList<>();
		final List<String> testLinks = new ArrayList<>();

		expectedLinks.add("link");
		testLinks.add("link");
		testReporting.setTagStatLinks(testLinks);
		Assertions.assertEquals(expectedLinks, testReporting.getTagStatLinks());
	}

	@Test
	void testSetTasks() {
		final List<String> expectedTasks = new ArrayList<>();
		final List<String> testTasks = new ArrayList<>();

		expectedTasks.add("task");
		testTasks.add("task");
		testReporting.setTasks(testTasks);
		Assertions.assertEquals(expectedTasks, testReporting.getTasks());
	}

	@Test
	void testSetTimeStampOutputs() {
		testReporting.setTimeStampOutputs(true);
		Assertions.assertTrue(testReporting.isTimeStampOutputs());
	}

	@Test
	void testSetXunit() {
		final File mockedXunit = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedXunit);
		testReporting.setXunit(mockedXunit);
		Assertions.assertSame(mockedXunit, testReporting.getXunit());
		EasyMock.verify(mockedXunit);
	}

	@Test
	void testSetXunitSkipNonCritical() {
		testReporting.setXunitSkipNonCritical(true);
		Assertions.assertTrue(testReporting.isXunitSkipNonCritical());
	}
}
