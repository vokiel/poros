package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Collections;
import java.util.Set;

import org.easymock.EasyMock;
import org.gradle.api.file.FileCollection;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotTestdocTaskTest {
	private RobotRunnerBridge mockedBridge;
	private RobotTestdocTask testTask;

	@BeforeEach
	void setUp() {
		mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		testTask = ProjectBuilder.builder().build().getTasks().create("testTestdoc", RobotTestdocTask.class, mockedBridge);
	}

	@Test
	void testGetSourceFiles() {
		EasyMock.replay(mockedBridge);
		Assertions.assertNull(testTask.getSourceFiles());
		EasyMock.verify(mockedBridge);
	}

	@Test
	void testRobotTestdoc() {
		final FileCollection mockedClasspath = EasyMock.createStrictMock(FileCollection.class);
		final RobotTestdocProto mockedProto = EasyMock.createStrictMock(RobotTestdocProto.class);
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = Collections.singleton(mockedFile);

		mockedBridge.testDoc(mockedProto, testFiles, mockedClasspath);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBridge, mockedClasspath, mockedFile, mockedProto);
		testTask.setLibraryClasspath(mockedClasspath);
		testTask.setSourceFiles(testFiles);
		testTask.setRobotProto(mockedProto);
		testTask.robotTestdoc();
		EasyMock.verify(mockedBridge, mockedClasspath, mockedFile, mockedProto);
	}
}
