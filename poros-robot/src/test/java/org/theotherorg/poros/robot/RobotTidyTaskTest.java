package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Collections;
import java.util.Set;

import org.easymock.EasyMock;
import org.gradle.api.file.FileCollection;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotTidyTaskTest {
	private RobotRunnerBridge mockedBridge;
	private RobotTidyTask testTask;

	@BeforeEach
	void setUp() {
		mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		testTask = ProjectBuilder.builder().build().getTasks().create("testTidy", RobotTidyTask.class, mockedBridge);
	}

	@Test
	void testRobotTidy() {
		final FileCollection mockedClasspath = EasyMock.createStrictMock(FileCollection.class);
		final RobotTidyProto mockedProto = EasyMock.createStrictMock(RobotTidyProto.class);
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = Collections.singleton(mockedFile);

		mockedBridge.tidy(mockedProto, testFiles, mockedClasspath);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBridge, mockedClasspath, mockedFile, mockedProto);
		testTask.setLibraryClasspath(mockedClasspath);
		testTask.setSourceFiles(testFiles);
		testTask.setRobotProto(mockedProto);
		testTask.robotTidy();
		EasyMock.verify(mockedBridge, mockedClasspath, mockedFile, mockedProto);
	}
}
