package org.theotherorg.poros.robot;

import java.util.Collections;
import java.util.function.Supplier;

import org.easymock.EasyMock;
import org.gradle.api.GradleException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.robotframework.RobotRunner;
import org.theotherorg.poros.robot.RobotRunnerBridge.IsolationRunnable;

public class IsolationRunnableTest {
	private interface RobotSupplier extends Supplier<RobotRunner> {
	}

	@Test
	void testRun() {
		final Supplier<RobotRunner> mockedSupplier = EasyMock.createStrictMock(RobotSupplier.class);
		final RobotRunner mockedRunner = EasyMock.createStrictMock(RobotRunner.class);
		final IsolationRunnable testRunnable = new IsolationRunnable(mockedSupplier, "arg");

		EasyMock.expect(mockedSupplier.get()).andReturn(mockedRunner).once();
		EasyMock.expect(mockedRunner.run(new String[] {"arg"})).andReturn(0).once();
		mockedRunner.close();
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedSupplier, mockedRunner);
		testRunnable.run();
		EasyMock.verify(mockedSupplier, mockedRunner);
	}

	@Test
	void testRunFailure() {
		final Supplier<RobotRunner> mockedSupplier = EasyMock.createStrictMock(RobotSupplier.class);
		final RobotRunner mockedRunner = EasyMock.createStrictMock(RobotRunner.class);
		final IsolationRunnable testRunnable = new IsolationRunnable(mockedSupplier, "arg");

		EasyMock.expect(mockedSupplier.get()).andReturn(mockedRunner).once();
		EasyMock.expect(mockedRunner.run(new String[] {"arg"})).andReturn(255).once();
		mockedRunner.close();
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedSupplier, mockedRunner);
		Assertions.assertThrows(GradleException.class, testRunnable::run);
		EasyMock.verify(mockedSupplier, mockedRunner);
	}

	@Test
	void testRunNoArgs() {
		new IsolationRunnable(Collections.emptyList()).run();
	}
}
