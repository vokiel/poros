package org.theotherorg.poros.robot;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotRunProtoTest {
	private RobotRunProto testRun;

	@BeforeEach
	void setUp() {
		testRun = new RobotRunProto();
	}

	@Test
	void testGetConsoleMarkers() {
		Assertions.assertNull(testRun.getConsoleMarkers());
	}

	@Test
	void testGetConsoleType() {
		Assertions.assertNull(testRun.getConsoleType());
	}

	@Test
	void testGetConsoleWidth() {
		Assertions.assertEquals(0, testRun.getConsoleWidth());
	}

	@Test
	void testGetDebugFile() {
		Assertions.assertNull(testRun.getDebugFile());
	}

	@Test
	void testGetExtensions() {
		Assertions.assertNull(testRun.getExtensions());
	}

	@Test
	void testGetListeners() {
		Assertions.assertNull(testRun.getListeners());
	}

	@Test
	void testGetMaxErrorLines() {
		Assertions.assertEquals(0, testRun.getMaxErrorLines());
	}

	@Test
	void testGetPreRunModifiers() {
		Assertions.assertNull(testRun.getPreRunModifiers());
	}

	@Test
	void testGetRandomize() {
		Assertions.assertNull(testRun.getRandomize());
	}

	@Test
	void testGetRerunFailed() {
		Assertions.assertNull(testRun.getRerunFailed());
	}

	@Test
	void testGetRerunFailedSuites() {
		Assertions.assertNull(testRun.getRerunFailedSuites());
	}

	@Test
	void testGetVariableFiles() {
		Assertions.assertNull(testRun.getVariableFiles());
	}

	@Test
	void testGetVariables() {
		Assertions.assertNull(testRun.getVariables());
	}

	@Test
	void testIsDryRun() {
		Assertions.assertFalse(testRun.isDryRun());
	}

	@Test
	void testIsExitOnError() {
		Assertions.assertFalse(testRun.isExitOnError());
	}

	@Test
	void testIsExitOnFailure() {
		Assertions.assertFalse(testRun.isExitOnFailure());
	}

	@Test
	void testIsRunEmptySuite() {
		Assertions.assertFalse(testRun.isRunEmptySuite());
	}

	@Test
	void testIsSkipTearDownOnExit() {
		Assertions.assertFalse(testRun.isSkipTearDownOnExit());
	}

	@Test
	void testSetConsoleMarkers() {
		final String expectedMarker = "marker";

		testRun.setConsoleMarkers(expectedMarker);
		Assertions.assertEquals(expectedMarker, testRun.getConsoleMarkers());
	}

	@Test
	void testSetConsoleType() {
		final String expectedType = "type";

		testRun.setConsoleType(expectedType);
		Assertions.assertEquals(expectedType, testRun.getConsoleType());
	}

	@Test
	void testSetConsoleWidth() {
		final int expectedWidth = 37;

		testRun.setConsoleWidth(expectedWidth);
		Assertions.assertEquals(expectedWidth, testRun.getConsoleWidth());
	}

	@Test
	void testSetDebugFile() {
		final File mockedDebug = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedDebug);
		testRun.setDebugFile(mockedDebug);
		Assertions.assertSame(mockedDebug, testRun.getDebugFile());
		EasyMock.verify(mockedDebug);
	}

	@Test
	void testSetDryRun() {
		testRun.setDryRun(true);
		Assertions.assertTrue(testRun.isDryRun());
	}

	@Test
	void testSetExitOnError() {
		testRun.setExitOnError(true);
		Assertions.assertTrue(testRun.isExitOnError());
	}

	@Test
	void testSetExitOnFailure() {
		testRun.setExitOnFailure(true);
		Assertions.assertTrue(testRun.isExitOnFailure());
	}

	@Test
	void testSetExtensions() {
		final List<String> expectedExtensions = new ArrayList<>();
		final List<String> testExtensions = new ArrayList<>();

		expectedExtensions.add("extension");
		testExtensions.add("extension");
		testRun.setExtensions(testExtensions);
		Assertions.assertEquals(expectedExtensions, testRun.getExtensions());
	}

	@Test
	void testSetListeners() {
		final List<String> expectedListeners = new ArrayList<>();
		final List<String> testListeners = new ArrayList<>();

		expectedListeners.add("listener");
		testListeners.add("listener");
		testRun.setListeners(testListeners);
		Assertions.assertEquals(expectedListeners, testRun.getListeners());
	}

	@Test
	void testSetMaxErrorLines() {
		final int expectedMax = 37;

		testRun.setMaxErrorLines(expectedMax);
		Assertions.assertEquals(expectedMax, testRun.getMaxErrorLines());
	}

	@Test
	void testSetPreRunModifiers() {
		final List<String> expectedModifiers = new ArrayList<>();
		final List<String> testModifiers = new ArrayList<>();

		expectedModifiers.add("modifier");
		testModifiers.add("modifier");
		testRun.setPreRunModifiers(testModifiers);
		Assertions.assertEquals(expectedModifiers, testRun.getPreRunModifiers());
	}

	@Test
	void testSetRandomize() {
		final String expectedRandomize = "random";

		testRun.setRandomize(expectedRandomize);
		Assertions.assertEquals(expectedRandomize, testRun.getRandomize());
	}

	@Test
	void testSetRerunFailed() {
		final File mockedFailed = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedFailed);
		testRun.setRerunFailed(mockedFailed);
		Assertions.assertSame(mockedFailed, testRun.getRerunFailed());
		EasyMock.verify(mockedFailed);
	}

	@Test
	void testSetRerunFailedSuites() {
		final File mockedSuites = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedSuites);
		testRun.setRerunFailedSuites(mockedSuites);
		Assertions.assertSame(mockedSuites, testRun.getRerunFailedSuites());
		EasyMock.verify(mockedSuites);
	}

	@Test
	void testSetRunEmptySuite() {
		testRun.setRunEmptySuite(true);
		Assertions.assertTrue(testRun.isRunEmptySuite());
	}

	@Test
	void testSetSkipTearDownOnExit() {
		testRun.setSkipTearDownOnExit(true);
		Assertions.assertTrue(testRun.isSkipTearDownOnExit());
	}

	@Test
	void testSetVariableFiles() {
		final List<String> expectedFiles = new ArrayList<>();
		final List<String> testFiles = new ArrayList<>();

		expectedFiles.add("variables");
		testFiles.add("variables");
		testRun.setVariableFiles(testFiles);
		Assertions.assertEquals(expectedFiles, testRun.getVariableFiles());
	}

	@Test
	void testSetVariables() {
		final Map<String, String> expectedVars = new HashMap<>();
		final Map<String, String> testVars = new HashMap<>();

		expectedVars.put("key", "variable");
		testVars.put("key", "variable");
		testRun.setVariables(testVars);
		Assertions.assertEquals(expectedVars, testRun.getVariables());
	}
}
