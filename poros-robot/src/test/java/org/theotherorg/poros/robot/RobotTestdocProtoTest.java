package org.theotherorg.poros.robot;

import java.io.File;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotTestdocProtoTest {
	private RobotTestdocProto testTestdoc;

	@BeforeEach
	void setUp() {
		testTestdoc = new RobotTestdocProto();
	}

	@Test
	void testGetOutputFile() {
		Assertions.assertNull(testTestdoc.getOutputFile());
	}

	@Test
	void testGetPythonPaths() {
		Assertions.assertNull(testTestdoc.getPythonPaths());
	}

	@Test
	void testGetTitle() {
		Assertions.assertNull(testTestdoc.getTitle());
	}

	@Test
	void testSetOutputFile() {
		final File mockedOutput = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedOutput);
		testTestdoc.setOutputFile(mockedOutput);
		Assertions.assertSame(mockedOutput, testTestdoc.getOutputFile());
		EasyMock.verify(mockedOutput);
	}

	@Test
	void testSetTitle() {
		final String expectedTitle = "title";

		testTestdoc.setTitle(expectedTitle);
		Assertions.assertEquals(expectedTitle, testTestdoc.getTitle());
	}
}
