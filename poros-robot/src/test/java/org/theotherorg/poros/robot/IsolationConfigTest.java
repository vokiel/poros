package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.easymock.EasyMock;
import org.gradle.workers.ForkMode;
import org.gradle.workers.IsolationMode;
import org.gradle.workers.WorkerConfiguration;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.IsolationConfig;

public class IsolationConfigTest {
	@Test
	void testExecute() {
		final WorkerConfiguration mockedConfig = EasyMock.createStrictMock(WorkerConfiguration.class);
		final List<File> expectedClasspath = Collections.emptyList();
		final String expectedArgs = "args";
		final String expectedCommand = "command";
		final IsolationConfig testConfig = new IsolationConfig(expectedClasspath, expectedCommand, expectedArgs);

		mockedConfig.setForkMode(ForkMode.NEVER);
		EasyMock.expectLastCall().once();
		mockedConfig.setIsolationMode(IsolationMode.CLASSLOADER);
		EasyMock.expectLastCall().once();
		mockedConfig.setParams(Arrays.asList(expectedCommand, expectedArgs));
		EasyMock.expectLastCall().once();
		mockedConfig.setClasspath(expectedClasspath);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedConfig);
		testConfig.execute(mockedConfig);
		EasyMock.verify(mockedConfig);
	}
}
