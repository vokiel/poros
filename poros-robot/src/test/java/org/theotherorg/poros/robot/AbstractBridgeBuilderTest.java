package org.theotherorg.poros.robot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.AbstractBuilder;

public class AbstractBridgeBuilderTest {
	private static final class TestBridgeBuilder extends AbstractBuilder {
	}

	private AbstractBuilder testBuilder;

	@BeforeEach
	void setUp() {
		testBuilder = new TestBridgeBuilder();
	}

	@Test
	void testAddFlag() {
		final String expectedFlag = "flag";
		final List<String> testOptions;

		testBuilder.addFlag(expectedFlag);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals(expectedFlag, testOptions.get(0));
	}

	@Test
	void testAddMap() {
		final String expectedArg = "arg";
		final String expectedKey = "key";
		final String expectedValue = "value";
		final Map<String, String> expectedValues = new HashMap<>();
		final List<String> testOptions;

		expectedValues.put(expectedKey, expectedValue);
		testBuilder.addMap(expectedArg, expectedValues);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertEquals(expectedArg, testOptions.get(0));
		Assertions.assertEquals(expectedKey + ":" + expectedValue, testOptions.get(1));
	}

	@Test
	void testAddOption() {
		final String expectedArg = "arg";
		final String expectedValue = "value";
		final List<String> testOptions;

		testBuilder.addOption(expectedArg, expectedValue);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertEquals(expectedArg, testOptions.get(0));
		Assertions.assertEquals(expectedValue, testOptions.get(1));
	}

	@Test
	void testAddOptions() {
		final String expectedArg = "arg";
		final String expectedValue = "value";
		final List<String> expectedValues = new ArrayList<>();
		final List<String> testOptions;

		expectedValues.add(expectedValue);
		testBuilder.addOptions(expectedArg, expectedValues);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertEquals(expectedArg, testOptions.get(0));
		Assertions.assertEquals(expectedValue, testOptions.get(1));
	}

	@Test
	void testGetOptions() {
		final List<String> testOptions = testBuilder.getOptions();

		Assertions.assertNotNull(testOptions);
		Assertions.assertTrue(testOptions.isEmpty());
	}
}
