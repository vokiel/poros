package org.theotherorg.poros.robot;

import javax.inject.Inject;

import org.easymock.EasyMock;
import org.gradle.api.file.FileCollection;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AbstractRobotTaskTest {
	static class TestTask extends AbstractRobotTask<Object> {
		@Inject
		public TestTask() {
		}
	}

	private RobotRunnerBridge mockedBridge;
	private TestTask testTask;

	@BeforeEach
	void setUp() {
		mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		testTask = ProjectBuilder.builder().build().getTasks().create("testAbstract", TestTask.class);
	}

	@Test
	void testGetLibraryClasspath() {
		EasyMock.replay(mockedBridge);
		Assertions.assertNull(testTask.getLibraryClasspath());
		EasyMock.verify(mockedBridge);
	}

	@Test
	void testGetRobotProto() {
		EasyMock.replay(mockedBridge);
		Assertions.assertNull(testTask.getRobotProto());
		EasyMock.verify(mockedBridge);
	}

	@Test
	void testSetLibraryClasspath() {
		final FileCollection mockedClasspath = EasyMock.createStrictMock(FileCollection.class);

		EasyMock.replay(mockedBridge, mockedClasspath);
		testTask.setLibraryClasspath(mockedClasspath);
		Assertions.assertSame(mockedClasspath, testTask.getLibraryClasspath());
		EasyMock.verify(mockedBridge, mockedClasspath);
	}

	@Test
	void testSetRobotProto() {
		final Object mockedProto = EasyMock.createStrictMock(Object.class);

		EasyMock.replay(mockedBridge, mockedProto);
		testTask.setRobotProto(mockedProto);
		Assertions.assertSame(mockedProto, testTask.getRobotProto());
		EasyMock.verify(mockedBridge, mockedProto);
	}
}
