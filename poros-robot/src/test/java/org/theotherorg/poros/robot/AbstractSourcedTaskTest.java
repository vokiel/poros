package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Collections;
import java.util.Set;

import javax.inject.Inject;

import org.easymock.EasyMock;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AbstractSourcedTaskTest {
	static class TestTask extends AbstractSourcedTask<Object> {
		@Inject
		public TestTask() {
		}
	}

	private RobotRunnerBridge mockedBridge;
	private AbstractSourcedTask<Object> testTask;

	@BeforeEach
	void setUp() {
		mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		testTask = ProjectBuilder.builder().build().getTasks().create("testAbstract", TestTask.class);
	}

	@Test
	void testGetSourceFiles() {
		EasyMock.replay(mockedBridge);
		Assertions.assertNull(testTask.getSourceFiles());
		EasyMock.verify(mockedBridge);
	}

	@Test
	void testSetSourceFiles() {
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = Collections.singleton(mockedFile);

		EasyMock.replay(mockedBridge, mockedFile);
		testTask.setSourceFiles(testFiles);
		Assertions.assertEquals(1, testTask.getSourceFiles().size());
		Assertions.assertSame(mockedFile, testTask.getSourceFiles().iterator().next());
		EasyMock.verify(mockedBridge, mockedFile);
	}
}
