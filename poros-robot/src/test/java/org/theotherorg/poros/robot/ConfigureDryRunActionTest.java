package org.theotherorg.poros.robot;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import org.easymock.EasyMock;
import org.gradle.api.Project;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskContainer;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureDryRunAction;

public class ConfigureDryRunActionTest {
	private interface ClasspathSupplier extends Supplier<Iterable<File>> {
	}

	private interface SourceSetSupplier extends Supplier<SourceSet> {
	}

	@Test
	void testExecute() {
		final Supplier<Iterable<File>> mockedClasspath = EasyMock.createStrictMock(ClasspathSupplier.class);
		final Supplier<SourceSet> mockedSources = EasyMock.createStrictMock(SourceSetSupplier.class);
		final RobotRunProto mockedProto = EasyMock.createStrictMock(RobotRunProto.class);
		final RobotRunnerBridge mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		final RobotRunTask mockedTask = EasyMock.createStrictMock(RobotRunTask.class);
		final Project mockedProject = EasyMock.createStrictMock(Project.class);
		final TaskContainer mockedTasks = EasyMock.createStrictMock(TaskContainer.class);
		final SourceSet mockedSourceSet = EasyMock.createStrictMock(SourceSet.class);
		final SourceDirectorySet mockedDirSet = EasyMock.createStrictMock(SourceDirectorySet.class);
		final Set<File> expectedClasspath = new HashSet<>();
		final Set<File> expectedSourceFiles = new HashSet<>();
		final ConfigureDryRunAction testAction;

		EasyMock.expect(mockedTask.getProject()).andReturn(mockedProject).once();
		EasyMock.expect(mockedProject.getTasks()).andReturn(mockedTasks).once();
		EasyMock.expect(mockedTasks.findByName(EasyMock.anyString())).andReturn(mockedTask).once();
		EasyMock.expect(mockedTask.dependsOn(mockedTask)).andReturn(mockedTask);
		mockedTask.setDescription(EasyMock.anyString());
		EasyMock.expectLastCall().once();
		mockedProto.setDryRun(true);
		EasyMock.expectLastCall().once();
		mockedTask.setRobotProto(mockedProto);
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedSources.get()).andReturn(mockedSourceSet).once();
		EasyMock.expect(mockedSourceSet.getResources()).andReturn(mockedDirSet).once();
		EasyMock.expect(mockedDirSet.getFiles()).andReturn(expectedSourceFiles).once();
		mockedTask.setSourceFiles(expectedSourceFiles);
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedClasspath.get()).andReturn(expectedClasspath).once();
		mockedTask.setLibraryClasspath(expectedClasspath);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedTask, mockedProject, mockedTasks, mockedClasspath, mockedSources, mockedProto,
			mockedBridge, mockedSourceSet, mockedDirSet);
		testAction = new ConfigureDryRunAction(mockedClasspath, mockedSources, mockedProto);
		testAction.execute(mockedTask);
		EasyMock.verify(mockedTask, mockedProject, mockedTasks, mockedClasspath, mockedSources, mockedProto,
			mockedBridge, mockedSourceSet, mockedDirSet);
	}
}
