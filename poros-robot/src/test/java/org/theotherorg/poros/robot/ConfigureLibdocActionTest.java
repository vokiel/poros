package org.theotherorg.poros.robot;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import org.easymock.EasyMock;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.tasks.SourceSet;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureLibdocAction;

public class ConfigureLibdocActionTest {
	private interface ClasspathSupplier extends Supplier<Iterable<File>> {
	}

	private interface SourceSetSupplier extends Supplier<SourceSet> {
	}

	@Test
	void testExecute() {
		final Supplier<Iterable<File>> mockedClasspath = EasyMock.createStrictMock(ClasspathSupplier.class);
		final Supplier<SourceSet> mockedSourceSetSupplier = EasyMock.createStrictMock(SourceSetSupplier.class);
		final SourceSet mockedSourceSet = EasyMock.createStrictMock(SourceSet.class);
		final RobotLibdocProto mockedProto = EasyMock.createStrictMock(RobotLibdocProto.class);
		final RobotRunnerBridge mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		final RobotLibdocTask testTask = ProjectBuilder.builder().build().getTasks().create("testLibdoc", RobotLibdocTask.class, mockedBridge);
		final SourceDirectorySet mockedDirSet = EasyMock.createStrictMock(SourceDirectorySet.class);
		final Set<File> expectedClasspath = new HashSet<>();
		final ConfigureLibdocAction testAction;

		EasyMock.expect(mockedClasspath.get()).andReturn(expectedClasspath).once();
		EasyMock.expect(mockedSourceSetSupplier.get()).andReturn(mockedSourceSet).once();
		EasyMock.expect(mockedSourceSet.getAllJava()).andReturn(mockedDirSet).once();
		EasyMock.expect(mockedDirSet.getFiles()).andReturn(new HashSet<>());
		EasyMock.replay(mockedClasspath, mockedSourceSetSupplier, mockedSourceSet, mockedProto, mockedBridge, mockedDirSet);
		testAction = new ConfigureLibdocAction(mockedClasspath, mockedSourceSetSupplier, mockedProto);
		testAction.execute(testTask);
		Assertions.assertNotNull(testTask.getDescription());
		Assertions.assertSame(mockedProto, testTask.getRobotProto());
		Assertions.assertSame(expectedClasspath, testTask.getLibraryClasspath());
		Assertions.assertNotNull(testTask.getLibraryFiles());
		Assertions.assertTrue(testTask.getLibraryFiles().isEmpty());
		EasyMock.verify(mockedClasspath, mockedSourceSetSupplier, mockedSourceSet, mockedProto, mockedBridge, mockedDirSet);
	}
}
