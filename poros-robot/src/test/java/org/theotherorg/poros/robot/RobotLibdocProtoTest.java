package org.theotherorg.poros.robot;

import java.io.File;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotLibdocProtoTest {
	private RobotLibdocProto testLibdoc;

	@BeforeEach
	void setUp() {
		testLibdoc = new RobotLibdocProto();
	}

	@Test
	void testGetDocFormat() {
		Assertions.assertNull(testLibdoc.getDocFormat());
	}

	@Test
	void testGetFormat() {
		Assertions.assertNull(testLibdoc.getFormat());
	}

	@Test
	void testGetOutputDir() {
		Assertions.assertNull(testLibdoc.getOutputDir());
	}

	@Test
	void testGetVersion() {
		Assertions.assertNull(testLibdoc.getVersion());
	}

	@Test
	void testSetDocFormat() {
		final String expectedFormat = "format";

		testLibdoc.setDocFormat(expectedFormat);
		Assertions.assertEquals(expectedFormat, testLibdoc.getDocFormat());
	}

	@Test
	void testSetFormat() {
		final String expectedFormat = "format";

		testLibdoc.setFormat(expectedFormat);
		Assertions.assertEquals(expectedFormat, testLibdoc.getFormat());
	}

	@Test
	void testSetOutputDir() {
		final File mockedFile = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedFile);
		testLibdoc.setOutputDir(mockedFile);
		Assertions.assertSame(mockedFile, testLibdoc.getOutputDir());
		EasyMock.verify(mockedFile);
	}

	@Test
	void testSetVersion() {
		final String expectedVersion = "version";

		testLibdoc.setVersion(expectedVersion);
		Assertions.assertEquals(expectedVersion, testLibdoc.getVersion());
	}
}
