package org.theotherorg.poros.robot;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.TidyBuilder;

public class TidyBridgeBuilderTest {
	private TidyBuilder testBuilder;

	@BeforeEach
	void setUp() {
		this.testBuilder = new TidyBuilder();
	}

	@Test
	void testAddFormat() {
		final String expectedFormat = "format";
		final List<String> testOptions;

		testBuilder.addFormat(expectedFormat);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedFormat, testOptions.get(1));
	}

	@Test
	void testAddInPlace() {
		final List<String> testOptions;

		testBuilder.addInPlace();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("-i", testOptions.get(0));
	}

	@Test
	void testAddLineSeparator() {
		final String expectedSeparator = "separator";
		final List<String> testOptions;

		testBuilder.addLineSeparator(expectedSeparator);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedSeparator, testOptions.get(1));
	}

	@Test
	void testAddRecursive() {
		final List<String> testOptions;

		testBuilder.addRecursive();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("-r", testOptions.get(0));
	}

	@Test
	void testAddSpaceCounts() {
		final int expectedCounts = 37;
		final List<String> testOptions;

		testBuilder.addSpaceCounts(expectedCounts);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(String.valueOf(expectedCounts), testOptions.get(1));
	}

	@Test
	void testAddUsePipes() {
		final List<String> testOptions;

		testBuilder.addUsePipes();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("-p", testOptions.get(0));
	}

	@Test
	void testFromAll() {
		final RobotTidyProto mockedTidy = EasyMock.createStrictMock(RobotTidyProto.class);
		final String expectedFormat = "robot";
		final String expectedLineSep = "unix";
		final int expectedSpaceCounts = 37;
		final TidyBuilder testBuilder;
		final Set<String> testOptions;

		EasyMock.expect(mockedTidy.getFormat()).andReturn(expectedFormat).once();
		EasyMock.expect(mockedTidy.getLineSeparator()).andReturn(expectedLineSep).once();
		EasyMock.expect(mockedTidy.getSpaceCount()).andReturn(expectedSpaceCounts).once();
		EasyMock.expect(mockedTidy.isUsePipes()).andReturn(true).once();
		EasyMock.replay(mockedTidy);
		testBuilder = TidyBuilder.from(mockedTidy, true);
		Assertions.assertNotNull(testBuilder);
		testOptions = new HashSet<>(testBuilder.getOptions());
		Assertions.assertNotNull(testOptions);
		Assertions.assertTrue(testOptions.contains(expectedFormat));
		Assertions.assertTrue(testOptions.contains(expectedLineSep));
		Assertions.assertTrue(testOptions.contains(String.valueOf(expectedSpaceCounts)));
		EasyMock.verify(mockedTidy);
	}

	@Test
	void testFromNothing() {
		final RobotTidyProto mockedTidy = EasyMock.createStrictMock(RobotTidyProto.class);
		final TidyBuilder testBuilder;
		final List<String> testOptions;

		EasyMock.expect(mockedTidy.getFormat()).andReturn(null).once();
		EasyMock.expect(mockedTidy.getLineSeparator()).andReturn(null).once();
		EasyMock.expect(mockedTidy.getSpaceCount()).andReturn(0).once();
		EasyMock.expect(mockedTidy.isUsePipes()).andReturn(false).once();
		EasyMock.replay(mockedTidy);
		testBuilder = TidyBuilder.from(mockedTidy, false);
		Assertions.assertNotNull(testBuilder);
		testOptions = testBuilder.getOptions();
		Assertions.assertTrue(testOptions.isEmpty());
		EasyMock.verify(mockedTidy);
	}

	@Test
	void testToArgs() {
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> expectedFiles = new HashSet<>();
		final String expectedPath = "path";
		final String[] testArgs;

		expectedFiles.add(mockedFile);
		EasyMock.expect(mockedFile.getPath()).andReturn(expectedPath).once();
		EasyMock.replay(mockedFile);
		testArgs = testBuilder.toArgs(expectedFiles);
		Assertions.assertNotNull(testArgs);
		Assertions.assertEquals(2, testArgs.length);
		Assertions.assertEquals(expectedPath, testArgs[1]);
		EasyMock.verify(mockedFile);
	}

	@Test
	void testToArgsNoData() {
		final String[] testArgs = testBuilder.toArgs(null);

		Assertions.assertNotNull(testArgs);
		Assertions.assertEquals(1, testArgs.length);
	}
}
