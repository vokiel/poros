package org.theotherorg.poros.robot;

import java.io.File;
import java.util.Collections;
import java.util.Set;

import org.easymock.EasyMock;
import org.gradle.api.file.FileCollection;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotRunTaskTest {
	private RobotRunnerBridge mockedBridge;
	private RobotRunTask testTask;

	@BeforeEach
	void setUp() {
		mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		testTask = ProjectBuilder.builder().build().getTasks().create("testRun", RobotRunTask.class, mockedBridge);
	}

	@Test
	void testGetLibraryClasspath() {
		EasyMock.replay(mockedBridge);
		Assertions.assertNull(testTask.getLibraryClasspath());
		EasyMock.verify(mockedBridge);
	}

	@Test
	void testGetRunTimestamp() {
		Assertions.assertNull(testTask.getRunTimestamp());
	}

	@Test
	void testGetSourceFiles() {
		EasyMock.replay(mockedBridge);
		Assertions.assertNull(testTask.getSourceFiles());
		EasyMock.verify(mockedBridge);
	}

	@Test
	void testRobotDryRun() {
		final FileCollection mockedClasspath = EasyMock.createStrictMock(FileCollection.class);
		final RobotRunProto mockedProto = EasyMock.createStrictMock(RobotRunProto.class);
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = Collections.singleton(mockedFile);

		testTask.setLibraryClasspath(mockedClasspath);
		testTask.setRobotProto(mockedProto);
		testTask.setSourceFiles(testFiles);
		EasyMock.expect(mockedProto.isDryRun()).andReturn(true).once();
		mockedBridge.run(mockedProto, testFiles, mockedClasspath);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBridge, mockedClasspath, mockedProto, mockedFile);
		testTask.robotRun();
		EasyMock.verify(mockedBridge, mockedClasspath, mockedProto, mockedFile);
	}

	@Test
	void testRobotRun() {
		final FileCollection mockedClasspath = EasyMock.createStrictMock(FileCollection.class);
		final RobotRunProto mockedProto = EasyMock.createStrictMock(RobotRunProto.class);
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = Collections.singleton(mockedFile);

		testTask.setLibraryClasspath(mockedClasspath);
		testTask.setRobotProto(mockedProto);
		testTask.setSourceFiles(testFiles);
		EasyMock.expect(mockedProto.isDryRun()).andReturn(false).once();
		mockedBridge.run(mockedProto, testFiles, mockedClasspath);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBridge, mockedClasspath, mockedProto, mockedFile);
		testTask.robotRun();
		EasyMock.verify(mockedBridge, mockedClasspath, mockedProto, mockedFile);
	}

	@Test
	void testSetLibraryClasspath() {
		final FileCollection mockedClasspath = EasyMock.createStrictMock(FileCollection.class);

		EasyMock.replay(mockedBridge, mockedClasspath);
		testTask.setLibraryClasspath(mockedClasspath);
		Assertions.assertSame(mockedClasspath, testTask.getLibraryClasspath());
		EasyMock.verify(mockedBridge, mockedClasspath);
	}

	@Test
	void testSetSourceFiles() {
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = Collections.singleton(mockedFile);

		EasyMock.replay(mockedBridge, mockedFile);
		testTask.setSourceFiles(testFiles);
		Assertions.assertEquals(1, testTask.getSourceFiles().size());
		Assertions.assertSame(mockedFile, testTask.getSourceFiles().iterator().next());
		EasyMock.verify(mockedBridge, mockedFile);
	}
}
