package org.theotherorg.poros.robot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.BaseBuilder;
import org.theotherorg.poros.robot.RobotRunnerBridge.BaseBuilder.HasBase;

public class BaseBridgeBuilterTest {
	private BaseBuilder testBuilder;

	@BeforeEach
	void setUp() {
		testBuilder = new BaseBuilder();
	}

	@Test
	void testAddArgumentFiles() {
		final List<String> expectedFiles = new ArrayList<>();
		final String expectedFile = "file";
		final List<String> testOptions;

		expectedFiles.add(expectedFile);
		testBuilder.addArgumentFiles(expectedFiles);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedFile, testOptions.get(1));
	}

	@Test
	void testAddBase() {
		final HasBase mockedBase = EasyMock.createStrictMock(HasBase.class);
		final Map<String, String> metadata = new HashMap<>();
		final List<String> expectedTags = Collections.singletonList("tag");
		final List<String> expectedTests = Collections.singletonList("test");
		final List<String> expectedSuites = Collections.singletonList("suite");
		final List<String> expectedIncludes = Collections.singletonList("include");
		final List<String> expectedExcludes = Collections.singletonList("exclude");
		final List<String> expectedPythonPaths = Collections.singletonList("path");
		final List<String> expectedArgumentFiles = Collections.singletonList("file");
		final String expectedKey = "key";
		final String expectedValue = "value";
		final String expectedName = "name";
		final String expectedDoc = "doc";
		final Set<String> testOptions;

		metadata.put(expectedKey, expectedValue);
		EasyMock.expect(mockedBase.getName()).andReturn(expectedName).once();
		EasyMock.expect(mockedBase.getDoc()).andReturn(expectedDoc).once();
		EasyMock.expect(mockedBase.getMetadata()).andReturn(metadata).once();
		EasyMock.expect(mockedBase.getTags()).andReturn(expectedTags).once();
		EasyMock.expect(mockedBase.getTests()).andReturn(expectedTests).once();
		EasyMock.expect(mockedBase.getSuites()).andReturn(expectedSuites).once();
		EasyMock.expect(mockedBase.getIncludes()).andReturn(expectedIncludes).once();
		EasyMock.expect(mockedBase.getExcludes()).andReturn(expectedExcludes).once();
		EasyMock.expect(mockedBase.getPythonPaths()).andReturn(expectedPythonPaths).once();
		EasyMock.expect(mockedBase.getArgumentFiles()).andReturn(expectedArgumentFiles).once();
		EasyMock.replay(mockedBase);
		testBuilder.addBase(mockedBase);
		testOptions = new HashSet<>(testBuilder.getOptions());
		Assertions.assertNotNull(testOptions);
		Assertions.assertTrue(testOptions.contains(expectedName));
		Assertions.assertTrue(testOptions.contains(expectedDoc));
		Assertions.assertTrue(testOptions.contains(expectedKey + ":" + expectedValue));
		Assertions.assertTrue(testOptions.contains(expectedTags.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedTests.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedSuites.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedIncludes.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedExcludes.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedPythonPaths.get(0)));
		Assertions.assertTrue(testOptions.contains(expectedArgumentFiles.get(0)));
		EasyMock.verify(mockedBase);
	}

	@Test
	void testAddDoc() {
		final String expectedDoc = "doc";
		final List<String> testOptions;

		testBuilder.addDoc(expectedDoc);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedDoc, testOptions.get(1));
	}

	@Test
	void testAddExcludes() {
		final List<String> expectedExcludes = new ArrayList<>();
		final String expectedExclude = "exclude";
		final List<String> testOptions;

		expectedExcludes.add(expectedExclude);
		testBuilder.addExcludes(expectedExcludes);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedExclude, testOptions.get(1));
	}

	@Test
	void testAddIncludes() {
		final List<String> expectedIncludes = new ArrayList<>();
		final String expectedInclude = "include";
		final List<String> testOptions;

		expectedIncludes.add(expectedInclude);
		testBuilder.addIncludes(expectedIncludes);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedInclude, testOptions.get(1));
	}

	@Test
	void testAddMetadata() {
		final Map<String, String> expectedMetadata = new HashMap<>();
		final String expectedKey = "key";
		final String expectedValue = "value";
		final List<String> testOptions;

		expectedMetadata.put(expectedKey, expectedValue);
		testBuilder.addMetadata(expectedMetadata);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedKey + ":" + expectedValue, testOptions.get(1));
	}

	@Test
	void testAddName() {
		final String expectedName = "name";
		final List<String> testOptions;

		testBuilder.addName(expectedName);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedName, testOptions.get(1));
	}

	@Test
	void testAddPythonPaths() {
		final List<String> expectedPaths = new ArrayList<>();
		final String expectedPath = "path";
		final List<String> testOptions;

		expectedPaths.add(expectedPath);
		testBuilder.addPythonPaths(expectedPaths);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedPath, testOptions.get(1));
	}

	@Test
	void testAddSuites() {
		final List<String> expectedSuites = new ArrayList<>();
		final String expectedSuite = "suite";
		final List<String> testOptions;

		expectedSuites.add(expectedSuite);
		testBuilder.addSuites(expectedSuites);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedSuite, testOptions.get(1));
	}

	@Test
	void testAddTags() {
		final List<String> expectedTags = new ArrayList<>();
		final String expectedTag = "tag";
		final List<String> testOptions;

		expectedTags.add(expectedTag);
		testBuilder.addTags(expectedTags);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedTag, testOptions.get(1));
	}

	@Test
	void testAddTests() {
		final List<String> expectedTests = new ArrayList<>();
		final String expectedTest = "test";
		final List<String> testOptions;

		expectedTests.add(expectedTest);
		testBuilder.addTests(expectedTests);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedTest, testOptions.get(1));
	}

	@Test
	void testToArgs() {
		final String expectedName = "name";
		final String[] expectedArgs = new String[] {"-N", expectedName};

		testBuilder.addName(expectedName);
		Assertions.assertArrayEquals(expectedArgs, testBuilder.toArgs());
	}
}
