package org.theotherorg.poros.robot;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import org.easymock.EasyMock;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureRebotAction;

public class ConfigureRebotActionTest {
	private interface ClasspathSupplier extends Supplier<Iterable<File>> {
	}

	@Test
	void testExecute() {
		final ClasspathSupplier mockedClasspath = EasyMock.createStrictMock(ClasspathSupplier.class);
		final RobotRebotProto mockedProto = EasyMock.createStrictMock(RobotRebotProto.class);
		final RobotRunnerBridge mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		final RobotRebotTask testTask = ProjectBuilder.builder().build().getTasks().create("testRebot", RobotRebotTask.class, mockedBridge);
		final Set<File> expectedClasspath = new HashSet<>();
		final ConfigureRebotAction testAction;

		EasyMock.expect(mockedClasspath.get()).andReturn(expectedClasspath).once();
		EasyMock.replay(mockedClasspath, mockedProto, mockedBridge);
		testAction = new ConfigureRebotAction(mockedClasspath, mockedProto);
		testAction.execute(testTask);
		Assertions.assertNotNull(testTask.getDescription());
		Assertions.assertSame(mockedProto, testTask.getRobotProto());
		Assertions.assertSame(expectedClasspath, testTask.getLibraryClasspath());
		EasyMock.verify(mockedClasspath, mockedProto, mockedBridge);
	}
}
