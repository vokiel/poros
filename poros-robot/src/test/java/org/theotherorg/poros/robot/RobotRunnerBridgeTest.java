package org.theotherorg.poros.robot;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.easymock.EasyMock;
import org.gradle.api.Action;
import org.gradle.workers.WorkerExecutor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.IsolationRunnable;

class RobotRunnerBridgeTest {
	private WorkerExecutor mockedExecutor;
	private RobotRunnerBridge testBridge;

	@BeforeEach
	void setUp() {
		mockedExecutor = EasyMock.createStrictMock(WorkerExecutor.class);
		testBridge = new RobotRunnerBridge(mockedExecutor);
	}

	@SuppressWarnings("unchecked")
	@Test
	void testLibDocHtml() {
		final RobotLibdocProto testLibdoc = new RobotLibdocProto();
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final File mockedOutput = EasyMock.createStrictMock(File.class);
		final File mockedLibrary = EasyMock.createStrictMock(File.class);
		final File mockedLibrary2 = EasyMock.createStrictMock(File.class);
		final Set<File> testLibraryFiles = new HashSet<>();
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);

		testLibraryFiles.add(mockedLibrary);
		testLibraryFiles.add(mockedLibrary2);
		testLibdoc.setOutputDir(mockedOutput);
		EasyMock.expect(mockedOutput.toPath()).andReturn(Paths.get("output")).once();
		EasyMock.expect(mockedLibrary.toPath()).andReturn(Paths.get("library")).once();
		EasyMock.expect(mockedLibrary.getPath()).andReturn("library").once();
		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedLibrary2.toPath()).andReturn(Paths.get("library.java")).once();
		EasyMock.expect(mockedLibrary2.getPath()).andReturn("library.java").once();
		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedExecutor, mockedClasspath, mockedOutput, mockedLibrary, mockedLibrary2);
		testBridge.libDoc(testLibdoc, testLibraryFiles, classpath);
		EasyMock.verify(mockedExecutor, mockedClasspath, mockedOutput, mockedLibrary, mockedLibrary2);
	}

	@Test
	void testLibDocNoLibs() {
		final RobotLibdocProto testLibdoc = new RobotLibdocProto();
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final File mockedOutput = EasyMock.createStrictMock(File.class);
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);

		testLibdoc.setOutputDir(mockedOutput);
		EasyMock.replay(mockedExecutor, mockedClasspath, mockedOutput);
		testBridge.libDoc(testLibdoc, null, classpath);
		EasyMock.verify(mockedExecutor, mockedClasspath, mockedOutput);
	}

	@Test
	void testLibDocNoOutput() {
		final RobotLibdocProto testLibdoc = new RobotLibdocProto();
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final File mockedLibrary = EasyMock.createStrictMock(File.class);
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);
		final Iterable<File> libraries = Collections.singletonList(mockedLibrary);

		EasyMock.replay(mockedExecutor, mockedClasspath, mockedLibrary);
		testBridge.libDoc(testLibdoc, libraries, classpath);
		EasyMock.verify(mockedExecutor, mockedClasspath, mockedLibrary);
	}

	@SuppressWarnings("unchecked")
	@Test
	void testLibDocXml() {
		final RobotLibdocProto testLibdoc = new RobotLibdocProto();
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final File mockedOutput = EasyMock.createStrictMock(File.class);
		final File mockedLibrary = EasyMock.createStrictMock(File.class);
		final File mockedLibrary2 = EasyMock.createStrictMock(File.class);
		final Set<File> testLibraryFiles = new HashSet<>();
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);

		testLibraryFiles.add(mockedLibrary);
		testLibraryFiles.add(mockedLibrary2);
		testLibdoc.setFormat("XML");
		testLibdoc.setOutputDir(mockedOutput);
		EasyMock.expect(mockedOutput.toPath()).andReturn(Paths.get("output")).once();
		EasyMock.expect(mockedLibrary.toPath()).andReturn(Paths.get("library")).once();
		EasyMock.expect(mockedLibrary.getPath()).andReturn("library").once();
		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedLibrary2.toPath()).andReturn(Paths.get("library.java")).once();
		EasyMock.expect(mockedLibrary2.getPath()).andReturn("library.java").once();
		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedExecutor, mockedClasspath, mockedOutput, mockedLibrary, mockedLibrary2);
		testBridge.libDoc(testLibdoc, testLibraryFiles, classpath);
		EasyMock.verify(mockedExecutor, mockedClasspath, mockedOutput, mockedLibrary, mockedLibrary2);
	}

	@SuppressWarnings("unchecked")
	@Test
	void testRebot() {
		final RobotRebotProto mockedRebot = new RobotRebotProto();
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);

		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedExecutor);
		testBridge.rebot(mockedRebot, classpath);
		EasyMock.verify(mockedExecutor);
	}

	@SuppressWarnings("unchecked")
	@Test
	void testRun() {
		final RobotRunProto testRun = new RobotRunProto();
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);

		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedExecutor);
		testBridge.run(testRun, null, classpath);
		EasyMock.verify(mockedExecutor);
	}

	@SuppressWarnings("unchecked")
	@Test
	void testTestDoc() {
		final RobotTestdocProto testTestDoc = new RobotTestdocProto();
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);

		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedExecutor);
		testBridge.testDoc(testTestDoc, null, classpath);
		EasyMock.verify(mockedExecutor);
	}

	@SuppressWarnings("unchecked")
	@Test
	void testTidy() {
		final RobotTidyProto testTidy = new RobotTidyProto();
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);
		final Iterable<File> dataFiles = Arrays.asList(mockedFile, mockedFile, mockedFile, mockedFile);
		final Path expectedNoExtension = Paths.get("noExtension");
		final Path expectedRobot = Paths.get("/path/file.robot");
		final Path expectedHtml = Paths.get("/path/file.html");
		final Path expectedDot = Paths.get("a.");

		testTidy.setFormat("robot");
		EasyMock.expect(mockedFile.toPath()).andReturn(expectedDot).once();
		EasyMock.expect(mockedFile.getPath()).andReturn(expectedDot.toString()).once();
		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedFile.toPath()).andReturn(expectedNoExtension).once();
		EasyMock.expect(mockedFile.getPath()).andReturn(expectedNoExtension.toString()).once();
		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedFile.toPath()).andReturn(expectedRobot).once();
		EasyMock.expect(mockedFile.getPath()).andReturn(expectedRobot.toString()).once();
		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedFile.toPath()).andReturn(expectedHtml).once();
		EasyMock.expect(mockedFile.getPath()).andReturn(expectedHtml.toString()).once();
		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedExecutor, mockedClasspath, mockedFile);
		testBridge.tidy(testTidy, dataFiles, classpath);
		EasyMock.verify(mockedExecutor, mockedClasspath, mockedFile);
	}

	@Test
	void testTidyNoData() {
		final RobotTidyProto mockedTidy = EasyMock.createStrictMock(RobotTidyProto.class);
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);

		EasyMock.expect(mockedTidy.getFormat()).andReturn("robot").once();
		EasyMock.replay(mockedExecutor, mockedClasspath, mockedTidy);
		testBridge.tidy(mockedTidy, null, classpath);
		EasyMock.verify(mockedExecutor, mockedClasspath, mockedTidy);
	}

	@SuppressWarnings("unchecked")
	@Test
	void testTidyNoFormat() {
		final RobotTidyProto testTidy = new RobotTidyProto();
		final File mockedClasspath = EasyMock.createStrictMock(File.class);
		final Iterable<File> classpath = Collections.singletonList(mockedClasspath);

		mockedExecutor.submit(EasyMock.eq(IsolationRunnable.class), EasyMock.anyObject(Action.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedExecutor, mockedClasspath);
		testBridge.tidy(testTidy, null, classpath);
		EasyMock.verify(mockedExecutor, mockedClasspath);
	}
}
