package org.theotherorg.poros.robot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AbstractBaseProtoTest {
	private static final class TestBaseProto extends AbstractBaseProto {
	}

	private AbstractBaseProto testBase;

	@BeforeEach
	void setUp() {
		testBase = new TestBaseProto();
	}

	@Test
	void testGetArgumentFiles() {
		Assertions.assertNull(testBase.getArgumentFiles());
	}

	@Test
	void testGetDoc() {
		Assertions.assertNull(testBase.getDoc());
	}

	@Test
	void testGetExcludes() {
		Assertions.assertNull(testBase.getExcludes());
	}

	@Test
	void testGetIncludes() {
		Assertions.assertNull(testBase.getIncludes());
	}

	@Test
	void testGetMetadata() {
		Assertions.assertNull(testBase.getMetadata());
	}

	@Test
	void testGetName() {
		Assertions.assertNull(testBase.getName());
	}

	@Test
	void testGetPythonPaths() {
		Assertions.assertNull(testBase.getPythonPaths());
	}

	@Test
	void testGetSuites() {
		Assertions.assertNull(testBase.getSuites());
	}

	@Test
	void testGetTags() {
		Assertions.assertNull(testBase.getTags());
	}

	@Test
	void testGetTests() {
		Assertions.assertNull(testBase.getTests());
	}

	@Test
	void testSetArgumentFiles() {
		final List<String> expectedFiles = new ArrayList<>();
		final List<String> testFiles = new ArrayList<>();

		expectedFiles.add("file1");
		testFiles.add("file1");
		testBase.setArgumentFiles(testFiles);
		Assertions.assertEquals(expectedFiles, testBase.getArgumentFiles());
	}

	@Test
	void testSetDoc() {
		final String expectedDoc = "doc";

		testBase.setDoc(expectedDoc);
		Assertions.assertEquals(expectedDoc, testBase.getDoc());
	}

	@Test
	void testSetExcludes() {
		final List<String> expectedExcludes = new ArrayList<>();
		final List<String> testExcludes = new ArrayList<>();

		expectedExcludes.add("exclude1");
		testExcludes.add("exclude1");
		testBase.setExcludes(testExcludes);
		Assertions.assertEquals(expectedExcludes, testBase.getExcludes());
	}

	@Test
	void testSetIncludes() {
		final List<String> expectedIncludes = new ArrayList<>();
		final List<String> testIncludes = new ArrayList<>();

		expectedIncludes.add("include1");
		testIncludes.add("include1");
		testBase.setIncludes(testIncludes);
		Assertions.assertEquals(expectedIncludes, testBase.getIncludes());
	}

	@Test
	void testSetMetadata() {
		final Map<String, String> expectedMetadata = new HashMap<>();
		final Map<String, String> testMetadata = new HashMap<>();

		expectedMetadata.put("key1", "value1");
		testMetadata.put("key1", "value1");
		testBase.setMetadata(testMetadata);
		Assertions.assertEquals(expectedMetadata, testBase.getMetadata());
	}

	@Test
	void testSetName() {
		final String expectedName = "name";

		testBase.setName(expectedName);
		Assertions.assertEquals(expectedName, testBase.getName());
	}

	@Test
	void testSetPythonPaths() {
		final List<String> expectedPaths = new ArrayList<>();
		final List<String> testPaths = new ArrayList<>();

		expectedPaths.add("path1");
		testPaths.add("path1");
		testBase.setPythonPaths(testPaths);
		Assertions.assertEquals(expectedPaths, testBase.getPythonPaths());
	}

	@Test
	void testSetSuites() {
		final List<String> expectedSuites = new ArrayList<>();
		final List<String> testSuites = new ArrayList<>();

		expectedSuites.add("suite1");
		testSuites.add("suite1");
		testBase.setSuites(testSuites);
		Assertions.assertEquals(expectedSuites, testBase.getSuites());
	}

	@Test
	void testSetTags() {
		final List<String> expectedTags = new ArrayList<>();
		final List<String> testTags = new ArrayList<>();

		expectedTags.add("tags1");
		testTags.add("tags1");
		testBase.setTags(testTags);
		Assertions.assertEquals(expectedTags, testBase.getTags());
	}

	@Test
	void testSetTests() {
		final List<String> expectedTests = new ArrayList<>();
		final List<String> testTests = new ArrayList<>();

		expectedTests.add("test1");
		testTests.add("test1");
		testBase.setTests(testTests);
		Assertions.assertEquals(expectedTests, testBase.getTests());
	}
}
