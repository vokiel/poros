package org.theotherorg.poros.robot;

import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.gradle.api.Action;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.ConfigurationContainer;
import org.gradle.api.artifacts.Dependency;
import org.gradle.api.artifacts.DependencySet;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.plugins.Convention;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.plugins.PluginContainer;
import org.gradle.api.provider.Provider;
import org.gradle.api.reporting.ReportingExtension;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.workers.WorkerExecutor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.python.google.common.collect.Sets;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureDryRunAction;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureLibdocAction;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureRebotAction;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureRunAction;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureTestdocAction;
import org.theotherorg.poros.robot.RobotPlugin.ConfigureTidyAction;

class RobotPluginTest {
	private interface FileProvider extends Provider<File> {
	}

	private interface RunTaskProvider extends TaskProvider<RobotRunTask> {
	}

	private interface RebotTaskProvider extends TaskProvider<RobotRebotTask> {
	}

	private interface LibdocTaskProvider extends TaskProvider<RobotLibdocTask> {
	}

	private interface TidyTaskProvider extends TaskProvider<RobotTidyTask> {
	}

	private interface TestdocTaskProvider extends TaskProvider<RobotTestdocTask> {
	}

	private RobotRunnerBridge mockedBridge;
	private RobotPlugin testPlugin;

	@BeforeEach
	void setUp() {
		mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		testPlugin = new RobotPlugin(mockedBridge);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testApply() throws Exception {
		final Project mockedProject = EasyMock.createStrictMock(Project.class);
		final ConfigurationContainer mockedConfigs = EasyMock.createStrictMock(ConfigurationContainer.class);
		final Configuration mockedConfig = EasyMock.createStrictMock(Configuration.class);
		final ExtensionContainer mockedExtensions = EasyMock.createStrictMock(ExtensionContainer.class);
		final RobotPluginExtension mockedExtension = EasyMock.createStrictMock(RobotPluginExtension.class);
		final ReportingExtension mockedReporting = EasyMock.createStrictMock(ReportingExtension.class);
		final Provider<File> mockedFileProvider = EasyMock.createStrictMock(FileProvider.class);
		final Capture<Callable<File>> callableCapture = Capture.newInstance();
		final Capture<Action<DependencySet>> dependencySetCapture = Capture.newInstance();
		final File mockedReportingFile = EasyMock.createStrictMock(File.class);
		final DependencySet mockedDependencies = EasyMock.createStrictMock(DependencySet.class);
		final DependencyHandler mockedHandler = EasyMock.createStrictMock(DependencyHandler.class);
		final Dependency mockedDependency = EasyMock.createStrictMock(Dependency.class);
		final PluginContainer mockedPlugins = EasyMock.createStrictMock(PluginContainer.class);
		final Capture<Action<JavaPlugin>> javaCapture = Capture.newInstance();
		final JavaPlugin mockedPlugin = EasyMock.createStrictMock(JavaPlugin.class);
		final Convention mockedConvention = EasyMock.createStrictMock(Convention.class);
		final JavaPluginConvention mockedPluginConvention = EasyMock.createStrictMock(JavaPluginConvention.class);
		final SourceSetContainer mockedSourceSets = EasyMock.createStrictMock(SourceSetContainer.class);
		final SourceSet mockedSourceSet = EasyMock.createStrictMock(SourceSet.class);
		final SourceDirectorySet mockedDirSet = EasyMock.createStrictMock(SourceDirectorySet.class);
		final TaskContainer mockedTasks = EasyMock.createStrictMock(TaskContainer.class);
		final RobotRunProto mockedRun = EasyMock.createStrictMock(RobotRunProto.class);
		final RobotRebotProto mockedRebot = EasyMock.createStrictMock(RobotRebotProto.class);
		final RobotLibdocProto mockedLibdoc = EasyMock.createStrictMock(RobotLibdocProto.class);
		final RobotTidyProto mockedTidy = EasyMock.createStrictMock(RobotTidyProto.class);
		final RobotTestdocProto mockedTestdoc = EasyMock.createStrictMock(RobotTestdocProto.class);
		final TaskProvider<RobotRunTask> mockedRunTaskProvider = EasyMock.createStrictMock(RunTaskProvider.class);
		final TaskProvider<RobotRebotTask> mockedRebotTaskProvider = EasyMock.createStrictMock(RebotTaskProvider.class);
		final TaskProvider<RobotLibdocTask> mockedLibdocTaskProvider = EasyMock.createStrictMock(LibdocTaskProvider.class);
		final TaskProvider<RobotTidyTask> mockedTidyTaskProvider = EasyMock.createStrictMock(TidyTaskProvider.class);
		final TaskProvider<RobotTestdocTask> mockedTestdocTaskProvider = EasyMock.createStrictMock(TestdocTaskProvider.class);
		final File testFile = Paths.get("/parent/test").toFile();

		EasyMock.expect(mockedProject.getConfigurations()).andReturn(mockedConfigs).once();
		EasyMock.expect(mockedConfigs.create(EasyMock.anyString())).andReturn(mockedConfig).once();
		EasyMock.expect(mockedProject.getExtensions()).andReturn(mockedExtensions).once();
		EasyMock.expect(mockedExtensions.create(EasyMock.anyString(), EasyMock.eq(RobotPluginExtension.class))).andReturn(mockedExtension).once();
		EasyMock.expect(mockedProject.provider(EasyMock.capture(callableCapture))).andReturn(mockedFileProvider).once();
		mockedExtension.setReportsDir(mockedFileProvider);
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedConfig.setDescription(EasyMock.anyString())).andReturn(mockedConfig).once();
		EasyMock.expect(mockedConfig.setVisible(false)).andReturn(mockedConfig).once();
		EasyMock.expect(mockedConfig.setTransitive(true)).andReturn(mockedConfig).once();

		// configureDependencies
		EasyMock.expect(mockedExtension.getToolVersion()).andReturn("version").once();
		EasyMock.expect(mockedConfig.defaultDependencies(EasyMock.capture(dependencySetCapture))).andReturn(mockedConfig).once();

		// configureSourceSets
		EasyMock.expect(mockedProject.getPlugins()).andReturn(mockedPlugins).once();
		EasyMock.expect(mockedPlugins.withType(EasyMock.eq(JavaPlugin.class), EasyMock.capture(javaCapture))).andReturn(null).once();

		// configureTasks
		EasyMock.expect(mockedProject.getTasks()).andReturn(mockedTasks).once();
		EasyMock.expect(mockedExtension.getRunProto()).andReturn(mockedRun).once();
		EasyMock.expect(mockedExtension.getRebotProto()).andReturn(mockedRebot).once();
		EasyMock.expect(mockedExtension.getLibdocProto()).andReturn(mockedLibdoc).once();
		EasyMock.expect(mockedExtension.getTidyProto()).andReturn(mockedTidy).once();
		EasyMock.expect(mockedExtension.getTestdocProto()).andReturn(mockedTestdoc).once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotRunTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedRunTaskProvider).once();
		mockedRunTaskProvider.configure(EasyMock.anyObject(ConfigureDryRunAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotRunTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedRunTaskProvider).once();
		mockedRunTaskProvider.configure(EasyMock.anyObject(ConfigureRunAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotRebotTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedRebotTaskProvider).once();
		mockedRebotTaskProvider.configure(EasyMock.anyObject(ConfigureRebotAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotLibdocTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedLibdocTaskProvider).once();
		mockedLibdocTaskProvider.configure(EasyMock.anyObject(ConfigureLibdocAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotTidyTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedTidyTaskProvider).once();
		mockedTidyTaskProvider.configure(EasyMock.anyObject(ConfigureTidyAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotTestdocTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedTestdocTaskProvider).once();
		mockedTestdocTaskProvider.configure(EasyMock.anyObject(ConfigureTestdocAction.class));
		EasyMock.expectLastCall().once();

		// Provider<File> capture
		EasyMock.expect(mockedProject.getExtensions()).andReturn(mockedExtensions).once();
		EasyMock.expect(mockedExtensions.findByType(ReportingExtension.class)).andReturn(mockedReporting).once();
		EasyMock.expect(mockedReporting.file(EasyMock.anyString())).andReturn(mockedReportingFile).once();

		// DependencySet capture
		EasyMock.expect(mockedProject.getDependencies()).andReturn(mockedHandler).once();
		EasyMock.expect(mockedHandler.create(EasyMock.anyString())).andReturn(mockedDependency).once();
		EasyMock.expect(mockedDependencies.add(mockedDependency)).andReturn(true).once();

		// JavaPlugin capture
		EasyMock.expect(mockedProject.getConvention()).andReturn(mockedConvention).once();
		EasyMock.expect(mockedConvention.getPlugin(JavaPluginConvention.class)).andReturn(mockedPluginConvention).once();
		EasyMock.expect(mockedPluginConvention.getSourceSets()).andReturn(mockedSourceSets).once();
		EasyMock.expect(mockedSourceSets.create(EasyMock.anyString())).andReturn(mockedSourceSet).once();
		mockedSourceSet.setCompileClasspath(mockedConfig);
		EasyMock.expectLastCall().once();
		mockedSourceSet.setRuntimeClasspath(mockedConfig);
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedSourceSet.getResources()).andReturn(mockedDirSet).once();
		EasyMock.expect(mockedDirSet.getSrcDirs()).andReturn(Sets.newHashSet(testFile)).times(2);
		EasyMock.expect(mockedDirSet.setSrcDirs(EasyMock.anyObject(Iterable.class))).andReturn(mockedDirSet).once();

		EasyMock.replay(mockedBridge, mockedProject, mockedConfigs, mockedConfig, mockedExtensions, mockedExtension,
			mockedReporting, mockedFileProvider, mockedReportingFile, mockedDependencies, mockedHandler, mockedDependency,
			mockedPlugins, mockedPlugin, mockedConvention, mockedPluginConvention, mockedSourceSets, mockedSourceSet, mockedDirSet,
			mockedTasks, mockedRun, mockedRebot, mockedLibdoc, mockedTidy, mockedTestdoc,
			mockedRunTaskProvider, mockedRebotTaskProvider, mockedLibdocTaskProvider, mockedTidyTaskProvider, mockedTestdocTaskProvider);

		testPlugin.apply(mockedProject);
		Assertions.assertSame(mockedReportingFile, callableCapture.getValue().call());
		dependencySetCapture.getValue().execute(mockedDependencies);
		javaCapture.getValue().execute(mockedPlugin);

		EasyMock.verify(mockedBridge, mockedProject, mockedConfigs, mockedConfig, mockedExtensions, mockedExtension,
			mockedReporting, mockedFileProvider, mockedReportingFile, mockedDependencies, mockedHandler, mockedDependency,
			mockedPlugins, mockedPlugin, mockedConvention, mockedPluginConvention, mockedSourceSets, mockedSourceSet, mockedDirSet,
			mockedTasks, mockedRun, mockedRebot, mockedLibdoc, mockedTidy, mockedTestdoc,
			mockedRunTaskProvider, mockedRebotTaskProvider, mockedLibdocTaskProvider, mockedTidyTaskProvider, mockedTestdocTaskProvider);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testApplyNoReporting() throws Exception {
		final Project mockedProject = EasyMock.createStrictMock(Project.class);
		final ConfigurationContainer mockedConfigs = EasyMock.createStrictMock(ConfigurationContainer.class);
		final Configuration mockedConfig = EasyMock.createStrictMock(Configuration.class);
		final ExtensionContainer mockedExtensions = EasyMock.createStrictMock(ExtensionContainer.class);
		final RobotPluginExtension mockedExtension = EasyMock.createStrictMock(RobotPluginExtension.class);
		final Provider<File> mockedFileProvider = EasyMock.createStrictMock(FileProvider.class);
		final Capture<Callable<File>> callableCapture = Capture.newInstance();
		final Capture<Action<DependencySet>> dependencySetCapture = Capture.newInstance();
		final DependencySet mockedDependencies = EasyMock.createStrictMock(DependencySet.class);
		final DependencyHandler mockedHandler = EasyMock.createStrictMock(DependencyHandler.class);
		final Dependency mockedDependency = EasyMock.createStrictMock(Dependency.class);
		final PluginContainer mockedPlugins = EasyMock.createStrictMock(PluginContainer.class);
		final Capture<Action<JavaPlugin>> javaCapture = Capture.newInstance();
		final JavaPlugin mockedPlugin = EasyMock.createStrictMock(JavaPlugin.class);
		final Convention mockedConvention = EasyMock.createStrictMock(Convention.class);
		final JavaPluginConvention mockedPluginConvention = EasyMock.createStrictMock(JavaPluginConvention.class);
		final SourceSetContainer mockedSourceSets = EasyMock.createStrictMock(SourceSetContainer.class);
		final SourceSet mockedSourceSet = EasyMock.createStrictMock(SourceSet.class);
		final SourceDirectorySet mockedDirSet = EasyMock.createStrictMock(SourceDirectorySet.class);
		final TaskContainer mockedTasks = EasyMock.createStrictMock(TaskContainer.class);
		final RobotRunProto mockedRun = EasyMock.createStrictMock(RobotRunProto.class);
		final RobotRebotProto mockedRebot = EasyMock.createStrictMock(RobotRebotProto.class);
		final RobotLibdocProto mockedLibdoc = EasyMock.createStrictMock(RobotLibdocProto.class);
		final RobotTidyProto mockedTidy = EasyMock.createStrictMock(RobotTidyProto.class);
		final RobotTestdocProto mockedTestdoc = EasyMock.createStrictMock(RobotTestdocProto.class);
		final TaskProvider<RobotRunTask> mockedRunTaskProvider = EasyMock.createStrictMock(RunTaskProvider.class);
		final TaskProvider<RobotRebotTask> mockedRebotTaskProvider = EasyMock.createStrictMock(RebotTaskProvider.class);
		final TaskProvider<RobotLibdocTask> mockedLibdocTaskProvider = EasyMock.createStrictMock(LibdocTaskProvider.class);
		final TaskProvider<RobotTidyTask> mockedTidyTaskProvider = EasyMock.createStrictMock(TidyTaskProvider.class);
		final TaskProvider<RobotTestdocTask> mockedTestdocTaskProvider = EasyMock.createStrictMock(TestdocTaskProvider.class);
		final File testFile = Paths.get("/parent/test").toFile();

		EasyMock.expect(mockedProject.getConfigurations()).andReturn(mockedConfigs).once();
		EasyMock.expect(mockedConfigs.create(EasyMock.anyString())).andReturn(mockedConfig).once();
		EasyMock.expect(mockedProject.getExtensions()).andReturn(mockedExtensions).once();
		EasyMock.expect(mockedExtensions.create(EasyMock.anyString(), EasyMock.eq(RobotPluginExtension.class))).andReturn(mockedExtension).once();
		EasyMock.expect(mockedProject.provider(EasyMock.capture(callableCapture))).andReturn(mockedFileProvider).once();
		mockedExtension.setReportsDir(mockedFileProvider);
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedConfig.setDescription(EasyMock.anyString())).andReturn(mockedConfig).once();
		EasyMock.expect(mockedConfig.setVisible(false)).andReturn(mockedConfig).once();
		EasyMock.expect(mockedConfig.setTransitive(true)).andReturn(mockedConfig).once();

		// configureDependencies
		EasyMock.expect(mockedExtension.getToolVersion()).andReturn(null).once();
		EasyMock.expect(mockedConfig.defaultDependencies(EasyMock.capture(dependencySetCapture))).andReturn(mockedConfig).once();

		// configureSourceSets
		EasyMock.expect(mockedProject.getPlugins()).andReturn(mockedPlugins).once();
		EasyMock.expect(mockedPlugins.withType(EasyMock.eq(JavaPlugin.class), EasyMock.capture(javaCapture))).andReturn(null).once();

		// configureTasks
		EasyMock.expect(mockedProject.getTasks()).andReturn(mockedTasks).once();
		EasyMock.expect(mockedExtension.getRunProto()).andReturn(mockedRun).once();
		EasyMock.expect(mockedExtension.getRebotProto()).andReturn(mockedRebot).once();
		EasyMock.expect(mockedExtension.getLibdocProto()).andReturn(mockedLibdoc).once();
		EasyMock.expect(mockedExtension.getTidyProto()).andReturn(mockedTidy).once();
		EasyMock.expect(mockedExtension.getTestdocProto()).andReturn(mockedTestdoc).once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotRunTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedRunTaskProvider).once();
		mockedRunTaskProvider.configure(EasyMock.anyObject(ConfigureDryRunAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotRunTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedRunTaskProvider).once();
		mockedRunTaskProvider.configure(EasyMock.anyObject(ConfigureRunAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotRebotTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedRebotTaskProvider).once();
		mockedRebotTaskProvider.configure(EasyMock.anyObject(ConfigureRebotAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotLibdocTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedLibdocTaskProvider).once();
		mockedLibdocTaskProvider.configure(EasyMock.anyObject(ConfigureLibdocAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotTidyTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedTidyTaskProvider).once();
		mockedTidyTaskProvider.configure(EasyMock.anyObject(ConfigureTidyAction.class));
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedTasks.register(EasyMock.anyString(), EasyMock.eq(RobotTestdocTask.class), EasyMock.eq(mockedBridge)))
			.andReturn(mockedTestdocTaskProvider).once();
		mockedTestdocTaskProvider.configure(EasyMock.anyObject(ConfigureTestdocAction.class));
		EasyMock.expectLastCall().once();

		// Provider<File> capture
		EasyMock.expect(mockedProject.getExtensions()).andReturn(mockedExtensions).once();
		EasyMock.expect(mockedExtensions.findByType(ReportingExtension.class)).andReturn(null).once();
		EasyMock.expect(mockedProject.file(EasyMock.anyString())).andReturn(null).once();

		// DependencySet capture
		EasyMock.expect(mockedProject.getDependencies()).andReturn(mockedHandler).once();
		EasyMock.expect(mockedHandler.create(EasyMock.anyString())).andReturn(mockedDependency).once();
		EasyMock.expect(mockedDependencies.add(mockedDependency)).andReturn(true).once();

		// JavaPlugin capture
		EasyMock.expect(mockedProject.getConvention()).andReturn(mockedConvention).once();
		EasyMock.expect(mockedConvention.getPlugin(JavaPluginConvention.class)).andReturn(mockedPluginConvention).once();
		EasyMock.expect(mockedPluginConvention.getSourceSets()).andReturn(mockedSourceSets).once();
		EasyMock.expect(mockedSourceSets.create(EasyMock.anyString())).andReturn(mockedSourceSet).once();
		mockedSourceSet.setCompileClasspath(mockedConfig);
		EasyMock.expectLastCall().once();
		mockedSourceSet.setRuntimeClasspath(mockedConfig);
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedSourceSet.getResources()).andReturn(mockedDirSet).once();
		EasyMock.expect(mockedDirSet.getSrcDirs()).andReturn(Sets.newHashSet(testFile)).times(2);
		EasyMock.expect(mockedDirSet.setSrcDirs(EasyMock.anyObject(Iterable.class))).andReturn(mockedDirSet).once();

		EasyMock.replay(mockedBridge, mockedProject, mockedConfigs, mockedConfig, mockedExtensions, mockedExtension,
			mockedFileProvider, mockedDependencies, mockedHandler, mockedDependency, mockedPlugins, mockedPlugin, mockedConvention,
			mockedPluginConvention, mockedSourceSets, mockedSourceSet, mockedDirSet,
			mockedTasks, mockedRun, mockedRebot, mockedLibdoc, mockedTidy, mockedTestdoc,
			mockedRunTaskProvider, mockedRebotTaskProvider, mockedLibdocTaskProvider, mockedTidyTaskProvider, mockedTestdocTaskProvider);

		testPlugin.apply(mockedProject);
		Assertions.assertNull(callableCapture.getValue().call());
		dependencySetCapture.getValue().execute(mockedDependencies);
		javaCapture.getValue().execute(mockedPlugin);

		EasyMock.verify(mockedBridge, mockedProject, mockedConfigs, mockedConfig, mockedExtensions, mockedExtension,
			mockedFileProvider, mockedDependencies, mockedHandler, mockedDependency, mockedPlugins, mockedPlugin, mockedConvention,
			mockedPluginConvention, mockedSourceSets, mockedSourceSet, mockedDirSet,
			mockedTasks, mockedRun, mockedRebot, mockedLibdoc, mockedTidy, mockedTestdoc,
			mockedRunTaskProvider, mockedRebotTaskProvider, mockedLibdocTaskProvider, mockedTidyTaskProvider, mockedTestdocTaskProvider);
	}

	@Test
	void testInjectedWorker() {
		final WorkerExecutor mockedExec = EasyMock.createStrictMock(WorkerExecutor.class);

		EasyMock.replay(mockedExec);
		new RobotPlugin(mockedExec);
		EasyMock.verify(mockedExec);
	}
}
