package org.theotherorg.poros.robot;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.RebotBuilder;

public class RebotBridgeBuilderTest {
	private RebotBuilder testBuilder;

	@BeforeEach
	void setUp() {
		testBuilder = new RebotBuilder();
	}

	@Test
	void testAddEndTime() {
		final String expectedTime = "time";
		final List<String> testOptions;

		testBuilder.addEndTime(expectedTime);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedTime, testOptions.get(1));
	}

	@Test
	void testAddProcessEmptySuite() {
		final List<String> testOptions;

		testBuilder.addProcessEmptySuite();
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(1, testOptions.size());
		Assertions.assertEquals("--processemptysuite", testOptions.get(0));
	}

	@Test
	void testAddStartTime() {
		final String expectedTime = "time";
		final List<String> testOptions;

		testBuilder.addStartTime(expectedTime);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedTime, testOptions.get(1));
	}

	@Test
	void testFromAll() {
		final RobotRebotProto mockedRebot = EasyMock.createStrictMock(RobotRebotProto.class);
		final String expectedStartTime = "startTime";
		final String expectedEndTime = "endTime";
		final RebotBuilder testBuilder;
		final Set<String> testOptions;

		EasyMock.expect(mockedRebot.getStartTime()).andReturn(expectedStartTime).once();
		EasyMock.expect(mockedRebot.getEndTime()).andReturn(expectedEndTime).once();
		EasyMock.expect(mockedRebot.getName()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getDoc()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getMetadata()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTags()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTests()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getSuites()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getIncludes()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getExcludes()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getPythonPaths()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getArgumentFiles()).andReturn(null).once();
		EasyMock.expect(mockedRebot.isProcessEmptySuite()).andReturn(true).once();
		EasyMock.expect(mockedRebot.getTasks()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getCriticals()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getNonCriticals()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getOutput()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getOutputDir()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getLog()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getLogLevel()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getLogTitle()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getReport()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getReportTitle()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getXunit()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getReportBackground()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getSuiteStatLevel()).andReturn(0).once();
		EasyMock.expect(mockedRebot.getTagDocs()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTagStatCombines()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTagStatExcludes()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTagStatIncludes()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTagStatLinks()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getRemoveKeywords()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getFlattenKeywords()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getPreRebotModifiers()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getConsoleColors()).andReturn(null).once();
		EasyMock.expect(mockedRebot.isRpa()).andReturn(false).once();
		EasyMock.expect(mockedRebot.isSplitLog()).andReturn(false).once();
		EasyMock.expect(mockedRebot.isTimeStampOutputs()).andReturn(false).once();
		EasyMock.expect(mockedRebot.isXunitSkipNonCritical()).andReturn(false).once();
		EasyMock.expect(mockedRebot.isNoStatusRc()).andReturn(false).once();
		EasyMock.replay(mockedRebot);
		testBuilder = RebotBuilder.from(mockedRebot);
		testOptions = new HashSet<>(testBuilder.getOptions());
		Assertions.assertTrue(testOptions.contains(expectedEndTime));
		Assertions.assertTrue(testOptions.contains(expectedStartTime));
		Assertions.assertTrue(testOptions.contains("--processemptysuite"));
		EasyMock.verify(mockedRebot);
	}

	@Test
	void testFromEmpty() {
		final RobotRebotProto mockedRebot = EasyMock.createStrictMock(RobotRebotProto.class);
		final RebotBuilder testBuilder;

		EasyMock.expect(mockedRebot.getStartTime()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getEndTime()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getName()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getDoc()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getMetadata()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTags()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTests()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getSuites()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getIncludes()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getExcludes()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getPythonPaths()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getArgumentFiles()).andReturn(null).once();
		EasyMock.expect(mockedRebot.isProcessEmptySuite()).andReturn(false).once();
		EasyMock.expect(mockedRebot.getTasks()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getCriticals()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getNonCriticals()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getOutput()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getOutputDir()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getLog()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getLogLevel()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getLogTitle()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getReport()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getReportTitle()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getXunit()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getReportBackground()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getSuiteStatLevel()).andReturn(0).once();
		EasyMock.expect(mockedRebot.getTagDocs()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTagStatCombines()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTagStatExcludes()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTagStatIncludes()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getTagStatLinks()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getRemoveKeywords()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getFlattenKeywords()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getPreRebotModifiers()).andReturn(null).once();
		EasyMock.expect(mockedRebot.getConsoleColors()).andReturn(null).once();
		EasyMock.expect(mockedRebot.isRpa()).andReturn(false).once();
		EasyMock.expect(mockedRebot.isSplitLog()).andReturn(false).once();
		EasyMock.expect(mockedRebot.isTimeStampOutputs()).andReturn(false).once();
		EasyMock.expect(mockedRebot.isXunitSkipNonCritical()).andReturn(false).once();
		EasyMock.expect(mockedRebot.isNoStatusRc()).andReturn(false).once();
		EasyMock.replay(mockedRebot);
		testBuilder = RebotBuilder.from(mockedRebot);
		Assertions.assertNotNull(testBuilder);
		Assertions.assertTrue(testBuilder.getOptions().isEmpty());
		EasyMock.verify(mockedRebot);
	}

	@Test
	void testToArgs() {
		final File mockedFile = EasyMock.createStrictMock(File.class);
		final String[] expectedArgs = new String[] {"rebot", "path"};
		final String[] testArgs;

		EasyMock.expect(mockedFile.getPath()).andReturn(expectedArgs[1]).once();
		EasyMock.replay(mockedFile);
		testArgs = testBuilder.toArgs(mockedFile);
		Assertions.assertNotNull(testArgs);
		Assertions.assertArrayEquals(expectedArgs, testArgs);
		EasyMock.verify(mockedFile);
	}

	@Test
	void testToArgsNull() {
		final String[] testArgs = testBuilder.toArgs(null);

		Assertions.assertNotNull(testArgs);
		Assertions.assertEquals(1, testArgs.length);
	}
}
