package org.theotherorg.poros.robot;

import java.io.File;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotRebotProtoTest {
	private RobotRebotProto testRebot;

	@BeforeEach
	void setUp() {
		testRebot = new RobotRebotProto();
	}

	@Test
	void testGetEndTime() {
		Assertions.assertNull(testRebot.getEndTime());
	}

	@Test
	void testGetOutputXml() {
		Assertions.assertNull(testRebot.getOutputXml());
	}

	@Test
	void testGetStartTime() {
		Assertions.assertNull(testRebot.getStartTime());
	}

	@Test
	void testIsProcessEmptySuite() {
		Assertions.assertFalse(testRebot.isProcessEmptySuite());
	}

	@Test
	void testSetEndTime() {
		final String expectedTime = "time";

		testRebot.setEndTime(expectedTime);
		Assertions.assertEquals(expectedTime, testRebot.getEndTime());
	}

	@Test
	void testSetOutputXml() {
		final File mockedOutput = EasyMock.createStrictMock(File.class);

		EasyMock.replay(mockedOutput);
		testRebot.setOutputXml(mockedOutput);
		Assertions.assertSame(mockedOutput, testRebot.getOutputXml());
		EasyMock.verify(mockedOutput);
	}

	@Test
	void testSetProcessEmptySuite() {
		testRebot.setProcessEmptySuite(true);
		Assertions.assertTrue(testRebot.isProcessEmptySuite());
	}

	@Test
	void testSetStartTime() {
		final String expectedTime = "time";

		testRebot.setStartTime(expectedTime);
		Assertions.assertEquals(expectedTime, testRebot.getStartTime());
	}
}
