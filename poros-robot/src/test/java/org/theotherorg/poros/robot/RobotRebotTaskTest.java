package org.theotherorg.poros.robot;

import org.easymock.EasyMock;
import org.gradle.api.file.FileCollection;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotRebotTaskTest {
	private RobotRunnerBridge mockedBridge;
	private RobotRebotTask testTask;

	@BeforeEach
	void setUp() {
		mockedBridge = EasyMock.createStrictMock(RobotRunnerBridge.class);
		testTask = ProjectBuilder.builder().build().getTasks().create("testRebot", RobotRebotTask.class, mockedBridge);
	}

	@Test
	void testRobotRebot() {
		final RobotRebotProto mockedProto = EasyMock.createStrictMock(RobotRebotProto.class);
		final FileCollection mockedClasspath = EasyMock.createStrictMock(FileCollection.class);

		mockedBridge.rebot(mockedProto, mockedClasspath);
		EasyMock.expectLastCall().once();
		EasyMock.replay(mockedBridge, mockedClasspath, mockedProto);
		testTask.setRobotProto(mockedProto);
		testTask.setLibraryClasspath(mockedClasspath);
		testTask.robotRebot();
		EasyMock.verify(mockedBridge, mockedClasspath, mockedProto);
	}
}
