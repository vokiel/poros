package org.theotherorg.poros.robot;

import java.io.File;
import java.nio.file.Paths;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.gradle.api.Action;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.plugins.Convention;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.plugins.PluginContainer;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.python.google.common.collect.Sets;
import org.theotherorg.poros.robot.RobotPlugin.SourceSetConfiguration;

public class SourceSetConfigurationTest {
	private SourceSetConfiguration testConfig;

	@BeforeEach
	void setUp() {
		testConfig = new SourceSetConfiguration();
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	void testConfigureProject() {
		final Project mockedProject = EasyMock.createStrictMock(Project.class);
		final Configuration mockedConfig = EasyMock.createStrictMock(Configuration.class);
		final PluginContainer mockedPlugins = EasyMock.createStrictMock(PluginContainer.class);
		final Capture<Action<JavaPlugin>> capturedAction = Capture.newInstance();
		final JavaPlugin mockedPlugin = EasyMock.createStrictMock(JavaPlugin.class);
		final Convention mockedConvention = EasyMock.createStrictMock(Convention.class);
		final JavaPluginConvention mockedConventionPlugin = EasyMock.createStrictMock(JavaPluginConvention.class);
		final SourceSetContainer mockedSourceSets = EasyMock.createStrictMock(SourceSetContainer.class);
		final SourceSet mockedResult = EasyMock.createStrictMock(SourceSet.class);
		final SourceDirectorySet mockedDirSet = EasyMock.createStrictMock(SourceDirectorySet.class);
		final File testFile = Paths.get("/parent/test").toFile();

		EasyMock.expect(mockedProject.getPlugins()).andReturn(mockedPlugins).once();
		EasyMock.expect(mockedPlugins.withType(EasyMock.eq(JavaPlugin.class), EasyMock.capture(capturedAction))).andReturn(null).once();
		EasyMock.expect(mockedProject.getConvention()).andReturn(mockedConvention).once();
		EasyMock.expect(mockedConvention.getPlugin(JavaPluginConvention.class)).andReturn(mockedConventionPlugin).once();
		EasyMock.expect(mockedConventionPlugin.getSourceSets()).andReturn(mockedSourceSets).once();
		EasyMock.expect(mockedSourceSets.create(EasyMock.anyString())).andReturn(mockedResult).once();
		mockedResult.setCompileClasspath(mockedConfig);
		EasyMock.expectLastCall().once();
		mockedResult.setRuntimeClasspath(mockedConfig);
		EasyMock.expectLastCall().once();
		EasyMock.expect(mockedResult.getResources()).andReturn(mockedDirSet).once();
		EasyMock.expect(mockedDirSet.getSrcDirs()).andReturn(Sets.newHashSet(testFile)).times(2);
		EasyMock.expect(mockedDirSet.setSrcDirs(EasyMock.anyObject(Iterable.class))).andReturn(mockedDirSet).once();
		EasyMock.replay(mockedProject, mockedConfig, mockedPlugins, mockedPlugin, mockedConvention, mockedConventionPlugin,
			mockedSourceSets, mockedResult, mockedDirSet);
		testConfig.configureProject(mockedProject, mockedConfig);
		Assertions.assertTrue(capturedAction.hasCaptured());
		capturedAction.getValue().execute(mockedPlugin);
		Assertions.assertSame(mockedResult, testConfig.get());
		EasyMock.verify(mockedProject, mockedConfig, mockedPlugins, mockedPlugin, mockedConvention, mockedConventionPlugin,
			mockedSourceSets, mockedResult, mockedDirSet);
	}

	@Test
	void testGet() {
		Assertions.assertNull(testConfig.get());
	}
}
