package org.theotherorg.poros.robot;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.theotherorg.poros.robot.RobotRunnerBridge.TestDocBuilder;

public class TestdocBridgeBuilderTest {
	private TestDocBuilder testBuilder;

	@BeforeEach
	void setUp() {
		testBuilder = new TestDocBuilder();
	}

	@Test
	void testAddTitle() {
		final String expectedTitle = "title";
		final List<String> testOptions;

		testBuilder.addTitle(expectedTitle);
		testOptions = testBuilder.getOptions();
		Assertions.assertNotNull(testOptions);
		Assertions.assertEquals(2, testOptions.size());
		Assertions.assertNotNull(testOptions.get(0));
		Assertions.assertEquals(expectedTitle, testOptions.get(1));
	}

	@Test
	void testFrom() {
		final RobotTestdocProto mockedTestdoc = EasyMock.createStrictMock(RobotTestdocProto.class);
		final String expectedTitle = "title";
		final TestDocBuilder testBuilder;
		final Set<String> testOptions;

		EasyMock.expect(mockedTestdoc.getTitle()).andReturn(expectedTitle).once();
		EasyMock.expect(mockedTestdoc.getName()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getDoc()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getMetadata()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getTags()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getTests()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getSuites()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getIncludes()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getExcludes()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getPythonPaths()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getArgumentFiles()).andReturn(null).once();
		EasyMock.replay(mockedTestdoc);
		testBuilder = TestDocBuilder.from(mockedTestdoc);
		Assertions.assertNotNull(testBuilder);
		testOptions = new HashSet<>(testBuilder.getOptions());
		Assertions.assertTrue(testOptions.contains(expectedTitle));
		EasyMock.verify(mockedTestdoc);
	}

	@Test
	void testFromNoTitle() {
		final RobotTestdocProto mockedTestdoc = EasyMock.createStrictMock(RobotTestdocProto.class);
		final TestDocBuilder testBuilder;

		EasyMock.expect(mockedTestdoc.getTitle()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getName()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getDoc()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getMetadata()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getTags()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getTests()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getSuites()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getIncludes()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getExcludes()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getPythonPaths()).andReturn(null).once();
		EasyMock.expect(mockedTestdoc.getArgumentFiles()).andReturn(null).once();
		EasyMock.replay(mockedTestdoc);
		testBuilder = TestDocBuilder.from(mockedTestdoc);
		Assertions.assertNotNull(testBuilder);
		Assertions.assertTrue(testBuilder.getOptions().isEmpty());
		EasyMock.verify(mockedTestdoc);
	}

	@Test
	void testToArgs() {
		final File mockedInput = EasyMock.createStrictMock(File.class);
		final File mockedOutput = EasyMock.createStrictMock(File.class);
		final Set<File> testFiles = new HashSet<>();
		final String expectedInput = "input";
		final String expectedOutput = "output";
		final String[] testArgs;

		testFiles.add(mockedInput);
		EasyMock.expect(mockedInput.getPath()).andReturn(expectedInput).once();
		EasyMock.expect(mockedOutput.getPath()).andReturn(expectedOutput).once();
		EasyMock.replay(mockedInput, mockedOutput);
		testArgs = testBuilder.toArgs(testFiles, mockedOutput);
		Assertions.assertNotNull(testArgs);
		Assertions.assertArrayEquals(new String[] {"testdoc", expectedInput, expectedOutput}, testArgs);
		EasyMock.verify(mockedInput, mockedOutput);
	}

	@Test
	void testToArgsNullOutput() {
		final Set<File> testFiles = new HashSet<>();
		final String[] testArgs = testBuilder.toArgs(testFiles, null);

		Assertions.assertNotNull(testArgs);
		Assertions.assertEquals(1, testArgs.length);
	}

	@Test
	void testToArgsNullSources() {
		final String[] testArgs = testBuilder.toArgs(null, null);

		Assertions.assertNotNull(testArgs);
		Assertions.assertEquals(1, testArgs.length);
	}
}
