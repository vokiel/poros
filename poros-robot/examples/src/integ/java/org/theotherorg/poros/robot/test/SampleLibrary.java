package org.theotherorg.poros.robot.test;

import java.util.Objects;

@SuppressWarnings("unused")
public class SampleLibrary {
	private String lastArgument;

	public void echo(final String argument) {
		lastArgument = argument;
		System.out.println("Got the argument: " + argument);
	}

	public boolean shouldEqual(final String expected) {
		return Objects.equals(expected, lastArgument);
	}
}
