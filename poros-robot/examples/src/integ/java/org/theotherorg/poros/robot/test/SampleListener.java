package org.theotherorg.poros.robot.test;

import java.util.Map;

@SuppressWarnings("unused")
public class SampleListener {
	public static final int ROBOT_LISTENER_API_VERSION = 2;

	public void endSuite(final String name, final Map<String, String> attributes) {
		System.out.println("End suite " + name);
	}

	public void endTest(final String name, final Map<String, String> attributes) {
		System.out.println("End test " + name);
	}

	public void startSuite(final String name, final Map<String, String> attributes) {
		System.out.println("Start suite " + name);
	}

	public void startTest(final String name, final Map<String, String> attributes) {
		System.out.println("Start test " + name);
	}
}
