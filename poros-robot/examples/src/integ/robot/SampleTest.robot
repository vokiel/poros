*** Settings ***
Documentation     Run testdoc gradle library tests
Test Template     Echo argument
Library           org.theotherorg.poros.robot.test.SampleLibrary

*** Test Cases ***
Test Echo String
    [Documentation]    Test cases documentation
    [Tags]    includeTag    nonCriticalTag
    Testing One, Two

Test Echo String 2
    [Tags]    includeTag    criticalTag
    Testing One, Two, Three

Test Exclude
    [Tags]    tag nasty    excludeTag
    Testing Zero

*** Keywords ***
Echo argument
    [Arguments]    ${value}
    [Documentation]    The *first line* creates the short doc, which
    ...    can span multiple lines.
    ...
    ...    This is the body of the _documentation_.
    ...    It is not shown in Libdoc outputs but only
    ...    the short doc is shown in logs.
    Echo    ${value}
    Should Equal    ${value}
